package com.daokin.biochecker.ui;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListViewAdapter extends BaseAdapter {
	private List<Map<String, String>> list;
	private Context context;
	
	public ListViewAdapter(Context context, List<Map<String, String>> list) {
		this.list = list;
		this.context = context;
	}

	public int getCount() {
		return list.size();
	}

	public Object getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		MyView tag;
		
		if (null == convertView) {
			View v = LayoutInflater.from(context).inflate(
					R.layout.grid_view_list, null);
			tag = new MyView();
			tag.textView = (TextView) v.findViewById(R.id.grid_device_name);
//			tag.statusView = (TextView) v.findViewById(R.id.grid_device_status); 
			
			v.setTag(tag);
			convertView = v;
		} else {
			tag = (MyView) convertView.getTag();
		}
		
		String text = list.get(position).get("deviceName");
		//String status = list.get(position).get("bondStatus");		
		
		tag.textView.setText(text);
		//tag.statusView.setText(status);
		
		return convertView;
	}
}


class MyView {
TextView textView;
//TextView statusView;
}