package com.daokin.biochecker.model;

/**
 * Created by SamXCode on 2016/2/25.
 */
public class ResultDetail extends ResultData {
    private AssayData assay_printing;

    public AssayData getAssay_printing() {
        return assay_printing;
    }

    public void setAssay_printing(AssayData assay_printing) {
        this.assay_printing = assay_printing;
    }
}
