package com.daokin.biochecker.protocol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import android.util.Log;

import com.daokin.biochecker.model.AssayData;
import com.daokin.biochecker.model.GMRData;
import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.SensorData;

/**
 * Usage
 * methods
 * 1. dataOperation.setPrintingSensors(printingSensors)
 * 2. dataOperation.setRefSensors(refSensors)
 * 3. dataOperation.operate(dataInputs)
 * 4. dataOperation.getSensorAllDataList()
 * 5. dataOperation.getVolChgData()
 *
 * @author XXY
 */
public class DataOperation {

    private static final String TAG = "BioChecker";
    //private static int SENSOR_COUNT = ModelContainer.i.getLabelSensorMap().size();

    private int MNPsInitialCycle = 1; // 10;
    private int MNPsFinalCycle = 5; // 20;
    private int N_initial = 1; // 5;
    private int N_final = 1; // 5;
    private float NoiseLimit = 0.025f; //1E-5f;
    private int cycleMNPs;
    private int postCycle;
    private int cycle;
    private float referenceAvg;
    private int recSensorCount; // the number of the finished cycles in one loop
    private static String electricalReferenceSensor = "";
    //the sensors data List before MNPs added
    private List<Map> preSensorDataList;
    //sensors that need to print in all assays
    private Set<String> printingSensors;
    private List<Byte> refSensors; // reference sensors
    private List<Map> sensorAllDataList; // save all sensors data of all times
    //a map which the value is printing sensors data(Reference Subtraction) list
//    private Map<String, List> pSensorData;
    //save a set of sensors data in one loop
    private Map<String, Float> onceSensorsData;
    //the sensors data map during which MNPs were added
    private Map<String, Float> addingMNPsSensorData;
    private Map<String, Float> volChgData; // voltage change data
    //store the data within 4 loops
    private Map<String, LinkedList> noiseDataMap;
    //store the print sensor values
    private Map<Byte, List<Float>> pSensorValues;
    private Map<Byte, List<Float>> oriSensorValues;
    // zs - muxInUse and labelInUse lists exclusive of QCFailed sensors, all calculations should be based on those lists, not all sensors
    private List<Byte> muxInUse = new ArrayList<Byte>();

    private GMRData gmrData;

    public List<Byte> getMuxInUse() {
        return muxInUse;
    }

    public void setMuxInUse(List<Byte> muxInUse) {
        this.muxInUse = muxInUse;
    }

    private List<String> labelInUse = new ArrayList<String>();

    public List<String> getLabelInUse() {
        return labelInUse;
    }

    public void setLabelInUse(List<String> labelInUse) {
        this.labelInUse = labelInUse;
    }

    private Map<Byte, String> muxLabelMap = new TreeMap<Byte, String>();

    public Map<Byte, String> getMuxLabelMap() {
        return muxLabelMap;
    }

    public void setMuxLabelMap(Map<Byte, String> muxLabelMap) {
        this.muxLabelMap = muxLabelMap;
    }

    private Map<String, Byte> labelMuxMap = new TreeMap<String, Byte>();

    public Map<String, Byte> getLabelMuxMap() {
        return labelMuxMap;
    }

    public void setLabelMuxMap(Map<String, Byte> labelMuxMap) {
        this.labelMuxMap = labelMuxMap;
    }

    public static String getElectricalReferenceSensor() { return electricalReferenceSensor; }
    public static void setElectricalReferenceSensor(String electricalReferenceLabel) {
        electricalReferenceSensor = electricalReferenceLabel;
    }

    public DataOperation() {
        cycleMNPs = -1;
        recSensorCount = 0;
        postCycle = 0;
        cycle = 0;
        sensorAllDataList = new ArrayList<>();
        preSensorDataList = new ArrayList<>();
        onceSensorsData = new HashMap<>();
        addingMNPsSensorData = new HashMap<>();
        volChgData = new HashMap<>();
        noiseDataMap = new HashMap<>();
        pSensorValues = new HashMap<>();
        oriSensorValues = new HashMap<>();
        for (Byte b : ModelContainer.i.getMux1LabelMap().keySet()) {
            oriSensorValues.put(b, new ArrayList<Float>());
        }
        gmrData = new GMRData();
    }

    public Map<Byte, List<Float>> getPSensorValues() {
        Iterator<Map.Entry<Byte, List<Float>>> it = pSensorValues.entrySet().iterator();
        while (it.hasNext()) {
            List<Float> list = it.next().getValue();
            int size = list.size();
            if (size == 0) {
                it.remove();
            } else if (size > postCycle) {
                list.remove(postCycle);
            }
        }
        return pSensorValues;
    }

    public List<Map> getSensorAllDataList() {
        return sensorAllDataList;
    }

    public Map<String, Float> getVolChgData() {
        volChangeCalculate();
        return volChgData;
    }

    public Map<Byte, List<Float>> getOriSensorValues() {
        Iterator<Map.Entry<Byte, List<Float>>> it = oriSensorValues.entrySet().iterator();
        while (it.hasNext()) {
            List<Float> list = it.next().getValue();
            int size = list.size();
            if (size == 0) {
                it.remove();
            } else if (size > cycle) {
                list.remove(cycle);
            }
        }
        return oriSensorValues;
    }

    public void setCycleMNPs() {
        this.cycleMNPs = 0;
    }

    public void setPrintingSensors(Set<String> printingSensors) {
        this.printingSensors = printingSensors;
        for (String printingSensor : printingSensors) {
            noiseDataMap.put(printingSensor, new LinkedList());
            pSensorValues.put(Byte.valueOf(printingSensor.substring(1)), new ArrayList<Float>());
        }
    }

    public void removeQCFailedSensor(Byte sensorId) {
        if (pSensorValues != null && pSensorValues.containsKey(sensorId)) {
            pSensorValues.remove(sensorId);
            //Log.d(TAG, "remove QC failed sensor:" + sensorId);
        }
        if (refSensors != null && refSensors.contains(sensorId)) {
            refSensors.remove(sensorId);
            //Log.d(TAG, "remove QC failed reference sensor:" + sensorId);
        }
        //Log.d(TAG, "sensors after removed QC failed sensors!");
        //Log.d(TAG, "pSensorValues sensors: " + Arrays.toString(
        //        pSensorValues.keySet().toArray(new Byte[pSensorValues.keySet().size()])));
        //Log.d(TAG, "refSensors: "
        //        + Arrays.toString(refSensors.toArray(new Byte[refSensors.size()])));
    }

    public void setRefSensors(List<Byte> refSensors) {
        this.refSensors = refSensors;
    }

    public Set getAllLatestPrintingSensorSet() {
        return noiseDataMap.keySet();
    }

    public Map<Integer, List<Float>> getAverages() {
        Map<Integer, List<Float>> map = new HashMap<>();
        Map<Integer, List<Byte>> analyteSensor = ModelContainer.i.getAnalyteSensor();
        for (Map.Entry<Integer, List<Byte>> entry : analyteSensor.entrySet()) {
            List<Byte> sensors = entry.getValue();
            Iterator<Byte> iterator = sensors.iterator();
            while (iterator.hasNext()) {
                Byte sMux = iterator.next();
                if (!noiseDataMap.keySet().contains("s" + sMux)
                        || !muxInUse.contains(sMux)) {
                    iterator.remove();
                    Log.d(TAG, "sensor [" + sMux + "] removed");
                }
            }
            if (sensors == null || sensors.isEmpty()) {
                continue;
            }
            List<Float> list = new ArrayList<>(postCycle);
            for (int i = 0; i < postCycle; i++) {
                float sum = 0;
                for (Byte b : sensors) {
                    List<Float> temp = pSensorValues.get(b);
                    Log.d(TAG, "sensor:" + b + ", values:" + temp);
                    sum += temp.get(i);
                }
                list.add(sum / sensors.size());
            }
            map.put(entry.getKey(), list);
        }
        return map;
    }

    /**
     * add the data item to process
     *
     * @param dataInputs
     */
    public void operate(Object[] dataInputs) {
        try {
            //Build gmr data
            buildGMRData(dataInputs);
        } catch (Exception ex) {
            Log.d(TAG, null, ex);
        }
    }

    public GMRData getGMRData() {
        return gmrData;
    }

    private void buildGMRData(Object[] sample) {
        byte sensorMux1 = -1;
        try {
            sensorMux1 = (byte)sample[0];
            //Map<Byte, String> sensorMap = ModelContainer.i.getSensorLabelMap();
            Map<Byte, String> mux1LabelMap = ModelContainer.i.getMux1LabelMap();
            String sensorLabel = mux1LabelMap.get(sensorMux1);
            List<Float> cycleData = new ArrayList<Float>();
            cycleData.add(new Double((double) sample[3]).floatValue());
            cycleData.add(new Short((short) sample[4]).floatValue());
            cycleData.add((Float) sample[1]);
            cycleData.add((Float) sample[2]);
            cycleData.add(new Integer((int) sample[5]).floatValue());
            cycleData.add((Float) sample[6]);
            cycleData.add((Float) sample[7]);
            SensorData sensorData = gmrData.getData(sensorLabel);
            if (sensorData == null) {
                sensorData = new SensorData();
            }
            sensorData.addData(cycleData);
            gmrData.setData(sensorLabel, sensorData);
        } catch (Exception ex) {
            Log.e(TAG, "Build gmr data failed. Sensor mux1: " + String.format("0x%02X ", sensorMux1) + ex.getMessage());
        }
    }

    /* check the sensor signals out of range of noise */
    private boolean checkNoise(String sensor, float signalVoltage) {
        if (noiseDataMap.keySet().contains(sensor)) {
            LinkedList<Float> linkedList = noiseDataMap.get(sensor);
            if (linkedList.size() < 4) { // the loop iterations are less than 3
                linkedList.add(signalVoltage);
                return false;
            }
            linkedList.add(signalVoltage);
            float sum02 = (linkedList.get(0) + linkedList.get(2)) / 2;
            float sum13 = (linkedList.get(1) + linkedList.get(3)) / 2;
            linkedList.poll(); // remove the first signal in the list
            if (Math.abs(sum02 - sum13) > NoiseLimit) {
                return true;
            }
        }
        return false;
    }

    /* calculate v3[s, n] which is the initial data points preceding n0 */
    private void setNInitialData() {
        int size = preSensorDataList.size();
        for (int i = size - N_initial; i < size; i++) {
            Map<String, Float> sensorData = preSensorDataList.get(i);
            Map<String, Float> map = new HashMap();
            ampTareCalculateForInitial(map, sensorData);
            refSubCalculateForInitial(map, refAvgCalculateForInitial(map));
            sensorAllDataList.add(sensorData);
        }
    }

    private float refSubCalculate(String label, float value) {
        try {
            float refSub; // zs temp
            if (addingMNPsSensorData != null) {
                float ampTare = Math.abs(value - addingMNPsSensorData.get(label));
                refSub = Math.abs(ampTare - referenceAvg);
                onceSensorsData.put(label, refSub);
            } else // zs temp
            {
                refSub = Math.abs(value);
            }
            return refSub;
        } catch (Exception e) {
            Log.e(TAG, "label=" + label, e);
            Log.e(TAG, "addingMNPsSensorData=" + addingMNPsSensorData);
            throw e;
        }
    }

    /* calculate reference average of the last postCycle */
    private void refAvgCalculate() {
        float sum = 0;
        try {
            for (Byte b : refSensors) {
                sum = sum
                        + (float) sensorAllDataList.get(sensorAllDataList.size() - 2).get("s" + b);
            }
        } catch (Exception e) {
            Log.e("BioChecker", "", e);
        }
        if (refSensors.size() == 0) {
            referenceAvg = 0;
        } else {
            referenceAvg = sum / refSensors.size();
        }
    }

    /*
     * calculate reference subtraction which data is received during the initial-time
     */
    private void refSubCalculateForInitial(Map<String, Float> map, float refAvg) {
        for (Map.Entry<String, Float> entrySet : map.entrySet()) {
            String key = entrySet.getKey();
            Float value = entrySet.getValue();
            value = value - refAvg;
            map.put(key, Math.abs(value));
        }
    }

    /*
     * calculate reference average which data is received during the initial-time
     */
    private float refAvgCalculateForInitial(Map<String, Float> map) {
        float sum = 0;
        for (Byte b : refSensors) {
            String str = "s" + b;
            try {
                sum = sum + map.get(str);
            } catch (NullPointerException e) {
                Log.e("BioChecker", "str=" + str, e);
                Log.e("BioChecker", "map=" + map);
                Log.e("BioChecker", "map.size()=" + map.size());
                Log.e("BioChecker", "onceSensorsData=" + onceSensorsData);
                Log.e("BioChecker", "onceSensorsData.size()=" + onceSensorsData.size());
                Log.e("BioChecker", "addingMNPsSensorData=" + addingMNPsSensorData);
            }
        }
        float refAvg;
        if (refSensors.size() == 0) {
            refAvg = 0;
        } else {
            refAvg = sum / refSensors.size();
        }
        // make the referenceAvg come from the postCycle before the added MNPS
        referenceAvg = refAvg;
        return refAvg;
    }

    /* calculate amplitude tare which data is received during the initial-time */
    private void ampTareCalculateForInitial(Map<String, Float> map,
                                            Map<String, Float> sensorData) {
        Log.d("BioChecker", "addingMNPsSensorData:" + addingMNPsSensorData);
        try {
            for (Map.Entry<String, Float> entrySet : sensorData.entrySet()) {
                try {
                    String key = entrySet.getKey();
                    Float value = entrySet.getValue();
                    Float ampTare = value; // zs temp;
                    if (addingMNPsSensorData != null) {
                        ampTare = Math.abs(value - addingMNPsSensorData.get(key));
                    }
                    map.put(key, ampTare);
                } catch (Throwable e) {
                    Log.e("BioChecker", "", e);
                }
            }
        } catch (Throwable e) {
            Log.e(TAG, "", e);
        }
    }

    /* calculate voltage change */
    private void volChangeCalculate() {
        Set<String> set = getIntersection(printingSensors,
                addingMNPsSensorData.keySet());
        for (String label : set) {
            float nInitialVal = 0;
            float nFinalVal = 0;
            for (int i = 0; i < N_initial; i++) {
                Map<String, Float> sensorData = sensorAllDataList.get(i);
                nInitialVal = nInitialVal + sensorData.get(label);
            }
            nInitialVal = nInitialVal / N_initial;
            for (int i = sensorAllDataList.size() - N_final; i < sensorAllDataList
                    .size(); i++) {
                Map<String, Float> sensorData = sensorAllDataList.get(i);
                try {
                    nFinalVal = nFinalVal + sensorData.get(label);
                } catch (Throwable e) {
                    Log.e(TAG, "label=" + label, e);
                    Log.e(TAG, "sensorData=" + sensorData);
                }
            }
            nFinalVal = nFinalVal / N_final;
            volChgData.put(label, Math.abs(nFinalVal - nInitialVal));
        }
    }

    /* add the data to the sensorAllDataList */
    private void addCalculatedData() {
        sensorAllDataList.add(onceSensorsData);
    }

    /* deep copy src to dest */
    private void deepClone(Map<String, Float> src, Map<String, Float> dest) {
        for (Map.Entry<String, Float> entrySet : src.entrySet()) {
            dest.put(entrySet.getKey(), entrySet.getValue());
        }
    }

    // get intersection
    private Set getIntersection(Set<String> set1, Set<String> set2) {
        Set<String> tempSet = new HashSet<>();
        for (String str : set2) {
            if (set1.contains(str)) {
                tempSet.add(str);
            }
        }
        return tempSet;
    }

    // zs - need more work
    public boolean checkNoise(List<List<Float>> data) {
        // zs need more work, check only before MNP added
        int dataSize = data.size();
        if (dataSize < 4) {
            return true;
        }
        for (int i = 0; i < dataSize - 4; i++) {
            float sum02 = (data.get(0).get(2) * (float)Math.cos(data.get(0).get(3)) +
                    data.get(2).get(2) * (float)Math.cos(data.get(2).get(3))) / 2;
            float sum13 = (data.get(1).get(2) * (float)Math.cos(data.get(1).get(3)) +
                    data.get(3).get(2) * (float)Math.cos(data.get(3).get(3))) / 2;
            if (Math.abs(sum02 - sum13) > NoiseLimit) {
                return true;
            }
        }
        return false;
    }

    private Map<Integer, List<String>> analyteSensorLabels = new TreeMap<Integer, List<String>>();
    private Map<Integer, List<Map<Integer, List<Float>>>> analyteSensors = new TreeMap<Integer, List<Map<Integer, List<Float>>>>();

    public Map<Integer, Map<Integer,Float>> getGroupAverage() {
        Map<Integer, Map<Integer, Float>> groupAverage = new TreeMap<Integer, Map<Integer, Float>>();
        Map<Integer, List<Byte>> analyteSensorMuxes =  new HashMap<Integer, List<Byte>>();
        List<String> labels = getLabelInUse();
        List<Byte> muxInUse = getMuxInUse();
        Map<Integer, List<Byte>> analyteSensor = ModelContainer.i.getAnalyteSensor();
        for (Map.Entry<Integer, List<Byte>> analyteSensorEntry : analyteSensor.entrySet()) {
            int analyte = analyteSensorEntry.getKey();
            for (Byte mux : analyteSensorEntry.getValue()) {
                if (muxInUse.contains(mux)) {
                    if (!analyteSensorMuxes.containsKey(analyte)) {
                        analyteSensorMuxes.put(analyte, new ArrayList<Byte>());
                    }
                    analyteSensorMuxes.get(analyte).add(mux);
                }
            }
        }
        for (Map.Entry<Integer, List<Byte>> analyteSensorMuxesEntry : analyteSensorMuxes.entrySet()) {
            int analyte = analyteSensorMuxesEntry.getKey();
            Log.d(TAG, "analyte: " + analyte);
            List<String> sensorLabels = new ArrayList<String>();
            for (Byte mux : analyteSensorMuxesEntry.getValue()) {
                if (muxInUse.contains(mux)) {
                    sensorLabels.add(muxLabelMap.get(mux));
                    SensorData sensorData = gmrData.getData(muxLabelMap.get(mux));
                    if (sensorData == null) {
                        Log.d(TAG, "##########sensorData is null");
                        continue;
                    }
                    Map < Integer, List < Float >> sensorSeriesData = sensorData.generateDataSeries();
                    List<Map<Integer, List<Float>>> analyteSensorsData; // List of sensor's series of the same analyte
                    if (analyteSensors.containsKey(analyte)) {
                        analyteSensorsData = analyteSensors.get(analyte);
                        analyteSensorsData.add(sensorSeriesData);
                    }
                    else {
                        analyteSensorsData = new ArrayList<Map<Integer, List<Float>>>();
                        analyteSensorsData.add(sensorSeriesData);
                        analyteSensors.put(analyte, analyteSensorsData);
                    }
                }
            }
            analyteSensorLabels.put(analyte, sensorLabels);
        }
        for (Map.Entry<Integer, List<Map<Integer, List<Float>>>> analyteSensorsEntry : analyteSensors.entrySet()) {
            int analyte = analyteSensorsEntry.getKey();
            List<Map<Integer, List<Float>>> sensorData = analyteSensorsEntry.getValue();
            Log.d(TAG, "analyte : " + analyte + ", number of sensors: " + sensorData.size());
            Map<Integer, SumData> sumDataMap = new TreeMap<Integer, SumData>();
            for (Map<Integer, List<Float>> data : sensorData) {
                for (Map.Entry<Integer, List<Float>> sensorMuxEntry : data.entrySet()) {
                    try {
                        List<Float> cycleData = sensorMuxEntry.getValue();
                        int cycle = Math.round(cycleData.get(1));
                        float mrMagnitude = cycleData.get(2);
                        float mrAngle = cycleData.get(3);
                        float centerToneImpedanceMagnitude = cycleData.get(5);
                        float centerToneImpedanceAngle = cycleData.get(6);
                        float centerToneImpedanceReal = centerToneImpedanceMagnitude * (float) Math.cos(centerToneImpedanceAngle);
                        float mrReal = 1000000 * mrMagnitude / centerToneImpedanceReal;
                        if (sumDataMap.containsKey(cycle)) {
                            SumData sumData = sumDataMap.get(cycle);
                            sumData.sum += mrReal;
                            sumData.count += 1;
                            sumDataMap.put(cycle, sumData);
                        } else {
                            SumData sumData = new SumData();
                            sumData.sum = mrReal;
                            sumData.count = 1;
                            sumDataMap.put(cycle, sumData);
                        }
                    }
                    catch (Exception ex) {
                        Log.d(TAG, null, ex);
                    }
                }
            }
            Map<Integer, Float> cycleAverage = new TreeMap<Integer, Float>();
            for (Map.Entry<Integer, SumData> sumDataEntry : sumDataMap.entrySet()) {
                SumData sumData = sumDataEntry.getValue();
                int cycle = sumDataEntry.getKey();
                float average = sumData.sum / sumData.count;
                cycleAverage.put(cycle, average);
            }
            groupAverage.put(analyte, cycleAverage);
        }
        return groupAverage;
    }

    private class SumData {
        public float sum = 0.0f;
        public int count = 0;
    }

    public void PostProcessing() {
//        Need the following variables to be set - get from assay printing assay control
//        MNPsInitialCycle
//        MNPsFinalCycle
//        N_initial
//        N_final
        try {
            AssayData assayData = ModelContainer.i.getAssayData();
            Map<Integer, AnalyteResult> analyteResults = new TreeMap<Integer, AnalyteResult>();

            Map<Integer, Map<Integer, Float>> groupAverage = getGroupAverage();
            Map<Integer, Float> referenceCycleAverage = groupAverage.get(0);
            for (Map.Entry<Integer, Map<Integer, Float>> groupAverageEntry : groupAverage.entrySet()) {
                int analyte = groupAverageEntry.getKey();
                Map<Integer, Float> cycleAverage = groupAverageEntry.getValue();
                List sensorLabels = analyteSensorLabels.get(analyte);
                Log.d(TAG, "analyte: " + analyte + ", " + sensorLabels.size() + " sensors: " + sensorLabels.toString());
                AnalyteResult analyteResult = new AnalyteResult();
                for (Map.Entry<Integer, Float> average : cycleAverage.entrySet()) {
                    int cycle = average.getKey();
                    float referenceCycleAverageValue;
                    if (referenceCycleAverage.containsKey(cycle)) {
                        referenceCycleAverageValue = referenceCycleAverage.get(cycle);
                        Log.d(TAG, "referenceCycleAverageValue: " + referenceCycleAverageValue);
                    } else {
                        referenceCycleAverageValue = 0.0f; // need to do interpolation to get value or skip
                        Log.d(TAG, "Error: Reference sensor average is missing data for this cycle");
                    }
                    float cycleAverageValue = average.getValue();
                    // zs - for now no subtraction
                    //cycleAverageValue -= referenceCycleAverageValue;
                    Log.d(TAG, "cycle: " + cycle + ", average: " + cycleAverageValue + ", reference average: " + referenceCycleAverageValue);
                    if (cycle > (MNPsInitialCycle - N_initial) && cycle <= MNPsInitialCycle) {
                        Log.d(TAG, "add MNPs initial cycle data");
                        analyteResult.addMNPsInitialData(cycleAverageValue);
                    }
                    if (cycle > (MNPsFinalCycle - N_final) && cycle <= MNPsFinalCycle) {
                        Log.d(TAG, "add MNPs final cycle data");
                        analyteResult.addMNPsFinalData(cycleAverageValue);
                    }
                }
                analyteResults.put(analyte, analyteResult);
            }
            for (Map.Entry<Integer, AnalyteResult> analyteResultEntry : analyteResults.entrySet()) {
                Log.d(TAG, "analyte: " + analyteResultEntry.getKey() + ", result: " + analyteResultEntry.getValue().getResult());
                analyteResult.put(analyteResultEntry.getKey(), analyteResultEntry.getValue().getResult());
            }
        }
        catch (Exception ex) {
            Log.e(TAG, ex.getMessage(), ex);
        }
    }

    private float getBSAResult(float rawData) {
        return rawData;
    }

    private float getIL_6Result(float rawData) {
        return rawData;
    }

    private float getHE_4Result(float rawData) {
        return rawData;
    }

    private float getCA_125Result(float rawData) {
        return rawData;
    }

    private float getFinalResult(int analyte, float rawData) {
        float ret = Float.MIN_VALUE;
        AssayData assayData =  ModelContainer.i.getAssayData();
        List<AssayData.Assay> assays = assayData.getAssays();
        for (AssayData.Assay assay : assays)
        {
            if (assay.getAnalyte() == analyte) {
                String assayName = assay.getName();
                switch (assayName) {
                    case "BSA":
                        ret = getBSAResult(rawData);
                        break;
                    case "IL-6":
                        ret = getIL_6Result(rawData);
                        break;
                    case "HE-4":
                        ret = getHE_4Result(rawData);
                        break;
                    case "CA-125":
                        ret = getCA_125Result(rawData);
                        break;
                    default:
                        break;
                }
            }
        }
        return ret;
    }

    public class AnalyteResult {
        private List<Float> MNPsInitialData = new ArrayList<Float>();
        private List<Float> MNPsFinalData = new ArrayList<Float>();
        public void addMNPsInitialData(float data) {
            MNPsInitialData.add(data);
        }
        public void addMNPsFinalData(float data) {
            MNPsFinalData.add(data);
        }
        private List<Float> averageInitialData = new ArrayList<Float>();
        private List<Float> averageFinalData = new ArrayList<Float>();
        public void addaverageInitialData(float data) {
            MNPsInitialData.add(data);
        }
        public void addaverageFinalData(float data) {
            MNPsFinalData.add(data);
        }
        public float getResult() {
            Log.d(TAG, "Not enough data. N_initial: " + N_initial + ", N_final: " + N_final);
            if (MNPsInitialData.size() < N_initial || MNPsFinalData.size() < N_final) {
                Log.d(TAG, "Not enough data. MNPs initial data size: " + MNPsInitialData.size() + ", MNPs final data size: " + MNPsFinalData.size());
                return -1.0f;
            }
            float initialDataSum = 0.0f;
            float finalDataSum = 0.0f;
            for (Float f : MNPsInitialData) {
                initialDataSum += f;
            }
            for (Float f : MNPsFinalData) {
                finalDataSum += f;
            }
            //return (initialDataSum / MNPsInitialData.size() + finalDataSum / MNPsFinalData.size()) / 2;
            return (finalDataSum / MNPsFinalData.size()) - (initialDataSum / MNPsInitialData.size());
        }
    }

    public Map<Integer, Float> analyteResult = new TreeMap<Integer, Float>();

}
