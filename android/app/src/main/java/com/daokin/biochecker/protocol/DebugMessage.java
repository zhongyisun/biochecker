package com.daokin.biochecker.protocol;

/**
 * Created by Lishun on 7/28/2016.
 */
public class DebugMessage extends GenericMessage {

    public DebugMessage(GenericMessage msg) {
        this.seqNum = msg.seqNum;
        this.payload = msg.payload.clone();
    }

    public String getText() {
        byte[] bytes = new byte[payload.length - 5];
        for(int n = 1; n < payload.length - 4; n++) {
            bytes[n - 1] = payload[n];
        }
        return new String(bytes);
    }

}