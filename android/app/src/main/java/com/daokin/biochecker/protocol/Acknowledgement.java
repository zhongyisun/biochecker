package com.daokin.biochecker.protocol;

import com.daokin.biochecker.protocol.PacketDefinitions.MessageSize;
import com.daokin.biochecker.protocol.PacketDefinitions.MessageType;


/**
 *
 */
public class Acknowledgement extends GenericMessage {
    
    public Acknowledgement(Byte[] vTag, short seqNum) {
        this.vTag = vTag;
        this.seqNum = seqNum;
        setHeaderFlag(((byte) PacketDefinitions.HeaderFlag.Ack.getValue()));
        payload = new Byte[6];
        writeMsgWindow(0x0001);
        writeByteWindow(0x00000404);
    }

    private void writeMsgWindow(int msgWindow) {
        write_unit16(0, msgWindow);
    }

    private void writeByteWindow(int byteWindow) {
        write_unit32(2, byteWindow);
    }

}
