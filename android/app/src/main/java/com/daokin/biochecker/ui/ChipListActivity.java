package com.daokin.biochecker.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.daokin.biochecker.model.AssayData;
import com.daokin.biochecker.model.InventoryChip;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.model.SharedApplicationData;
import com.daokin.biochecker.util.DBHelper;
import com.daokin.biochecker.util.RequestUtils;
import com.daokin.biochecker.util.Tools;
import com.daokin.biochecker.zxing.activity.CaptureActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChipListActivity extends Activity {

    private static final String TAG = "BioChecker";
    private DBHelper dbHelper = null;
    private ListView listView = null;
    private Button downloadBtn = null;
    private ActionBar actionBar = null;
    private Dialog loadDialog;
    private BaseAdapter adapter = null;
    private final int DELETE_MENU_BTN = 0x1;
    private List<InventoryChip> chips;
    private long lastClickedTime = 0;
    private long lastClickedId = -2L;
    private boolean hasClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chip_list_layout);
        dbHelper = new DBHelper(getApplicationContext());
        actionBar = getActionBar();
        actionBar.setTitle("Downloaded chip data");
        actionBar.setDisplayHomeAsUpEnabled(true);
        downloadBtn = (Button) findViewById(R.id.download_chip_btn);
        if (SharedApplicationData.isOffline()) {
            downloadBtn.setVisibility(View.GONE);
        } else {
            downloadBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ChipListActivity.this, CaptureActivity.class);
                    startActivityForResult(intent, 0);
                }
            });
        }
        if (getIntent().getExtras() != null) {
            Long chipId = getIntent().getLongExtra("chipId", 0L);
            Long chipAssayTypeId = getIntent().getLongExtra("typeId", 0L);
            String QCFailedSensor = getIntent().getStringExtra("QCFailedSensor");
            new GetChipAssayTypesTask().execute(chipId, chipAssayTypeId, QCFailedSensor);
        } else {
            new GetChipAssayTypesTask().execute();
        }
        listView = (ListView) findViewById(R.id.list_view);
        registerForContextMenu(listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long curTime = System.currentTimeMillis();
//                Log.d(TAG, hasClicked + " " + lastClickedId + " " + (curTime - lastClickedTime));
                if (hasClicked) {
                    return;
                }
                if (!hasClicked && (id != lastClickedId || curTime - lastClickedTime > 1000)) {
//                    Log.d(TAG, hasClicked + " " + lastClickedId + " " + (curTime - lastClickedTime));
                    lastClickedTime = curTime;
                    lastClickedId = id;
                    new GetAssayDataTask().execute(chips.get(position));
                }
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onDestroy() {
        if (loadDialog != null) {
            loadDialog.dismiss();
            loadDialog = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Long chipId = data.getLongExtra("chipId", 0L);
            Long typeId = data.getLongExtra("typeId", 0L);
            String QCFailedSensor = data.getStringExtra("QCFailedSensor");
            new GetChipAssayTypesTask().execute(chipId, typeId, QCFailedSensor);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, HomeActivity.class));
                this.finish();
                return true;
            case R.id.options_new_test:
                startActivity(new Intent(this, HomeActivity.class));
                this.finish();
                return true;
            case R.id.options_recent_tests:
                startActivity(new Intent(this, TestListActivity.class));
                this.finish();
                return true;
            case R.id.options_exit:
                Tools.showExitTips(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, DELETE_MENU_BTN, 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case DELETE_MENU_BTN:
                AdapterView.AdapterContextMenuInfo menuInfo =
                        (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                final int position = menuInfo.position;
                showDeleteTips(position);
                break;
            default:
                break;
        }
        return true;
    }

    private void showDeleteTips(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ChipListActivity.this)
                .setMessage("Are you sure to delete it?");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dbHelper.deleteInventoryChip(chips.get(position).getId())) {
                    Toast.makeText(ChipListActivity.this,
                            "Delete the chip success", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(ChipListActivity.this,
                            "Delete the chip failed", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
                new GetChipAssayTypesTask().execute();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }

    private void showLoadingDialog() {
        if (loadDialog == null) {
            loadDialog = new LoadingProgress(this, R.style.MyDialog);
            loadDialog.show();
        } else {
            loadDialog.show();
        }
    }

    private void hideLoadingDialog() {
        if (loadDialog != null) {
            loadDialog.dismiss();
        }
    }

    class GetChipAssayTypesTask extends AsyncTask<Object, Void, List<Map<String, Object>>> {
        private Long chipId;
        private ResponseModel<AssayData> model;

        @Override
        protected void onPreExecute() {
            showLoadingDialog();
        }

        @Override
        protected List<Map<String, Object>> doInBackground(Object... params) {
            if (!SharedApplicationData.isOffline() && params != null && params.length != 0) {
                chipId = (Long) params[0];
                Long chipAssayTypeId = (Long) params[1];
                String QCFailedSensor = (String) params[2];
                if (dbHelper.getChipCount(chipId) == 0) {
                    model = RequestUtils.getAssays(chipAssayTypeId);
                    if (model.getCode() != -1) {
                        InventoryChip type = new InventoryChip();
                        type.setId(chipId);
                        type.setAssayTypeId(chipAssayTypeId);
                        type.setAssayPrinting(new Gson().toJson(model.getData()));
                        type.setQCFailed(QCFailedSensor);
                        dbHelper.addInventoryChip(type);
                    }
                }
            }
            chips = dbHelper.getChips();
            List<Map<String, Object>> listItems = new ArrayList<>(chips.size());
            for (InventoryChip chip : chips) {
                Map<String, Object> map = new HashMap<>(2);
                map.put("id", chip.getId());
                map.put("QCFailed", getQCFailedSensor(chip.getQCFailed()));
                listItems.add(map);
            }
            return listItems;
        }

        @Override
        protected void onPostExecute(final List<Map<String, Object>> listItems) {
            hideLoadingDialog();
            if (model != null) {
                if (model.getCode() == -1) {
                    Toast.makeText(ChipListActivity.this, model.getMessage(),
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(ChipListActivity.this, "Downloaded chip data successfully!",
                            Toast.LENGTH_LONG).show();
                }
            } else if (chipId != null) {
                Toast.makeText(ChipListActivity.this, "The chip already exists!",
                        Toast.LENGTH_LONG).show();
            }
//            adapter = new SimpleAdapter(ChipListActivity.this, listItems, R.layout.chip_list_item,
//                    new String[]{"id", "QCFailed"}, new int[]{R.id.chip_id, R.id.QCFailed});
            adapter = new BaseAdapter() {
                @Override
                public int getCount() {
                    return listItems.size();
                }

                @Override
                public Object getItem(int position) {
                    return listItems.get(position);
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(final int position, View convertView, ViewGroup parent) {
                    ViewHolder holder;
                    if (convertView == null) {
                        convertView = LayoutInflater.from(ChipListActivity.this).
                                inflate(R.layout.chip_list_item, null);
                        holder = new ViewHolder();
                        holder.chipId = (TextView) convertView.findViewById(R.id.chip_id);
                        holder.QCFailed = (TextView) convertView.findViewById(R.id.QCFailed);
                        holder.chipIdLabel =
                                (TextView) convertView.findViewById(R.id.chip_id_label);
                        holder.QCFailedLabel =
                                (TextView) convertView.findViewById(R.id.QCFailed_label);
                        holder.delBtn = (Button) convertView.findViewById(R.id.delete_btn);
                        holder.delBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showDeleteTips(position);
                            }
                        });
                        convertView.setTag(holder);
                    } else {
                        holder = (ViewHolder) convertView.getTag();
                    }
                    InventoryChip chip = chips.get(position);
                    holder.chipId.setText(String.valueOf(chip.getId()));
                    holder.QCFailed.setText((String) listItems.get(position).get("QCFailed"));
                    return convertView;
                }
            };
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        //Get QC failed sensors
        private String getQCFailedSensor(String QCFailedSensor) {
            String[] helpArray = {"A", "B", "C", "D", "E", "F"};
            StringBuilder sb = new StringBuilder();
            if (QCFailedSensor != null && Tools.validateQCFailedSensorMark(QCFailedSensor)) {
                int len = QCFailedSensor.length();
                for (int i = 0; i < len; i += 2) {
                    byte b = Byte.valueOf(QCFailedSensor.substring(i, i + 2), 16);
                    for (int j = 1; j <= 8; j++) {
                        if ((b & 1) == 1) {
                            sb.append(helpArray[i / 2] + j + " ");
                        }
                        b >>= 1;
                    }
                }
            }
            return sb.toString();
        }

    }

    private class ViewHolder {
        public TextView chipIdLabel;
        public TextView QCFailedLabel;
        public Button delBtn;
        public TextView chipId;
        public TextView QCFailed;
    }

    private class GetAssayDataTask extends AsyncTask<InventoryChip, Void,
            List<Map<String, Object>>> {

        @Override
        protected void onPreExecute() {
            hasClicked = true;
            showLoadingDialog();
        }

        @Override
        protected List<Map<String, Object>> doInBackground(InventoryChip... params) {
            AssayData aData = params[0].getAssayData();
            List<Map<String, Object>> aps = new ArrayList<>();
            Map<Integer, String> printMap = new HashMap<>();
            for (AssayData.Assay assay : aData.getAssays()) {
                printMap.put(assay.getAnalyte(), "");
            }
            for (Map.Entry<String, AssayData.Sensor> entry : aData.getSensors().entrySet()) {
                AssayData.Sensor sensor = entry.getValue();
                int a = sensor.getAnalyte();
                printMap.put(a, printMap.get(a) + entry.getKey() + " ");
            }
            for (AssayData.Assay assay : aData.getAssays()) {
                Map<String, Object> map = new HashMap<>(aData.getAssays().size());
                map.put("name", assay.getName());
                map.put("printing", printMap.get(assay.getAnalyte()));
                map.put("duration", assay.getDuration());
                aps.add(map);
            }
            return aps;
        }

        @Override
        protected void onPostExecute(List<Map<String, Object>> assayPrintings) {
            hideLoadingDialog();
            if (!assayPrintings.isEmpty()) {
                activeListAlertDialog(assayPrintings);
            }
            hasClicked = false;
        }
    }

    private void activeListAlertDialog(List<Map<String, Object>> assayPrintings) {
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View layout = inflater.inflate(R.layout.assay_printing_list,
                (ViewGroup) findViewById(R.id.layout_assay_list));
        ListView assayListView = (ListView) layout.findViewById(R.id.assay_list_view);
        SimpleAdapter simpleAdapter = new SimpleAdapter(this, assayPrintings,
                R.layout.assay_pringting_list_item,
                new String[]{"name", "printing", "duration"},
                new int[]{R.id.assay_name, R.id.assay_printing, R.id.assay_duration});
        assayListView.setAdapter(simpleAdapter);
        new AlertDialog.Builder(ChipListActivity.this).setView(layout).create().show();
    }
}
