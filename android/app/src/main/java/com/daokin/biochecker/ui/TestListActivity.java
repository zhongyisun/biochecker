package com.daokin.biochecker.ui;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.daokin.biochecker.model.AssayData;
import com.daokin.biochecker.model.ChipDataTypeEnum;
import com.daokin.biochecker.model.InventoryChipData;
import com.daokin.biochecker.model.Location;
import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.model.ResultData;
import com.daokin.biochecker.model.ResultDetail;
import com.daokin.biochecker.model.SharedApplicationData;
import com.daokin.biochecker.model.Test;
import com.daokin.biochecker.protocol.DataOperation;
import com.daokin.biochecker.util.DBHelper;
import com.daokin.biochecker.util.RequestUtils;
import com.daokin.biochecker.util.Tools;
import com.daokin.biochecker.view.LocationListDialog;

public class TestListActivity extends Activity implements
        LoadListView.ILoadListener, LoadListView.IRefreshListener {
    private SimpleAdapter adapter = null;
    private List<HashMap<String, Object>> data = null;
    private LoadListView listView = null;
    private TextView tv_showuser = null;
    private final int DEFAULT_PAGE_SIZE = 20;
    private boolean isSearch = false;
    private Dialog loadDialog;
    private ActionBar mActionBar;
    private SearchView searchView;
    private String queryTerm;
    private static List<Location> locations;
    private LocationListDialog locationsDialog;
    private Button syncBtn;

    private int offlinePage = 0;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_list_layout);
        mActionBar = getActionBar();
        mActionBar.setTitle("My Recent Tests");
        mActionBar.setDisplayHomeAsUpEnabled(true);
        syncBtn = (Button) findViewById(R.id.sync_btn);
        syncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncTests();
            }
        });
        listView = (LoadListView) this.findViewById(R.id.test_listView);
        tv_showuser = (TextView) findViewById(R.id.tv_showuser);
        tv_showuser.setText("Hi, " + ModelContainer.i.getUser().getFirstName()
                + " " + ModelContainer.i.getUser().getLastName());

        listView.setOnItemClickListener(new OnItemClickListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position > adapter.getCount())
                    return;
                HashMap<String, Object> itemMap = (HashMap<String, Object>) listView
                        .getItemAtPosition(position);
                String testName = (String) itemMap.get("testName");
                String testDate = (String) itemMap.get("testDate");
                Long testId = (Long) itemMap.get("testId");
                Long chipId = (Long) itemMap.get("chipId");
                Integer state = (Integer) itemMap.get("state");
                if (chipId == null) {
                    chipId = 4L;
                }
                getTestDetail(testName, testDate, testId, chipId, state);
            }

        });
        dbHelper = new DBHelper(getApplicationContext());
        showLoadingDialog();
        getData("load", DEFAULT_PAGE_SIZE, 0, null);
    }

    public void showLoadingDialog() {
        if (loadDialog == null) {
            loadDialog = new LoadingProgress(this, R.style.MyDialog);
            loadDialog.show();
        } else {
            loadDialog.show();
        }
    }

    public void hideLoadingDialog() {
        if (loadDialog != null) {
            loadDialog.dismiss();
        }
    }

    public void showData() {
        adapter = new MySimpleAdapter(this, data, R.layout.test_item_layout,
                new String[]{"testName", "testDate", "sampleName"},
                new int[]{R.id.test_name, R.id.test_date, R.id.sample_name});
        listView.setAdapter(adapter);
        listView.setRefreshListener(this);
        listView.setLoadListener(this);
        adapter.notifyDataSetChanged();
    }

    public void getData(String status, int pageSize, int page, String term) {
        if (data == null) {
            data = new ArrayList<>();
        }
        AsyncTask<String, Void, Integer> loadDataTask =
                new AsyncTask<String, Void, Integer>() {
                    private String status;
                    private int pageSize;
                    private int page;
                    private String term;
                    private List<Test> results;
                    private Integer l;
                    private String msg;

                    @Override
                    protected Integer doInBackground(String... params) {
                        this.status = params[0];
                        this.pageSize = Integer.parseInt(params[1]);
                        this.page = Integer.parseInt(params[2]);
                        this.term = params[3];
                        results = new ArrayList<>();
                        int result = 1;
                        if (!isSearch) {
                            List<Test> tests = dbHelper.getTests(
                                    ModelContainer.i.getUser().getZeptoCustomerID());
                            results.addAll(tests);
                            int size = results.size();
                            if (size >= pageSize) {
                                offlinePage++;
                            }
                            l = results.size();
                        }
                        if (!SharedApplicationData.isOffline() && results.size() < pageSize) {
                            ResponseModel<List<Test>> model = null;
                            if (isSearch == true) {
                                model = RequestUtils.getTests(
                                        ModelContainer.i.getUser().getZeptoCustomerID(),
                                        pageSize, page - offlinePage);
                            } else {
                                model = RequestUtils.getTests(
                                        ModelContainer.i.getUser().getZeptoCustomerID(),
                                        pageSize, page - offlinePage);
                            }
                            if (model.getCode() != -1) {
                                results.addAll(model.getData());
                            } else {
                                result = -1;
                                msg = model.getMessage();
                            }
                        }
                        if ("refresh".equals(status)) {
                            data.clear();
                            offlinePage = 0;
                        }
                        for (int i = 0; i < results.size(); i++) {
                            Test test = results.get(i);
                            HashMap<String, Object> item = new HashMap<>();
                            item.put("testName", test.getName());
                            item.put("testDate", test.getCreatedTime());
                            Integer state = test.getState();
                            if (state != null && state == 1) {
                                item.put("testId", test.getOid());
                            } else {
                                item.put("testId", test.getId());
                            }
                            item.put("chipId", test.getChipId());
                            item.put("state", state);
                            item.put("sampleName", test.getSampleName());
                            data.add(item);
                        }
                        return result;
                    }

                    @Override
                    protected void onPostExecute(Integer result) {
                        hideLoadingDialog();
                        if (l != 0 && !SharedApplicationData.isOffline()) {
                            syncBtn.setVisibility(View.VISIBLE);
                        }
                        if (result == -1) {
                            Toast.makeText(TestListActivity.this, msg, Toast.LENGTH_LONG).show();
                        }
                        showData();
                        if ("refresh".equals(status)) {
                            listView.setCurrentPage(0);
                            listView.setQuery(term);
                            listView.reflashComplete();
                        } else if ("load".equals(status)) {
                            listView.setCurrentPage(page);
                            listView.setQuery(term);
                            listView.loadComplete();
                        }
                        if (results.size() == 0) {
                            if (page == 0) {
                                listView.loadComplete();
                                Toast.makeText(TestListActivity.this,
                                        "Not Found Data", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(TestListActivity.this,
                                        "No More Data", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                };
        loadDataTask.execute(new String[]{status, String.valueOf(pageSize),
                String.valueOf(page), term});
    }

    private void getLocations() {
        showLoadingDialog();
        if (locations != null) {
            hideLoadingDialog();
            locationsDialog =
                    new LocationListDialog(TestListActivity.this, TestListActivity.this, locations);
            locationsDialog.show();
            return;
        }
        AsyncTask<String, Void, ResponseModel<List<Location>>> loadDataTask =
                new AsyncTask<String, Void, ResponseModel<List<Location>>>() {

                    @Override
                    protected ResponseModel<List<Location>> doInBackground(
                            String... params) {
                        return RequestUtils.getLocations();
                    }

                    @Override
                    protected void onPostExecute(ResponseModel<List<Location>> result) {
                        hideLoadingDialog();
                        if (result == null || result.getCode() == -1) {
                            Toast.makeText(TestListActivity.this, result.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            if (result.getData() == null || result.getData().size() == 0) {
                                Toast.makeText(TestListActivity.this, "Not Location Data",
                                        Toast.LENGTH_LONG).show();
                            } else {
                                locations = result.getData();
                                if (locationsDialog != null) {
                                    locationsDialog.dismiss();
                                }
                                locationsDialog = new LocationListDialog(TestListActivity.this,
                                        TestListActivity.this, locations);
                                locationsDialog.show();
                            }
                        }
                    }
                };
        loadDataTask.execute(new String[]{});
    }

    @Override
    public void onRefresh(String query) {
        getData("refresh", DEFAULT_PAGE_SIZE, 0, query);
    }

    @Override
    public void onLoad(String query) {
        getData("load", DEFAULT_PAGE_SIZE, listView.getCurrentPage() + 1, query);
    }

    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_options_menu_bar, menu);
        MenuItem item = menu.findItem(R.id.options_recent_tests);
        searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        item.setVisible(false);
        if (searchView != null) {
            searchView.setQueryHint("Please type a term");
            if (isSearch) {
                searchView.setQuery(queryTerm, false);
            }
            searchView.setOnQueryTextListener(new OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    // to do search
                    queryTerm = query.trim();
                    onRefresh(query.trim());
                    isSearch = true;
                    showLoadingDialog();
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    if (isSearch) {
                        queryTerm = "";
                        onRefresh("");
                        isSearch = false;
                        showLoadingDialog();
                    }
                    return true;
                }
            });
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.options_new_test:
                startActivity(new Intent(TestListActivity.this, HomeActivity.class));
                finish();
                return true;
            case R.id.options_view_chip_data:
                startActivity(new Intent(this, ChipListActivity.class));
                finish();
                return true;
            case R.id.options_recent_tests:
                startActivity(new Intent(this, TestListActivity.class));
                finish();
                return true;
            case R.id.options_test_by_location_tests:
                getLocations();
                return true;
            case R.id.options_exit:
                Tools.showExitTips(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (loadDialog != null) {
            loadDialog.dismiss();
            loadDialog = null;
        }
        if (locationsDialog != null) {
            locationsDialog.dismiss();
            locationsDialog = null;
        }
    }

    private void syncTests() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                String customerId = ModelContainer.i.getUser().getZeptoCustomerID();
                List<Test> tests = dbHelper.getTests(customerId);
                ResponseModel<Long> model;
                ResponseModel<String> model2;
                for (Test test : tests) {
                    Long chipId = test.getChipId();
                    model = RequestUtils.createTest(test.getName(), test.getDescription(),
                            customerId, test.getSampleName(), chipId);
                    if (model.getCode() != -1) {
                        InventoryChipData chipData = dbHelper.getChipData(chipId, 7);
                        if (chipData != null) {
                            model2 = RequestUtils.saveResultData(chipId, chipData.getData());
                            if (model2.getCode() != -1) {
                                chipData = dbHelper.getChipData(chipId, 5);
                                if (chipData != null) {
                                    model2 = RequestUtils.saveGMRData(chipId, chipData.getData());
                                    if (model2.getCode() != -2) {
                                        Long testId = model.getData();
                                        test.setOid(testId);
                                        test.setState(1);
                                        dbHelper.updateTest(test);
                                    }
                                }
                            }
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                getData("load", DEFAULT_PAGE_SIZE, 0, null);
            }
        }.execute();
    }

    private class MySimpleAdapter extends SimpleAdapter {

        /**
         * Constructor
         *
         * @param context  The context where the View associated with this SimpleAdapter is running
         * @param data     A List of Maps. Each entry in the List corresponds to one row in the list. The
         *                 Maps contain the data for each row, and should include all the entries specified in
         *                 "from"
         * @param resource Resource identifier of a view layout that defines the views for this list
         *                 item. The layout file should include at least those named views defined in "to"
         * @param from     A list of column names that will be added to the Map associated with each
         *                 item.
         * @param to       The views that should display column in the "from" parameter. These should all be
         *                 TextViews. The first N views in this list are given the values of the first N columns
         */
        public MySimpleAdapter(Context context, List<? extends Map<String, ?>> data, int resource,
                               String[] from, int[] to) {
            super(context, data, resource, from, to);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView tv = (TextView) view.findViewById(R.id.test_name);
            Map<String, Object> map = (Map<String, Object>) getItem(position);
            Integer state = (Integer) map.get("state");
            if (state != null && state == 0) { // state=0 is offline tests
                tv.setTextColor(getResources().getColor(R.color.green));
            }
            return view;
        }
    }

//    private void getTestDetail(final String testName, final String testDate,
//                               final long testId, final long chipId, final Integer state) {
//        AsyncTask<String, Void, Integer> loadDataTask =
//                new AsyncTask<String, Void, Integer>() {
//                    String msg = null;
//                    HashMap<String, Float> resultMap = null;
//
//                    @Override
//                    protected void onPreExecute() {
//                        showLoadingDialog();
//                    }
//
//                    @Override
//                    protected Integer doInBackground(String... params) {
//                        AssayData assayData;
//                        ResultData resultData;
//                        if (!SharedApplicationData.isOffline() && (state == null || state != 0)) {
//                            ResponseModel<ResultDetail> model = RequestUtils.getTest(testId);
//                            if (model.getCode() != -1) {
//                                ResultDetail detail = model.getData();
//                                assayData = detail.getAssay_printing();
//                                resultData = detail;
//                            } else {
//                                msg = model.getMessage();
//                                return -1;
//                            }
//                        } else {
//                            resultData = dbHelper.getTestResult(testId, ChipDataTypeEnum.OVARIANCANCER.getValue());
//                            assayData = dbHelper.getAssayData(chipId);
//                            if (resultData == null) {
//                                msg = "No test result record";
//                                return -1;
//                            }
//                        }
//                        if (resultData != null) {
//                            resultMap = buildResultMap(resultData, assayData);
//                            return 1;
//                        }
//                        msg = "Some errors occur";
//                        return -1;
//                    }
//
//                    @Override
//                    protected void onPostExecute(Integer integer) {
//                        hideLoadingDialog();
//                        if (resultMap != null) {
//                            Intent intent = new Intent(TestListActivity.this,
//                                    TestDetailActivity.class);
//                            intent.putExtra("testName", testName);
//                            intent.putExtra("testDate", testDate);
//                            intent.putExtra("result", resultMap);
//                            startActivity(intent);
//                        } else {
//                            Toast.makeText(TestListActivity.this, msg, Toast.LENGTH_LONG).show();
//                        }
//                    }
//                };
//        loadDataTask.execute();
//    }

    private void getTestDetail(final String testName, final String testDate,
                               final long testId, final long chipId, final Integer state) {
        AsyncTask<String, Void, Integer> loadDataTask =
                new AsyncTask<String, Void, Integer>() {
                    String msg = null;
                    HashMap<String, String> resultMap = null;

                    @Override
                    protected void onPreExecute() {
                        showLoadingDialog();
                    }

                    @Override
                    protected Integer doInBackground(String... params) {
                        AssayData assayData;
                        ResultData resultData;
                        if (!SharedApplicationData.isOffline() && (state == null || state != 0)) {
                            ResponseModel<ResultDetail> model = RequestUtils.getTest(testId);
                            if (model.getCode() != -1) {
                                ResultDetail detail = model.getData();
                                assayData = detail.getAssay_printing();
                                resultData = detail;
                            } else {
                                msg = model.getMessage();
                                return -1;
                            }
                        } else {
                            resultData = dbHelper.getTestResult(testId, ChipDataTypeEnum.OVARIANCANCER.getValue());
                            assayData = dbHelper.getAssayData(chipId);
                            if (resultData == null) {
                                msg = "No test result record";
                                return -1;
                            }
                        }
                        if (resultData != null) {
                            resultMap = buildResultMap(resultData, assayData);
                            return 1;
                        }
                        msg = "Some errors occur";
                        return -1;
                    }

                    @Override
                    protected void onPostExecute(Integer integer) {
                        hideLoadingDialog();
                        if (resultMap != null) {
                            Intent intent = new Intent(TestListActivity.this,
                                    TestDetailActivity.class);
                            intent.putExtra("testName", testName);
                            intent.putExtra("testDate", testDate);
                            intent.putExtra("result", resultMap);
                            startActivity(intent);
                        } else {
                            Toast.makeText(TestListActivity.this, msg, Toast.LENGTH_LONG).show();
                        }
                    }
                };
        loadDataTask.execute();
    }

    // zs working
//    private HashMap<String, Float> buildResultMap(ResultData rData, AssayData aData) {
//        List<AssayData.Assay> assays = aData.getAssays();
//        HashMap<String, Float> map = new HashMap<>(assays.size());
//        Map<Integer, Float> anaRltMap = new HashMap<>(assays.size());
//        for (ResultData.AnalyteResult result : rData.getAssay()) {
//            float sum = 0f;
//            for (Float f : result.getResult()) {
//                sum += f;
//            }
//            anaRltMap.put(result.getAnalyte(), sum / result.getResult().size());
//        }
//        for (AssayData.Assay assay : assays) {
//            map.put(assay.getName(), anaRltMap.get(assay.getAnalyte()));
//        }
//        return map;
//    }

    private HashMap<String, String> buildResultMap(ResultData rData, AssayData aData) {
        List<AssayData.Assay> assays = aData.getAssays();
        HashMap<String, String> map = new HashMap<>(assays.size());
        Map<Integer, Float> anaRltMap = new HashMap<>(assays.size());
        for (ResultData.AnalyteResult result : rData.getAssay()) {
            float sum = 0f;
            for (Float f : result.getResult()) {
                sum += f;
            }
            anaRltMap.put(result.getAnalyte(), sum / result.getResult().size());
        }
        for (AssayData.Assay assay : assays) {
            // Skip BSA as analyte 0
            if (assay.getAnalyte() > 0) {
                try {
                    if (anaRltMap.containsKey(assay.getAnalyte())) {
                        float value = anaRltMap.get(assay.getAnalyte());
                        DecimalFormat myFormatter = new DecimalFormat("###.######");
                        String output = myFormatter.format(value);
                        map.put(assay.getName(), output + "\t" + assay.getUnit());
                    }
                }
                catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        }
        return map;
    }
}
