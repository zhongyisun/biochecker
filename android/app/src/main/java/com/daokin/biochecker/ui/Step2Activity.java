package com.daokin.biochecker.ui;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ProcessedResult.TestSample.SampleType;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.model.SharedApplicationData;
import com.daokin.biochecker.model.Test;
import com.daokin.biochecker.util.DBHelper;
import com.daokin.biochecker.util.DateUtil;
import com.daokin.biochecker.util.RequestUtils;

public class Step2Activity extends Activity {

    private Button createtest_btn = null;
    private TextView tv_home = null;
    private TextView tv_cancel = null;
    private TextView chipid_tv = null;
    private EditText text_sampleid = null;
    private Spinner spinner = null;
    private EditText testNameText = null;
    private Map<String, Integer> sampleTypeMap = null;
    private DBHelper dbHelper = new DBHelper(this);
    private ProgressDialog processingDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step2_layout);
        text_sampleid = (EditText) findViewById(R.id.text_sampleid);
        text_sampleid.setText("sample001");
        createtest_btn = (Button) findViewById(R.id.createtest_btn);
        chipid_tv = (TextView) findViewById(R.id.chipid_tv);
        chipid_tv.setText("Chip ID: " + ModelContainer.i.getChipId()
                + "\n" + ModelContainer.i.getAssayNames());
        testNameText = (EditText) findViewById(R.id.test_name);
        createtest_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String clientSampleId = text_sampleid.getText().toString();
                String testName = testNameText.getText().toString();
                if (clientSampleId == null || clientSampleId.trim().equals("")) {
                    text_sampleid.setFocusable(true);
                    Toast.makeText(Step2Activity.this,
                            "Please enter sample id", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (testName == null || testName.trim().equals("")) {
                    testNameText.setFocusable(true);
                    Toast.makeText(Step2Activity.this,
                            "Please enter test name.", Toast.LENGTH_SHORT).show();
                    return;
                }
                createtest_btn.setEnabled(false);
                String sampleName = spinner.getSelectedItem().toString();
                processingDialog.show();
                createTest.execute(testName, sampleName);
            }
        });

        processingDialog = new ProgressDialog(this);
        processingDialog.setTitle("Creating test");
        processingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        processingDialog.setCancelable(false);
        processingDialog.setCanceledOnTouchOutside(false);
        processingDialog.setMessage("Please wait, this will take a while...");

        tv_home = (TextView) findViewById(R.id.tv_home);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        tv_home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Step2Activity.this.finish();
            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Step2Activity.this.finish();
            }
        });
        tv_home.setVisibility(View.INVISIBLE);
        tv_cancel.setVisibility(View.INVISIBLE);
        List<SampleType> list = ModelContainer.i.getSampleTypes();
        sampleTypeMap = new HashMap<>();
        String[] sa = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            SampleType st = list.get(i);
            sa[i] = st.getName();
            sampleTypeMap.put(st.getName(), st.getId());
        }
        ArrayAdapter<String> sAdapter = new SpinnerArrayAdapter(this, sa);
        sAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner = (Spinner) findViewById(R.id.text_sampletype);
        spinner.setAdapter(sAdapter);
    }


    @Override
    protected void onDestroy() {
        if (processingDialog != null && processingDialog.isShowing()) {
            processingDialog.dismiss();
        }
        super.onDestroy();
    }

    private class SpinnerArrayAdapter extends ArrayAdapter<String> {
        private Context mContext;
        private String[] mStringArray;

        public SpinnerArrayAdapter(Context context, String[] stringArray) {
            super(context, android.R.layout.simple_spinner_item, stringArray);
            mContext = context;
            mStringArray = stringArray;
        }

//		@Override
//		public View getDropDownView(int position, View convertView,
//				ViewGroup parent) {
//			// 修改Spinner展开后的字体颜色
//			if (convertView == null) {
//				LayoutInflater inflater = LayoutInflater.from(mContext);
//				convertView = inflater.inflate(
//						android.R.layout.simple_spinner_dropdown_item, parent,
//						false);
//			}
//
//			// 此处text1是Spinner默认的用来显示文字的TextView
//			TextView tv = (TextView) convertView
//					.findViewById(android.R.id.text1);
//			tv.setText(mStringArray[position]);
//			tv.setTextColor(Color.DKGRAY);
//
//			return convertView;
//
//		}

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // 修改Spinner选择后结果的字体颜色
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(
                        android.R.layout.simple_spinner_item, parent, false);
            }

            // 此处text1是Spinner默认的用来显示文字的TextView
            TextView tv = (TextView) convertView
                    .findViewById(android.R.id.text1);
            tv.setText(mStringArray[position]);
            tv.setTextColor(Color.DKGRAY);
            return convertView;
        }
    }

    private AsyncTask<String, Void, Test> createTest = new AsyncTask<String, Void, Test>() {

        private ResponseModel<Long> responseModel = null;

        @Override
        protected Test doInBackground(String... params) {
            String testName = params[0];
            String sampleName = params[1];
            Test test = new Test();
            test.setState(0);
            if (!SharedApplicationData.isOffline()) {
                responseModel = RequestUtils.createTest(testName, "",
                        ModelContainer.i.getUser().getZeptoCustomerID(),
                        sampleName, ModelContainer.i.getChipId());
                if (responseModel.getCode() != -1) {
                    Long testOid = responseModel.getData();
                    test.setState(1);
                    test.setOid(testOid);
                }
            }
            test.setSampleName(sampleName);
            test.setName(testName);
            test.setDescription("");
            test.setChipId(ModelContainer.i.getChipId());
            test.setCreatedTime(DateUtil.formatDate(Calendar.getInstance().getTime()));
            dbHelper.addTest(test);
            return test;
        }

        @Override
        protected void onPostExecute(Test test) {
            if (responseModel != null && responseModel.getCode() == -1) {
                Toast.makeText(Step2Activity.this,
                        responseModel.getMessage(), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(Step2Activity.this,
                        "Test is created successfully", Toast.LENGTH_SHORT).show();
                ModelContainer.i.setTest(test);
                Intent intent = new Intent(Step2Activity.this, ScreenSensorActivity.class);
                startActivity(intent);
                Step2Activity.this.finish();
            }
        }
    };
}
