package com.daokin.biochecker.model;

public class ZeptoAPIException extends Exception {

	public ZeptoAPIException() {
		super();
	}

	public ZeptoAPIException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public ZeptoAPIException(String detailMessage) {
		super(detailMessage);
	}

	public ZeptoAPIException(Throwable throwable) {
		super(throwable);
	}
}
