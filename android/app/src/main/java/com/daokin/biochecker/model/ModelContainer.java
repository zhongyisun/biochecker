package com.daokin.biochecker.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.bluetooth.BluetoothDevice;

import com.daokin.biochecker.model.ProcessedResult.TestChip.AssayPrinting;
import com.daokin.biochecker.model.ProcessedResult.TestSample.SampleType;
import com.daokin.biochecker.model.User.UserRole;

public class ModelContainer {

    public static ModelContainer i = new ModelContainer();

    private String BTName;
    private User user;
    private Map<String, Object> map = new HashMap<String, Object>();
    private int testID;
    private int rawResultID;
    private Long chipId;
    private BluetoothDevice device;
    private List<AssayPrinting> assayPrintings;
    private List<SampleType> sampleTypes;
    private List<UserRole> userRoles;
    private List<Byte> reference;
    private List<AssayData.Sensor> referenceSensorList;
    private Test test;
    private AssayData assayData;
    private Long chipAssayTypeId;
    private List<Byte> qcFailedSensors;
    private Map<String, Byte> labelInUseMap;
    private Map<Byte, String> muxInUseMap;
    private Map<String, Byte> labelSensorMap;
    private Map<Byte, String> sensorLabelMap;
    private  Map<String, Byte> labelMux1Map;
    private  Map<Byte, String> mux1LabelMap;
    private  Map<String, Byte> labelMux2Map;
    private int maxDuration;
    private String mux2Label;

    public int getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
    }

    public String getBTName() {
        return BTName;
    }

    public void setBTName(String BTName) {
        this.BTName = BTName;
    }

    public Map<String, Byte> getLabelSensorMap() {
        return labelSensorMap;
    }

    public void setLabelSensorMap(Map<String, Byte> labelSensorMap) {
        this.labelSensorMap = labelSensorMap;
    }

    public Map<Byte, String> getSensorLabelMap() {
        return sensorLabelMap;
    }

    public void setSensorLabelMap(Map<Byte, String> sensorLabelMap) {
        this.sensorLabelMap = sensorLabelMap;
    }

    public List<Byte> getQcFailedSensors() {
        return qcFailedSensors;
    }

    public void setQcFailedSensors(List<Byte> qcFailedSensors) {
        this.qcFailedSensors = qcFailedSensors;
    }

    public Map<String, Byte> getLabelInUseMap() {
        return labelInUseMap;
    }

    public void setLabelInUseMap(Map<String, Byte> labelInUseMap) {
        this.labelInUseMap = labelInUseMap;
    }

    public Map<Byte, String> getMuxInUseMap() {
        return muxInUseMap;
    }

    public void setMuxInUseMap(Map<Byte, String> muxInUseMap) {
        this.muxInUseMap = muxInUseMap;
    }

    public Long getChipAssayTypeId() {
        return chipAssayTypeId;
    }

    public void setChipAssayTypeId(Long chipAssayTypeId) {
        this.chipAssayTypeId = chipAssayTypeId;
    }

    public AssayData getAssayData() {
        return assayData;
    }

    public void setAssayData(AssayData assayData) {
        this.assayData = assayData;
    }

//    public List<Byte> getReference() {
//        return reference;
//    }
//
//    public void setReference(List<Byte> reference) {
//        this.reference = reference;
//    }

    public Map<Byte, String> getMux1LabelMap() {
        return mux1LabelMap;
    }

    public void setMux1LabelMap(Map<Byte, String> mux1LabelMap) {
        this.mux1LabelMap = mux1LabelMap;
    }

    public Map<String, Byte> getLabelMux1Map() {
        return labelMux1Map;
    }

    public void setLabelMux1Map(Map<String, Byte> labelMux1Map) {
        this.labelMux1Map = labelMux1Map;
    }

    public Map<String, Byte> getLabelMux2Map() {
        return labelMux2Map;
    }

    public void setLabelMux2Map(Map<String, Byte> labelMux2Map) {
        this.labelMux2Map = labelMux2Map;
    }

    public List<AssayData.Sensor> getReferenceSensorList() {
        return referenceSensorList;
    }

    public void setReferenceSensorList(List<AssayData.Sensor> referenceSensorList) {
        this.referenceSensorList = referenceSensorList;
    }

    public List<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public List<SampleType> getSampleTypes() {
        return sampleTypes;
    }

    public void setSampleTypes(List<SampleType> sampleTypes) {
        this.sampleTypes = sampleTypes;
    }

    public List<AssayPrinting> getAssayPrintings() {
        return assayPrintings;
    }

    public void setAssayPrintings(List<AssayPrinting> assayPrintings) {
        this.assayPrintings = assayPrintings;
    }

    public Long getChipId() {
        return chipId;
    }

    public void setChipId(Long chipId) {
        this.chipId = chipId;
    }


    public BluetoothDevice getDevice() {
        return device;
    }

    public void setDevice(BluetoothDevice device) {
        this.device = device;
    }

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public int getRawResultID() {
        return rawResultID;
    }

    public void setRawResultID(int rawResultID) {
        this.rawResultID = rawResultID;
    }

    public void put(String key, Object value) {
        map.put(key, value);
    }

    public Object get(String key) {
        return map.get(key);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAssayNames() {
        if (ModelContainer.i.getAssayData() != null) {
            List<AssayData.Assay> assays = ModelContainer.i.getAssayData().getAssays();
            StringBuilder sb = new StringBuilder();
            for (AssayData.Assay assay : assays) {
                sb.append(assay.getName() + " ");
            }
            return sb.toString();
        }
        return "";
    }

    public Map<Integer, List<Byte>> getAnalyteSensor() {
        Map<Integer, List<Byte>> map = new HashMap<>();
        AssayData aData = ModelContainer.i.getAssayData();
        if (aData != null) {
            for (Map.Entry<Integer, List<String>> entry : aData.getAnalyteSensorMap().entrySet()) {
                List<String> list = entry.getValue();
                map.put(entry.getKey(), new ArrayList<Byte>(list.size()));
                for (String str : list) {
                    map.get(entry.getKey()).add(labelMux1Map.get(str));
                }
            }
        }
        return map;
    }
}
