package com.daokin.biochecker.ui;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daokin.biochecker.model.AssayData;
import com.daokin.biochecker.model.ChipDataTypeEnum;
import com.daokin.biochecker.model.GMRData;
import com.daokin.biochecker.model.ResultData;
import com.daokin.biochecker.model.InventoryChipData;
import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ProcessedResult.TestChip.AssayPrinting;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.model.SensorData;
import com.daokin.biochecker.model.SharedApplicationData;
import com.daokin.biochecker.protocol.DataOperation;
import com.daokin.biochecker.protocol.ProtocolHandler;
import com.daokin.biochecker.protocol.StartCommand;
import com.daokin.biochecker.util.DBHelper;
import com.daokin.biochecker.util.DateUtil;
import com.daokin.biochecker.util.RequestUtils;
import com.google.gson.Gson;

public class ScreenSensorActivity extends Activity {

    private static final String TAG = "BioChecker";
    private Map<String, List> assayPrintingMap = null;
    private TextView tv_home = null;
    private Button continue_btn = null;
    private TextView tv_cancel = null;
    private TextView notice_tv = null;
    private TextView tv_testid = null;
    private AlertDialog alertDialog = null;
    private ImageView indicator = null;
    private ImageView noticeImage = null;
    private ProgressDialog processingDialog = null;
    boolean isConnect = false;

    private String displayedMessage = "";

    private UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private BluetoothSocket clientSocket;

    private BluetoothDevice device = null;

    private ProtocolHandler pHandler = null;

    private Set<String> printingSensors = null;

    private int screenDuration = 60;
    private int intervalGap = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_sensor);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        Log.d(TAG, "ScreenSensorActivity starting...");

        indicator = (ImageView) findViewById(R.id.indicator);
        noticeImage = (ImageView) findViewById(R.id.notice_image);

        tv_home = (TextView) findViewById(R.id.tv_home);
        notice_tv = (TextView) findViewById(R.id.notice_tv);
        tv_testid = (TextView) findViewById(R.id.tv_testid);

        Long testId =  ModelContainer.i.getTest().getOid();
        if (testId == null) {//offline only
            testId = ModelContainer.i.getTest().getId();
        }
        System.out.println("oid:" + ModelContainer.i.getTest().getOid() + ", id:" + ModelContainer.i.getTest().getId() );
        tv_testid.setText("TEST ID: " + testId);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        tv_home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ScreenSensorActivity.this.finish();
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ScreenSensorActivity.this.finish();
            }
        });
        tv_home.setVisibility(View.INVISIBLE);
        tv_cancel.setVisibility(View.INVISIBLE);

        LinearLayout linearLayout = new LinearLayout(this);
        TextView tv_title = new TextView(this);
        TextView tv_date = new TextView(this);
        tv_title.setText("Test 001");
        tv_title.setTextSize(18);
        tv_date.setText("2014-11-11 11:11:11");
        tv_date.setTextSize(12);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(20, 10, 0, 10);
        linearLayout.addView(tv_title);
        linearLayout.addView(tv_date);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Your test result are ready");
        builder.setView(linearLayout);
        builder.setPositiveButton("VIEW TESTS", new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(ScreenSensorActivity.this,
                        TestListActivity.class));
                ScreenSensorActivity.this.finish();
            }
        });

        alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);

        continue_btn = (Button) findViewById(R.id.continue_btn);
        continue_btn.setClickable(false);
        continue_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "Before openBluetooth");
                device = ModelContainer.i.getDevice();
                openBluetooth();
                Log.d(TAG, "After openBluetooth");
                if (!isConnect) {
                    Toast.makeText(ScreenSensorActivity.this, "Not connect", Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }
                noticeImage.setImageDrawable(getResources().getDrawable(R.drawable.step4image));
                notice_tv.setVisibility(View.INVISIBLE);
                noticeImage.postInvalidate();
                notice_tv.postInvalidate();
                continue_btn.setText("Starting Base Station");
                continue_btn.setEnabled(false);
                continue_btn.postInvalidate();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        processingDialog.setTitle("Screening reference sensors");
                        displayedMessage = "Please wait, this will take " + screenDuration + " seconds...";
                        processingDialog.setMessage(displayedMessage + "\r\n\r\n ");
                        new ScreenHandlerTask().execute();
                    }
                }, 1000);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pHandler.sendStopCommand();
                        Log.d(TAG, "uiScreenHandler:pHandler stopped.");
                        processingDialog.setTitle("Preparing to process sample");
                        processingDialog.setMessage("Please wait...");
                        ScreenSensors();
                        pHandler.stopHandler();
                        Intent i = new Intent(ScreenSensorActivity.this, TestSampleActivity.class);
                        startActivity(i);
                        finish();
                    }
                }, screenDuration * 1000 + 1000); // zs
            }
        });

        processingDialog = new ProgressDialog(this);
        //processingDialog.setTitle("Start Test");
        processingDialog.setTitle("Processing Sample");
        processingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        processingDialog.setCancelable(false);
        processingDialog.setCanceledOnTouchOutside(false);
        //processingDialog.setMessage("Please wait, this will take several minutes...");
        processingDialog.setMessage("Please wait...");

        List<AssayPrinting> assayPrintingsList = ModelContainer.i.getAssayPrintings();
        List<String> assayNameList = new ArrayList<>();
        List<String> printingList = new ArrayList<>();
        for (Iterator it = assayPrintingsList.iterator(); it.hasNext(); ) {
            AssayPrinting ap = (AssayPrinting) it.next();
            String str = ap.getAssay().getName();
            assayNameList.add(str);
            printingList.add(ap.getPrinting());
        }
        assayPrintingMap = new HashMap<>();
        for (int i = 0; i < assayNameList.size(); i++) {
            String printingMark = assayNameList.get(i);
            String printingSensors = printingList.get(i);
            List<String> printingSensorList = null;
            if (printingSensors == null) {
                printingList = new ArrayList<>(0);
            } else {
                printingSensorList = Arrays.asList(printingSensors.split(","));
            }
            assayPrintingMap.put(printingMark, printingSensorList);
        }
    }

    @Override
    protected void onDestroy() {
        if (pHandler != null) {
            pHandler.stopHandler();
        }
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
        super.onDestroy();
    }

    private Handler uiScreenHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String strMsg = (String) msg.obj;

            if (!strMsg.startsWith("Checksum error") && !strMsg.startsWith("Elapsed time")) {
                Toast.makeText(ScreenSensorActivity.this, strMsg, Toast.LENGTH_LONG).show();
            }

            if (strMsg.equals("Start succeeded")) {
                indicator.setImageResource(R.drawable.step4indicator);
                processingDialog.show();
            } else if (strMsg.startsWith("Elapsed time")) {
                processingDialog.setMessage(displayedMessage + "\r\n\r\n" + strMsg);
            } else if ("NoChip".equals(strMsg)) {
                processingDialog.dismiss();
                notice_tv.setText("No Chip found, "
                        + "Please check your chip device and try again.");
            } else if (strMsg.startsWith("Checksum error")) {
                //notice_tv.setText("Checksum error");
                processingDialog.setMessage("Please wait, this " +
                        "will take several minutes...\r\n\r\n" + strMsg);
            } else if (strMsg.startsWith("Error")) {
                processingDialog.dismiss();
                Toast.makeText(ScreenSensorActivity.this, "Some error occurs.", Toast.LENGTH_LONG);
                Intent intent = new Intent(ScreenSensorActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            } else if (strMsg.equals("Unlinked")) {
                Toast.makeText(ScreenSensorActivity.this, "Some error occurs in the linking process.",
                        Toast.LENGTH_LONG);
                Intent intent = new Intent(ScreenSensorActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }
    };

    private class ScreenHandlerTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (isConnect) {
                    try {
                        pHandler = new ProtocolHandler(clientSocket, uiScreenHandler, true);
                        printingSensors = new HashSet<>();
                        for (AssayPrinting ap : ModelContainer.i
                                .getAssayPrintings()) {
                            String printing = ap.getPrinting();
                            if (printing.length() > 1) {
                                printingSensors.addAll(Arrays.asList(printing.split(",")));
                            }
                        }
                        DataOperation dataOperation = pHandler.getDataOperation();
                        Log.d(TAG, "Scan electrical reference sensors...");
                        //Log.d(TAG, "printing sensors :" + printingSensors);
                        dataOperation.setPrintingSensors(printingSensors);
                        //Log.d(TAG, "reference: " + ModelContainer.i.getReference());
                        //dataOperation.setRefSensors(ModelContainer.i.getReference());
                        pHandler.start(getApplicationContext());
                        Log.d(TAG, "Starting protocol handler...");
                    } catch (IOException e) {
                        Log.e(TAG, "", e);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "", e);
            }
            return null;
        }

    }

    public void openBluetooth() {
        try {
            if (device == null) {
                Toast.makeText(ScreenSensorActivity.this, "Can not find any device.",
                        Toast.LENGTH_LONG).show();
                return;
            }

            if (clientSocket == null) {
                clientSocket = device
                        .createRfcommSocketToServiceRecord(MY_UUID);
                try {
                    clientSocket.connect();
                    isConnect = true;
                } catch (Exception e) {
                    Log.d(TAG, "trying fallback");
                    try {
                        clientSocket = (BluetoothSocket) device
                                .getClass()
                                .getMethod("createRfcommSocket", new Class[]{int.class})
                                .invoke(device, 1);
                        clientSocket.connect();
                        isConnect = true;
                    } catch (Exception ex) {
                        Log.e(TAG, "clientSocket exception", ex);
                    }
                }
            }
            else {
                if (!clientSocket.isConnected()) {
                    clientSocket.connect();
                    isConnect = true;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    private void ScreenSensors() {

        Log.d(TAG, "ScreenSensors");
        DataOperation dataOperation = pHandler.getDataOperation();
        GMRData gmrData = dataOperation.getGMRData();
        Map<String, SensorData> sensorData = gmrData.getData();
        for (Map.Entry<String, SensorData> entry : sensorData.entrySet()) {
            String key = entry.getKey();
            SensorData value = entry.getValue();
            List<List<Float>> dataList = value.getData();
            boolean flagged = false;
            for (List<Float> floats : dataList) {
                if (floats.get(4) != 0.0f) {
                    Log.d(TAG, "Sample data has flag");
                    flagged = true;
                    break;
                }
            }
            if (flagged) {
                break;
            }
            if (dataOperation.checkNoise(dataList)) {
                break;
            }
            // good reference sensor found
            Log.d(TAG, "Set electrical reference: " + key);
            dataOperation.setElectricalReferenceSensor(key);
            break;
        }
    }
}
