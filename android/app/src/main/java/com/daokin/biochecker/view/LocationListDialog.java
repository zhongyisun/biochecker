package com.daokin.biochecker.view;

import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daokin.biochecker.model.Location;
import com.daokin.biochecker.ui.R;
import com.daokin.biochecker.ui.TestListActivity;
import com.daokin.biochecker.ui.TestLocationListActivity;

public class LocationListDialog extends Dialog {

	private Context context;
	private Activity mActivity;
	private List<Location> datas;
	private LinearLayout listView;

	public LocationListDialog(Context context, Activity mActivity,
			List<Location> locations) {
		super(context, R.style.constom_dialog_style);
		this.context = context;
		this.datas = locations;
		this.mActivity = mActivity;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.list_item_dialog_content);
		listView = (LinearLayout) findViewById(R.id.list_view);
		for (Location location : datas) {
			fillListItem(location);
		}
	}

	private void fillListItem(final Location location) {
		LayoutInflater inflater = getLayoutInflater();
		final View view = inflater.inflate(R.layout.location_list_item, null);
		TextView fileName = (TextView) view.findViewById(R.id.location_name);
		fileName.setText(location.getName());
		final LinearLayout locationLayout = (LinearLayout) view
				.findViewById(R.id.location_layout);
		locationLayout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
				mActivity.finish();
				Intent intent = new Intent(mActivity, TestLocationListActivity.class);
				intent.putExtra("locationId", location.getId());
				intent.putExtra("locationName", location.getName());
				mActivity.startActivity(intent);
			}
		});
		listView.addView(view);
	}
}
