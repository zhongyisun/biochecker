package com.daokin.biochecker.util;


import java.nio.ByteBuffer;

/**
 *
 */
public class ConvertUtil {

    public static Byte[] intToByte(long number) {
        Byte[] abyte = new Byte[4];
        abyte[0] = (byte) (0xff & number);
        abyte[1] = (byte) ((0xff00 & number) >> 8);
        abyte[2] = (byte) ((0xff0000 & number) >> 16);
        abyte[3] = (byte) ((0xff000000 & number) >> 24);
        return abyte;
    }

    public static Byte[] floatToBytes1(float floatValue) {
        int intValue = Float.floatToIntBits(floatValue);
        Byte[] intData = intToByte(intValue);
        return intData;
    }

    /**
     * Covert byte array to float value
     *
     * @param b
     * @param index
     * @return
     */
    public static float byte2float(Byte[] b, int index) {
        int l;
        l = b[index + 0];
        l &= 0xff;
        l |= ((long) b[index + 1] << 8);
        l &= 0xffff;
        l |= ((long) b[index + 2] << 16);
        l &= 0xffffff;
        l |= ((long) b[index + 3] << 24);
        return Float.intBitsToFloat(l);
    }

    public static int byte2Int(Byte[] b, int index) {
        int value = 0;
        byte bLoop;
        for (int i = index; i < 4; i++) {
            bLoop = b[i];
            value += (bLoop & 0xFF) << (8 * i);
        }
        return value;
    }

    public static int byte2Int(byte[] b, int index) {
        int value = 0;
        byte bLoop;
        for (int i = index; i < 4; i++) {
            bLoop = b[i];
            value += (bLoop & 0xFF) << (8 * i);
        }
        return value;
    }

    public static long byte2Long(Byte[] b, int index) {
        long value = 0;
        byte bLoop;
        for (int i = index; i < 4; i++) {
            bLoop = b[i];
            value += (bLoop & 0xFF) << (8 * i);
        }
        return value & 0xffffffffL;
    }

    public static int byte2short(Byte[] b, int index) {
        short value;
        value = (short) ((b[index] & 0xFF) | ((b[index + 1] & 0xFF) << 8));
        return value;
    }

    public static Byte[] shortToByte(short number) {
        Byte[] abyte = new Byte[2];
        abyte[0] = (byte) (0xff & number);
        abyte[1] = (byte) ((0xff00 & number) >> 8);
        return abyte;
    }

    public static double bytesToDouble(Byte[] b, int index) {
        long l;
        l = b[index];
        l &= 0xff;
        l |= ((long) b[index + 1] << 8);
        l &= 0xffff;
        l |= ((long) b[index + 2] << 16);
        l &= 0xffffff;
        l |= ((long) b[index + 3] << 24);
        l &= 0xffffffffl;
        l |= ((long) b[index + 4] << 32);
        l &= 0xffffffffffl;
        l |= ((long) b[index + 5] << 40);
        l &= 0xffffffffffffl;
        l |= ((long) b[index + 6] << 48);
        l &= 0xffffffffffffffl;
        l |= ((long) b[index + 7] << 56);
        return Double.longBitsToDouble(l);
    }

    public static Byte[] doubleToBytes(double value) {
        Byte[] bytes = new Byte[8];
        long tmp = Double.doubleToLongBits(value);
        for (int i = 0; i < 8; i++) {
            bytes[i] = new Long(tmp).byteValue();
            tmp = tmp >> 8;
        }
        return bytes;
    }

}
