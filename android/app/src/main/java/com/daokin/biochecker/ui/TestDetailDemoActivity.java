package com.daokin.biochecker.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.daokin.biochecker.view.TextProgressBar;

public class TestDetailDemoActivity extends Activity {
    private ActionBar mActionBar;
    private TextProgressBar afbProgressBar;
    private TextProgressBar otaProgressBar;
    private TextProgressBar resultProgressBar;
    private final static String UNDETECTABLE = "Undetectable";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_detail_demo);

        Intent intent = this.getIntent();

        mActionBar = getActionBar();
        mActionBar.setTitle(intent.getStringExtra("testName"));
        mActionBar.setSubtitle(intent.getStringExtra("testDate"));
        afbProgressBar = (TextProgressBar) findViewById(R.id.afb_progressbar);
        otaProgressBar = (TextProgressBar) findViewById(R.id.ota_progressbar);
        resultProgressBar = (TextProgressBar) findViewById(R.id.result_progressbar);
        int afb = intent.getIntExtra("afb", 0);
        int ota = intent.getIntExtra("ota", 0);
        int result = intent.getIntExtra("result", 0);
        if (afb == 0) {
            afbProgressBar.setText(UNDETECTABLE);
            afbProgressBar.setProgress(0);
        } else {
            if (afb > 100) {
                afbProgressBar.setText("> 100 ppb");
            } else {
                afbProgressBar.setText(afb + " ppb");
            }
            afbProgressBar.setProgress(afb);
        }
        if (ota == 0) {
            otaProgressBar.setText(UNDETECTABLE);
            otaProgressBar.setProgress(0);
        } else {
            if (ota > 100) {
                otaProgressBar.setText("> 100 ppb");
            } else {
                otaProgressBar.setText(ota + " ppb");
            }
            otaProgressBar.setProgress(ota);
        }
        if (result == 0) {
            resultProgressBar.setText(UNDETECTABLE);
            resultProgressBar.setProgress(0);
        } else {
            if (result > 100) {
                resultProgressBar.setText("> 100 ppb");
            } else {
                resultProgressBar.setText(result + " ppb");
            }
            resultProgressBar.setProgress(result);
        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu_bar, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                TestDetailDemoActivity.this.finish();
                return true;
            case R.id.options_new_test:
                startActivity(new Intent(TestDetailDemoActivity.this,
                        HomeActivity.class));
                TestDetailDemoActivity.this.finish();
                return true;
            case R.id.options_recent_tests:
                startActivity(new Intent(TestDetailDemoActivity.this,
                        TestListActivity.class));
                TestDetailDemoActivity.this.finish();
                return true;
            case R.id.options_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
