package com.daokin.biochecker.protocol;

public class HighWaterMessage extends GenericMessage {

    public HighWaterMessage(GenericMessage msg) {
        this.seqNum = msg.seqNum;
        this.payload = msg.payload.clone();
    }

    public float getMicroCycle() {
    {
        return this.read_float(1);
    }
    }
    public float getPeriod()
    {
        return this.read_float(5);
    }
    public int getAverage()
    {
        return this.read_unit16(9);
    }
    public int getHighWater()
    {
        return this.read_unit16(11);
    }

}