package com.daokin.biochecker.ui;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import android.R.drawable;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.model.User;
import com.daokin.biochecker.util.RequestUtils;

public class SignInActivity extends Activity {
	private static final String TAG = "BioChecker";
	private Button back_Button = null;
	private Button signin_btn = null;
	private EditText et_firstname = null;
	private EditText et_lastname = null;
	private EditText et_displayname = null;
	private EditText et_age = null;
	private EditText et_email = null;
	private EditText et_password = null;
	private EditText et_password2 = null;
	private RadioGroup genderGroup = null;
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signin_layout);
		Log.d(TAG, "SigninActivity start");
		et_firstname = (EditText) findViewById(R.id.et_firstname);
		et_lastname = (EditText) findViewById(R.id.et_lastname);
		et_displayname = (EditText) findViewById(R.id.et_displayname);
		et_age = (EditText) findViewById(R.id.et_age);
		et_email = (EditText) findViewById(R.id.et_email);
		et_password = (EditText) findViewById(R.id.et_password);
		et_password2 = (EditText) findViewById(R.id.et_password2);
		genderGroup = (RadioGroup) findViewById(R.id.genderGroup);
		progressDialog = new ProgressDialog(this);
		 et_firstname.setText("po");
		 et_lastname.setText("long");
		 et_displayname.setText("Paul");
		 et_age.setText("26");
		 et_email.setText("longpo1988@sina.com");
		 et_password.setText("123456");
		 et_password2.setText("123456");

		back_Button = (Button) findViewById(R.id.cancel_btn);
		back_Button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SignInActivity.this.finish();
			}
		});
		signin_btn = (Button) findViewById(R.id.signin_btn);
		signin_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.d(TAG, "sign in");
				User user = new User();
				if (isNull(user, et_firstname, "FirstName")
						|| isNull(user, et_lastname, "LastName")
						|| isNull(user, et_displayname, "DisplayName")
						|| isNull(user, et_age, "Age")
						|| isNull(user, et_email, "Email")
						|| isNull(user, et_password, "Password")) {
					Log.d(TAG, "return from click");
					return;
				}
				String password = et_password.getText().toString();
				String password2 = et_password2.getText().toString();
				if (!password.equals(password2)) {
					Toast.makeText(SignInActivity.this,
							"two password not equal", Toast.LENGTH_SHORT)
							.show();
					return;
				}
				signin_btn.setClickable(false);
				back_Button.setClickable(false);
				progressDialog.setMessage("Please wait...");
				progressDialog.setIcon(drawable.ic_dialog_info);
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setProgress(59);
				progressDialog.setIndeterminate(true);
				// progressDialog.setButton("Cancel", new
				// DialogInterface.OnClickListener(){
				// public void onClick(DialogInterface dialog, int which) {
				// dialog.cancel();
				// }
				// });
				progressDialog.setCancelable(false);
				progressDialog.setCanceledOnTouchOutside(false);
				progressDialog.show();
				RadioButton rb = (RadioButton) genderGroup
						.findViewById(genderGroup.getCheckedRadioButtonId());
				String gender = rb.getText().toString();
				user.setGender(gender);
				new SignInTask().execute(user);
			}
		});
	}

	private boolean isNull(User user, EditText et, String name) {
		if (et.getText() != null && !"".equals(et.getText().toString().trim())) {
			Log.d(TAG, name + "=" + et.getText().toString());
			if ("Age".equals(name)) {
				user.setAge(Integer.parseInt(et.getText().toString()));
			} else {
				try {
					user.getClass().getMethod("set" + name, String.class)
							.invoke(user, et.getText().toString());
				} catch (Exception e) {
					Log.e(TAG, "", e);
				}
			}
			return false;
		} else {
			Log.d(TAG, name + " is null");
			Toast.makeText(SignInActivity.this, name + " is null",
					Toast.LENGTH_SHORT).show();
			return true;
		}
	}

	class SignInTask extends AsyncTask<User, Void, ResponseModel<User>> {

		@Override
		protected ResponseModel<User> doInBackground(User... params) {
			return RequestUtils.createUser(params[0]);
		}

		@Override
		protected void onPostExecute(ResponseModel<User> result) {
			progressDialog.dismiss();
			if (result == null || result.getCode() == -1) {
				Toast.makeText(SignInActivity.this,
						"Sign In error: " + result.getMessage(),
						Toast.LENGTH_LONG).show();
				signin_btn.setClickable(true);
				back_Button.setClickable(true);
			} else {
				ModelContainer.i.setUser(result.getData());

				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						SignInActivity.this);
				alertDialog.setTitle("Prompt");
				alertDialog.setIcon(drawable.ic_dialog_info);
				alertDialog.setMessage("Register successfully! \nYour new zepto customer ID is: "
                        + ModelContainer.i.getUser().getZeptoCustomerID() + " \nPlease use it to login in the Biochecker and check your email if you forget it.");
				// set ok button
				alertDialog.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								startActivity(new Intent(SignInActivity.this,
										HomeActivity.class));
								SignInActivity.this.finish();
								finish();
							}
						}

				);
				alertDialog.show();

			}
		}
	};
}
