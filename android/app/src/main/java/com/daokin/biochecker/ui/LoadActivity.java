package com.daokin.biochecker.ui;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ProcessedResult.TestSample.SampleType;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.util.DBHelper;
import com.daokin.biochecker.util.RequestUtils;
import com.daokin.biochecker.util.Tools;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class LoadActivity extends Activity {

    //    private boolean animationFinished = false;
//    private boolean isConnected = false;
    private final static String TAG = "LoadActivity";
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load_layout);
        Log.d(TAG, "LoadActivity start");
        ImageView loadImage = (ImageView) findViewById(R.id.loadImage);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);


        AlphaAnimation alphaAnimation = new AlphaAnimation(0.1f, 1.0f);
        alphaAnimation.setDuration(3 * 1000);
        alphaAnimation.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Tools.checkNetwork(LoadActivity.this);
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (!bluetoothAdapter.isEnabled()) {
                    Log.d("BioChecker", "open bluetooth");
                    bluetoothAdapter.enable();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                if (isConnected) {
//                    LoadActivity.this.startActivity(new Intent(LoadActivity.this,
//                            SystemStartupActivity.class));
//                    LoadActivity.this.finish();
//                }
//                animationFinished = true;
            }
        });
        loadImage.setAnimation(alphaAnimation);


        final AsyncTask<Void, Void, ResponseModel<List<SampleType>>> loadSampleTypes =
                new AsyncTask<Void, Void, ResponseModel<List<SampleType>>>() {
                    @Override
                    protected ResponseModel<List<SampleType>> doInBackground(
                            Void... params) {
                        ResponseModel<List<SampleType>> results = RequestUtils.getSampleTypes();
                        return results;
                    }

                    @Override
                    protected void onPostExecute(ResponseModel<List<SampleType>> result) {
                        List<SampleType> sampleTypes = null;
                        DBHelper dbHelper = new DBHelper(getApplicationContext());
                        if (result == null || result.getCode() == -1) {
                            Toast.makeText(LoadActivity.this,
                                    "Can not connect network!", Toast.LENGTH_LONG).show();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    LoadActivity.this.finish();
                                }
                            }, 3000);
                            sampleTypes = dbHelper.querySamples();
                            // zs check null value to prevent from crash
                            if (sampleTypes == null || sampleTypes.isEmpty()) {
                                LoadActivity.this.finish();
                                return;
                            }
                            ModelContainer.i.setSampleTypes(sampleTypes);
                        } else {
                            sampleTypes = result.getData();
                            if (sampleTypes != null) {
                                ModelContainer.i.setSampleTypes(sampleTypes);
                                dbHelper.addSampleTypes(sampleTypes);
                            }
                        }
                        LoadActivity.this.startActivity(new Intent(LoadActivity.this,
                                SystemStartupActivity.class));
                        LoadActivity.this.finish();
                    }
                };
        AsyncTask<Void, Void, Boolean> loadConf = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                Properties properties = getProperties("api.properties");
                Map<String, String> map = new HashMap<>(properties.size());
                for (String str : properties.stringPropertyNames()) {
                    map.put(str, properties.getProperty(str));
                }
                RequestUtils.setUrl(map);
                ModelContainer.i.setBTName("RNBT");
                return true;
            }

            @Override
            protected void onPostExecute(Boolean b) {
                if (b) {
                    loadSampleTypes.execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Cannot load api configuration!",
                            Toast.LENGTH_LONG);
                    System.exit(0);
                }
            }

            private Properties getProperties(String fileName) {
                InputStream in;
                try {
                    in = getAssets().open(fileName);
                    Properties py = new Properties();
                    py.load(in);
                    return py;
                } catch (IOException e) {
                    Log.e(TAG, "Incorrect configures!", e);
                }
                return null;
            }

        };
        loadConf.execute();
    }
}
