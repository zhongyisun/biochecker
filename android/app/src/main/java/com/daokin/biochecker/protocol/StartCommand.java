package com.daokin.biochecker.protocol;

import android.util.Log;

import com.daokin.biochecker.protocol.PacketDefinitions.DigitalGain;
import com.daokin.biochecker.protocol.PacketDefinitions.MessageType;

/**
 *
 */
public class StartCommand extends GenericMessage {

    private void write_bridge_amp(float val) {
        this.write_float(1, val);
    }

    private void write_bridge_hz(float val) {
        this.write_float(5, val);
    }

    private void write_coil_amp(float val) {
        this.write_float(9, val);
    }

    private void write_coil_hz(float val) {
        this.write_float(13, val);
    }

    private void write_coil_dc(float val) {
        this.write_float(17, val);
    }

    private void write_meas_period(float val) {
        this.write_float(21, val);
    }

    private void write_sensor_mode(byte sensorMode) {
        this.payload[25] = sensorMode;
    }

    private void write_muxs(byte muxs) {
        this.payload[26] = muxs;
        this.payload[27] = 0;
        this.payload[28] = 0;
    }

    private void write_gain(byte gain) {
        this.payload[29] = gain;
    }

    private void write_sensor_count(int sensorCount) {
        this.payload[30] = (byte) sensorCount;
    }

    private void write_sensors(Byte[] sensors) {
        System.arraycopy(sensors, 0, payload, 31, sensors.length);
    }

    private void write_sensors_and_controls(Byte[] sensors, Byte[] controls) {
        this.payload[30] = (byte)sensors.length;
        System.arraycopy(sensors, 0, payload, 31, sensors.length);
        if (controls.length > 0) {
            System.arraycopy(controls, 0, payload, 31 + sensors.length, controls.length);
        }
    }

    public float get_bridge_amp() {
        return this.read_float(1);
    }

    public float get_bridge_hz() {
        return this.read_float(5);
    }

    public float get_coil_amp() {
        return this.read_float(9);
    }

    public float get_coil_hz() {
        return this.read_float(13);
    }

    public float get_coil_dc() {
        return this.read_float(17);
    }

    public float get_meas_period() {
        return this.read_float(21);
    }

    public DigitalGain get_gain() {
        return DigitalGain.getDigitalGain(this.payload[29]);
    }

    public byte get_sensor_num() {
        return this.payload[30];
    }

    public Byte[] get_sensors() {
        Byte[] sensors = new Byte[get_sensor_num()];
        System.arraycopy(payload, 31, sensors, 0, get_sensor_num());
        return sensors;
    }

    public StartCommand(Byte[] vTag, Byte[] sensors, float bridge_amp, float coil_amp, float bridge_hz,
                        float coil_dc, float coil_hz, float meas_period, byte gain, byte mux2, Byte[] controls) {
        super(vTag, generate_seqNum(), (byte) PacketDefinitions.HeaderFlag.Syn.getValue(), MessageType.Start.getValue());
        //payload = new Byte[31 + sensors.length];
        payload = new Byte[31 + sensors.length + controls.length];
        setMessageType(MessageType.Start);
        write_bridge_amp(bridge_amp);
        write_coil_amp(coil_amp);
        write_bridge_hz(bridge_hz);
        write_coil_dc(coil_dc);
        write_coil_hz(coil_hz);
        write_meas_period(meas_period);
        if (mux2 != (byte)0xFF) {
            write_sensor_mode((byte)0x01);
            write_muxs(mux2);
        }
        else {
            write_sensor_mode((byte)0x00);
            write_muxs((byte)0x00);
        }

        write_gain(gain);
        write_sensor_count(sensors.length);
        write_sensors(sensors);
        if (controls.length > 0) {
            write_sensors_and_controls(sensors, controls);
        }
        System.out.print("Start command payload: ");
        for (byte b : payload) {
            System.out.print(String.format("%02X ", b));
        }
        System.out.println("");

    }

}