package com.daokin.biochecker.ui;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daokin.biochecker.model.AssayData;
import com.daokin.biochecker.model.ChipDataTypeEnum;
import com.daokin.biochecker.model.GMRData;
import com.daokin.biochecker.model.ResultData;
import com.daokin.biochecker.model.InventoryChipData;
import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ProcessedResult.TestChip.AssayPrinting;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.model.SensorData;
import com.daokin.biochecker.model.SharedApplicationData;
import com.daokin.biochecker.protocol.DataOperation;
import com.daokin.biochecker.protocol.ProtocolHandler;
import com.daokin.biochecker.protocol.StartCommand;
import com.daokin.biochecker.util.DBHelper;
import com.daokin.biochecker.util.DateUtil;
import com.daokin.biochecker.util.RequestUtils;
import com.google.gson.Gson;

public class TestSampleActivity extends Activity {

    private static final String TAG = "BioChecker";
    private Map<String, List> assayPrintingMap = null;
    private TextView tv_home = null;
    private Button continue_btn = null;
    private TextView tv_cancel = null;
    private TextView notice_tv = null;
    private TextView tv_testid = null;
    private AlertDialog alertDialog = null;
    private ImageView indicator = null;
    private ImageView noticeImage = null;
    private ProgressDialog processingDialog = null;
    boolean isConnect = false;

    private String displayedMessage = "";

    private UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private BluetoothSocket clientSocket;

    private BluetoothDevice device = null;

    private ProtocolHandler pHandler = null;

    private Set<String> printingSensors = null;

    private int screenDuration = 60;
    private int intervalGap = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_sample);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        Log.d(TAG, "TestSampleActivity starting...");

        indicator = (ImageView) findViewById(R.id.indicator);
        noticeImage = (ImageView) findViewById(R.id.notice_image);

        tv_home = (TextView) findViewById(R.id.tv_home);
        notice_tv = (TextView) findViewById(R.id.notice_tv);
        tv_testid = (TextView) findViewById(R.id.tv_testid);

        Long testId =  ModelContainer.i.getTest().getOid();
        if (testId == null) {//offline only
            testId = ModelContainer.i.getTest().getId();
        }
        System.out.println("oid:" + ModelContainer.i.getTest().getOid() + ", id:" + ModelContainer.i.getTest().getId() );
        tv_testid.setText("TEST ID: " + testId);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        tv_home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TestSampleActivity.this.finish();
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TestSampleActivity.this.finish();
            }
        });
        tv_home.setVisibility(View.INVISIBLE);
        tv_cancel.setVisibility(View.INVISIBLE);

        LinearLayout linearLayout = new LinearLayout(this);
        TextView tv_title = new TextView(this);
        TextView tv_date = new TextView(this);
        tv_title.setText("Test 001");
        tv_title.setTextSize(18);
        tv_date.setText("2014-11-11 11:11:11");
        tv_date.setTextSize(12);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(20, 10, 0, 10);
        linearLayout.addView(tv_title);
        linearLayout.addView(tv_date);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Your test result are ready");
        builder.setView(linearLayout);
        builder.setPositiveButton("VIEW TESTS", new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(TestSampleActivity.this,
                        TestListActivity.class));
                TestSampleActivity.this.finish();
            }
        });

        alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);

        processingDialog = new ProgressDialog(this);
        //processingDialog.setTitle("Start Test");
        processingDialog.setTitle("Processing Sample");
        processingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        processingDialog.setCancelable(false);
        processingDialog.setCanceledOnTouchOutside(false);
        //processingDialog.setMessage("Please wait, this will take several minutes...");
        processingDialog.setMessage("Please wait...");

        List<AssayPrinting> assayPrintingsList = ModelContainer.i.getAssayPrintings();
        List<String> assayNameList = new ArrayList<>();
        List<String> printingList = new ArrayList<>();
        for (Iterator it = assayPrintingsList.iterator(); it.hasNext(); ) {
            AssayPrinting ap = (AssayPrinting) it.next();
            String str = ap.getAssay().getName();
            assayNameList.add(str);
            printingList.add(ap.getPrinting());
        }
        assayPrintingMap = new HashMap<>();
        for (int i = 0; i < assayNameList.size(); i++) {
            String printingMark = assayNameList.get(i);
            String printingSensors = printingList.get(i);
            List<String> printingSensorList = null;
            if (printingSensors == null) {
                printingList = new ArrayList<>(0);
            } else {
                printingSensorList = Arrays.asList(printingSensors.split(","));
            }
            assayPrintingMap.put(printingMark, printingSensorList);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                device = ModelContainer.i.getDevice();
                openBluetooth();
                if (!isConnect) {
                    Toast.makeText(TestSampleActivity.this, "Not connect", Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }

                noticeImage.setImageDrawable(getResources().getDrawable(R.drawable.step4image));
                notice_tv.setVisibility(View.INVISIBLE);
                noticeImage.postInvalidate();
                notice_tv.postInvalidate();
                continue_btn = (Button) findViewById(R.id.continue_btn);
                continue_btn.setClickable(false);
                continue_btn.setText("Testing sample");
                continue_btn.setEnabled(false);
                continue_btn.postInvalidate();

                processingDialog.setTitle("Processing sample");
                displayedMessage = "Please wait, this will take " + ModelContainer.i.getMaxDuration() + " seconds...";
                processingDialog.setMessage(displayedMessage + "\r\n\r\n ");
                new SampleHandlerTask().execute();
            }
        }, 2000);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                pHandler.sendStopCommand();
                Log.d(TAG, "uiSampleHandler:pHandler stopped.");

                // zs
                DataOperation dataOperation = pHandler.getDataOperation();
                dataOperation.PostProcessing();

                processingDialog.hide();
                saveResult();
            }
        }, ModelContainer.i.getMaxDuration() * 1000 + 10000);
    }

    @Override
    protected void onDestroy() {
        if (pHandler != null) {
            pHandler.stopHandler();
        }
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
        super.onDestroy();
    }

    private Handler uiSampleHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String strMsg = (String) msg.obj;

            if (!strMsg.startsWith("Checksum error") && !strMsg.startsWith("Elapsed time")) {
                Toast.makeText(TestSampleActivity.this, strMsg, Toast.LENGTH_LONG).show();
            }

            if (strMsg.equals("Start succeeded")) {
                indicator.setImageResource(R.drawable.step4indicator);
                processingDialog.show();
                DataOperation dataOperation = pHandler.getDataOperation();
                String electricReferenceSensor = dataOperation.getElectricalReferenceSensor();
                processingDialog.setMessage(displayedMessage + "\r\n\r\nSelected reference: " + electricReferenceSensor);

            } else if (strMsg.startsWith("Elapsed time")) {
                processingDialog.setMessage(displayedMessage + "\r\n\r\n" + strMsg);
            } else if ("NoChip".equals(strMsg)) {
                processingDialog.dismiss();
                notice_tv.setText("No Chip found, "
                        + "Please check your chip device and try again.");
            } else if (strMsg.startsWith("Checksum error")) {
                //notice_tv.setText("Checksum error");
                processingDialog.setMessage("Please wait, this " +
                        "will take several minutes...\r\n\r\n" + strMsg);
            } else if (strMsg.startsWith("Error")) {
                processingDialog.dismiss();
                Toast.makeText(TestSampleActivity.this, "Some error occurs.", Toast.LENGTH_LONG);
                Intent intent = new Intent(TestSampleActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            } else if (strMsg.equals("Unlinked")) {
                Toast.makeText(TestSampleActivity.this, "Some error occurs in the linking process.",
                        Toast.LENGTH_LONG);
                Intent intent = new Intent(TestSampleActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }
    };

    private class SampleHandlerTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (isConnect) {
                    try {
                        pHandler = new ProtocolHandler(clientSocket, uiSampleHandler, false);
                        printingSensors = new HashSet<>();
                        for (AssayPrinting ap : ModelContainer.i
                                .getAssayPrintings()) {
                            String printing = ap.getPrinting();
                            Log.d(TAG, "##########printing:" + printing);
                            if (printing.length() > 1) {
                                printingSensors.addAll(Arrays.asList(printing.split(",")));
                            }
                        }
                        DataOperation dataOperation = pHandler.getDataOperation();
                        Log.d(TAG, "Test sample start...");
                        dataOperation.setPrintingSensors(printingSensors);
                        pHandler.start(getApplicationContext());
                        Log.d(TAG, "Start protocol handler...");
                    } catch (IOException e) {
                        Log.e(TAG, "", e);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "", e);
            }
            return null;
        }
    }

    private void saveResult() {
        new AsyncTask<Void, Void, Integer>() {

            private ResultData resultData;
            private String msg;

            @Override
            protected Integer doInBackground(Void... params) {
                try {
                    Map<Integer, String> analyteMap = new TreeMap<Integer, String>();
                    AssayData assayData = ModelContainer.i.getAssayData();
                    List<AssayData.Assay> assayList = assayData.getAssays();
                    for (AssayData.Assay s : assayList) {
                        analyteMap.put(s.getAnalyte(), s.getName());
                    }
                    resultData = buildResult();
                    // populate assay
                    List<ResultData.AnalyteResult> assay = new ArrayList<>();
                    DataOperation dataOperation = pHandler.getDataOperation();
                    Map<Integer, Float> analyteResult = dataOperation.analyteResult;
                    for (Map.Entry<Integer, Float> entry : analyteResult.entrySet()) {

                        ResultData resultData = new ResultData();
                        ResultData.AnalyteResult result = resultData.new AnalyteResult();
                        result.setAnalyte(entry.getKey());
                        result.setAnalyteName(analyteMap.get(entry.getKey()));
                        List<Float> values = new ArrayList<Float>();
                        values.add(entry.getValue());
                        result.setResult(values);
                        assay.add(result);
                    }
                    resultData.setAssay(assay);

                    String resultJson = new Gson().toJson(resultData);
//                    String gmrResJson = buildGMRResultJson();
//                    Log.d(TAG, "GMR JSON: " + gmrResJson);
                    Log.d(TAG, "RESULT JSON: " + resultJson);

                    GMRData data = dataOperation.getGMRData();
                    String gmrResJson = new Gson().toJson(data);

                    if (SharedApplicationData.isOffline()) {
                        InventoryChipData chipData = new InventoryChipData();
                        chipData.setLastUpdated(DateUtil.formatDate());
                        chipData.setData(resultJson);
                        chipData.setDataTypeId(ChipDataTypeEnum.OVARIANCANCER.getValue());
                        chipData.setChipId(ModelContainer.i.getChipId());
                        DBHelper dbHelper = new DBHelper(getApplicationContext());
                        dbHelper.addChipData(chipData);
                        chipData.setData(gmrResJson);
                        chipData.setDataTypeId(ChipDataTypeEnum.GMR.getValue());
                        dbHelper.addChipData(chipData);
                        return 1;
                    } else {
                        RequestUtils.saveResultData(ModelContainer.i.getChipId(), resultJson);
                        //ResponseModel<String> model = RequestUtils.saveGMRData(
                        //        ModelContainer.i.getChipId(), gmrResJson);
//                        if (model.getCode() != -1) {
//                            //return 1;
//                        } else {
//                            //msg = model.getMessage();
//                            //return -1;
//                        }
                        // zs playing
                        Map<String, List<List<Float>>> map = new TreeMap<String, List<List<Float>>>();
                        map.put("*", new ArrayList<List<Float>>());
                        String json = new Gson().toJson(map);
                        ResponseModel<String> model = RequestUtils.updateGMRData(ModelContainer.i.getChipId(), json);
                        if (model.getCode() != -1) {
                            System.out.println("##########post data for * succeeded");
                        } else {
                            System.out.println("##########post data for * failed");
                        }
                        Map<String, SensorData> sensorDataMap = data.getData();
                        for (Map.Entry<String, SensorData> entry : sensorDataMap.entrySet()) {
                            map = new TreeMap<String, List<List<Float>>>();
                            SensorData sensorData = (SensorData)entry.getValue();
                            map.put(entry.getKey(), sensorData.getData());
                            json = new Gson().toJson(map);
                            model = RequestUtils.updateGMRData(ModelContainer.i.getChipId(), json);
                            if (model.getCode() != -1) {
                                System.out.println("##########post data for " + entry.getKey() + " succeeded");
                            } else {
                                System.out.println("##########post data for " + entry.getKey() + " failed");
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    Log.d(TAG, "Exception: " + ex.getMessage());
                    return -1;
                }
                // zs
                return 1;
            }

            @Override
            protected void onPostExecute(Integer result) {
                if (result == -1) {
                    Toast.makeText(TestSampleActivity.this, msg, Toast.LENGTH_LONG);
                }
                processingDialog.dismiss();
                alertDialog.show();
                Intent intent = new Intent(TestSampleActivity.this, TestDetailActivity.class);
                intent.putExtra("testName", ModelContainer.i.getTest().getName());
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = ModelContainer.i.getTest().getCreatedTime();
                intent.putExtra("testDate", formattedDate);
                intent.putExtra("result", buildResultMap(resultData, ModelContainer.i.getAssayData()));
                pHandler.stopHandler();
                startActivity(intent);
                TestSampleActivity.this.finish();
            }
        }.execute();
    }

    private ResultData buildResult() {
        DataOperation dataOperation = pHandler.getDataOperation();
        Map<Byte, List<Float>> pSensorValues = dataOperation.getPSensorValues();
        ResultData resultData = new ResultData();
        Map<String, ResultData.SensorResult> sensorMap = new HashMap<>();
        Map<Byte, String> mux1LabelMap = ModelContainer.i.getMux1LabelMap();
        for (Byte b : pSensorValues.keySet()) {
            ResultData.SensorResult sensorResult = resultData.new SensorResult();
            sensorResult.setResult(pSensorValues.get(b));
            sensorMap.put(mux1LabelMap.get(b), sensorResult);
        }
        resultData.setSensor(sensorMap);
        Map<Integer, List<Float>> averageMap = dataOperation.getAverages();
        List<ResultData.AnalyteResult> ars = new ArrayList<>();
        for (Map.Entry<Integer, List<Float>> entry : averageMap.entrySet()) {
            ResultData.AnalyteResult analyteResult = resultData.new AnalyteResult();
            analyteResult.setAnalyte(entry.getKey());
            analyteResult.setResult(entry.getValue());
            ars.add(analyteResult);
        }
        resultData.setAssay(ars);
        return resultData;
    }

    private HashMap<String, String> buildResultMap(ResultData rData, AssayData aData) {
        List<AssayData.Assay> assays = aData.getAssays();
        HashMap<String, String> map = new HashMap<String, String>();
        Map<Integer, Float> anaRltMap = new HashMap<>(assays.size());
        for (ResultData.AnalyteResult result : rData.getAssay()) {
            float sum = 0f;
            for (Float f : result.getResult()) {
                sum += f;
            }
            anaRltMap.put(result.getAnalyte(), sum / result.getResult().size());
        }

        DataOperation dataOperation = pHandler.getDataOperation();
        for (AssayData.Assay assay : assays) {
            // Reference sensor print should always have analyte as 0, skip display
            int analyte = assay.getAnalyte();
            if (analyte > 0 && dataOperation.analyteResult.containsKey(analyte)) {
                float value = dataOperation.analyteResult.get(analyte);
                DecimalFormat myFormatter = new DecimalFormat("###.###");
                String output = myFormatter.format(value);
                map.put(assay.getName(), output + "\t" + assay.getUnit());
            }
        }

        return map;
    }

    public void openBluetooth() {
        try {
            if (device == null) {
                Toast.makeText(TestSampleActivity.this, "Can not find any device.",
                        Toast.LENGTH_LONG).show();
                return;
            }

            if (clientSocket == null) {
                clientSocket = device
                        .createRfcommSocketToServiceRecord(MY_UUID);
                try {
                    clientSocket.connect();
                    isConnect = true;
                } catch (Exception e) {
                    Log.d(TAG, "trying fallback");
                    try {
                        clientSocket = (BluetoothSocket) device
                                .getClass()
                                .getMethod("createRfcommSocket", new Class[]{int.class})
                                .invoke(device, 1);
                        clientSocket.connect();
                        isConnect = true;
                    } catch (Exception ex) {
                        Log.e(TAG, "clientSocket exception", ex);
                    }
                }
            }
            else {
                if (!clientSocket.isConnected()) {
                    clientSocket.connect();
                    isConnect = true;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

}

