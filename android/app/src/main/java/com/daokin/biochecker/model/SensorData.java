package com.daokin.biochecker.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by SamXCode on 2016/2/16.
 */
public class SensorData {
    private List<List<Float>> result = new ArrayList<List<Float>>();
    //private Map<Integer, List<Float>> dataSereies = new TreeMap<Integer, List<Float>>();

    public SensorData() {
    }

    public List<List<Float>> getData() {
        return result;
    }

    public void setData(List<List<Float>> data) {
        this.result = data;
    }

    public void addData(List<Float> data) { this.result.add(data); }

    //public Map<Integer, List<Float>> getGenerateDataSeries() {
    //    return dataSereies;
    //}

//    public void generateDataSeries() {
//        dataSereies.clear();
//        for (List<Float> data : result) {
//            int cycle = (int) Math.round(data.get(1));
//            dataSereies.put(cycle, data);
//        }
//    }

    public Map<Integer, List<Float>> generateDataSeries() {
        Map<Integer, List<Float>> dataSeries = new TreeMap<Integer, List<Float>>();
        for (List<Float> data : result) {
            int cycle = (int) Math.round(data.get(1));
            dataSeries.put(cycle, data);
        }
        return dataSeries;
    }
}
