package com.daokin.biochecker.protocol;

import com.daokin.biochecker.protocol.PacketDefinitions.MessageSize;
import com.daokin.biochecker.protocol.PacketDefinitions.StatusCode;

/**
 *
 */
public class StatusMessage extends GenericMessage {

  public StatusCode getStatusCode() {
    return StatusCode.getStatusCode(payload[1]);
  }

  public StatusMessage(GenericMessage msg) {
//    this.payload = new Byte[MessageSize.StatusBytes.getValue()];
//    this.setMessageType(PacketDefinitions.MessageType.Status);
    this.seqNum = msg.seqNum;
    this.payload = msg.payload.clone();
  }
}
