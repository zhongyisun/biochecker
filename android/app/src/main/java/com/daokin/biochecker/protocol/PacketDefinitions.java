package com.daokin.biochecker.protocol;

/**
 *
 */
public class PacketDefinitions {
  public enum HeaderFlag {
    Ack((byte) 0x02),
    Syn((byte) 0x10),
    SAck((byte) 0x40);

    private final byte val;

    HeaderFlag(byte val) {
      this.val = val;
    }

    public byte getValue() {
      return this.val;
    }

    public static HeaderFlag getHeaderFlag(byte inputValue) {
      for (HeaderFlag mt : HeaderFlag.values()) {
        if (inputValue == mt.val) {
          return mt;
        }
      }
      return null;
    }
  }
  public enum MessageType {
    Start((byte)0x00),
    Stop((byte)0x01),
    Debug((byte)0x02),
    HighWater((byte)0x03),
    DeviceRegistration((byte)0x06),
    Ping((byte)0x07),
    StartReply((byte)0x08),
    Report((byte)0x09),
    Status((byte)0x0A),
    RetistrationReply((byte)0x0B),
    Unknown((byte)0xff);

    private byte value;

    MessageType(byte val) {
      this.value = val;
    }

    public byte getValue() {
      return this.value;
    }
    
    public static MessageType getMessageType(byte inputValue) {
      for (MessageType mt : MessageType.values()) {
        if (inputValue == mt.value) {
          return mt;
        }
      }
      return null;
    }
  }
  
  public enum SpecialByte {
    SOF((byte)0x12),
    EOF((byte)0x13),
    ESC((byte)0x7D),
    ACK_FLAG((byte)0x80),
    ESC_XOR((byte)0x20);
    
    private byte value;

    SpecialByte(byte val) {
      this.value = val;
    }

    public byte getValue() {
      return this.value;
    }
    
  }
  /**
   * Status code
   */
  public enum StatusCode {
    NO_CHIP((byte)0x01);
    
    private byte value;

    StatusCode(byte val) {
      this.value = val;
    }

    public byte getValue() {
      return this.value;
    }
    
    public static StatusCode getStatusCode(byte inputValue) {
      for (StatusCode code : values()) {
        if (code.getValue() == inputValue) {
          return code;
        }
      }
      return null;
    }
    
  }

  public enum MessageSize {
    MinStartBytes(31),
    MaxStartBytes(MinStartBytes.value + 64),
    StopBytes(1),
    PingBytes(1),
    StartConfirm(30),
    ReportBytes(112),
    StatusBytes(2); //need to confirm if it's 1 bytes

    private int value;

    private MessageSize(int value) {
      this.value = value;
    }

    public int getValue() {
      return this.value;
    }
  }
  
  
  public enum DigitalGain {
    ONE(0x01),
    TWO(0x02),
    FOUR(0x04),
    EIGHT(0x08),
    SIXTEEN(0x10),
    THIRTY_TWO(0x20),
    SIXTY_FOUR(0x40),
    ONE_HUNDRED_TWENTY_EIGHT(0x80);

    private int value;

    private DigitalGain(int value) {
      this.value = value;
    }

    public int getValue() {
      return this.value;
    }
    
    public static DigitalGain getDigitalGain(byte inputValue) {
      for (DigitalGain gain : values()) {
        if (gain.getValue() == inputValue) {
          return gain;
        }
      }
      return null;
    }
  }
  
    public enum packet_sm_ret {
      idle,
      working,
      packet_too_large,
      malformed_packet,
      bad_checksum,
      payload_ready,
  }
  
}
