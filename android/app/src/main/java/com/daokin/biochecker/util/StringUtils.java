package com.daokin.biochecker.util;

/**
 * Created by SamXCode on 2015/12/4.
 */
public class StringUtils {

    /**
     * check the string is null or its length is zero
     * @param s
     * @return
     */
    public static boolean isEmpty(String s) {
        return s == null || s.trim().length() == 0;
    }
}
