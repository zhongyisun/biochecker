package com.daokin.biochecker.util;

import android.util.Log;

import com.daokin.biochecker.model.AssayData;
import com.daokin.biochecker.model.Location;
import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ProcessedResult;
import com.daokin.biochecker.model.ProcessedResult.TestSample.SampleType;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.model.ResultData;
import com.daokin.biochecker.model.ResultDetail;
import com.daokin.biochecker.model.Test;
import com.daokin.biochecker.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RequestUtils {
    private static final String TAG = "BioChecker";
    private static String BASE_URL = "http://50.249.115.202:9000/";
    //private static String BASE_URL = "http://192.168.0.95:9000/";
    private static String LOGIN_API = BASE_URL + "user/login";
    private static String GET_PROCESSED_RESULTS_BY_LOCATION =
            BASE_URL + "test/location/processed_results/";
    private static String QUERY_PROCESSED_RESULTS_BY_TERM = BASE_URL + "test/query/";
    private static String GET_LOCATIONS = BASE_URL + "test/locations";
    private static String GET_SAMPLE_TYPES = BASE_URL + "test/sample_types";
    private static String CREATE_TEST = BASE_URL + "ws/test/createTest";
    private static String SIGN_IN = BASE_URL + "user";
    private static String api_key = "api_key=zepto_reserved_api_key";

    private static String GET_ASSAYS_2 = BASE_URL + "ws/test/chip_assay_type/printing/";
    private static String UPDATE_GMR_DATA = BASE_URL + "ws/test/updateGMRData/";
    private static String SAVE_GMR_DATA = BASE_URL + "ws/test/saveGMRData/";
    private static String SAVE_RESULT_DATA = BASE_URL + "ws/test/saveOvarianData/";
    private static String GET_TESTS = BASE_URL + "ws/test/list/";
    private static String GET_TEST = BASE_URL + "ws/test/chip/ovarian/data/";

    public static void setUrl(Map<String, String> map) {
        BASE_URL = map.get("BASE_URL");
        api_key = map.get("api_key");
        LOGIN_API = BASE_URL + map.get("LOGIN_API");
        GET_PROCESSED_RESULTS_BY_LOCATION = BASE_URL + map.get("GET_PROCESSED_RESULTS_BY_LOCATION");
        QUERY_PROCESSED_RESULTS_BY_TERM = BASE_URL + map.get("QUERY_PROCESSED_RESULTS_BY_TERM");
        GET_LOCATIONS = BASE_URL + map.get("GET_LOCATIONS");
        GET_SAMPLE_TYPES = BASE_URL + map.get("GET_SAMPLE_TYPES");
        CREATE_TEST = BASE_URL + map.get("CREATE_TEST");
        SIGN_IN = BASE_URL + map.get("SIGN_IN");
        GET_ASSAYS_2 = BASE_URL + map.get("GET_ASSAYS_2");
        SAVE_GMR_DATA = BASE_URL + map.get("SAVE_GMR_DATA");
        SAVE_RESULT_DATA = BASE_URL + map.get("SAVE_RESULT_DATA");
        GET_TESTS = BASE_URL + map.get("GET_TESTS");
        GET_TEST = BASE_URL + map.get("GET_TEST");
    }

    public static ResponseModel<List<Test>> getTests(String customerId, int pageSize, int pageIndex) {
        ResponseModel<List<Test>> model = new ResponseModel<>();
        try {
            Map<String, String> params = new HashMap<>();
            params.put("pageSize", String.valueOf(pageSize < 0 ? 20 : pageSize));
            params.put("page", String.valueOf((pageIndex) < 0 ? 0 : pageIndex));
            Map<String, Object> result = signRequestByGet(GET_TESTS + customerId, params);
            model.setCode((int) result.get("code"));
            if (model.getCode() == -1) {
                model.setMessage((String) result.get("message"));
            } else {
                if (((HashMap) result.get("data")).size() == 0 ||
                        !((HashMap) result.get("data")).containsKey("testResults")) {
                    model.setData(new ArrayList<Test>());
                } else {
                    List<Test> list = new Gson().fromJson(new Gson().toJson(((HashMap) result
                                    .get("data")).get("testResults")),
                            new TypeToken<List<Test>>() {
                            }.getType());
                    model.setData(list);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "request user api error.", e);
            model.setCode(-1);
            model.setMessage(e.getMessage());
        }
        return model;
    }

    public static ResponseModel<ResultDetail> getTest(long testId) {
        ResponseModel<ResultDetail> responseModel = new ResponseModel<>();
        try {
            Map<String, Object> result = signRequestByGet(GET_TEST + testId, null);
            responseModel.setCode((int) result.get("code"));
            if (responseModel.getCode() == -1) {
                responseModel.setMessage((String) result.get("message"));
            } else {
                ResultDetail detail = new Gson().fromJson(new Gson().toJson(result.get("data")),
                        new TypeToken<ResultDetail>() {
                        }.getType());
                responseModel.setData(detail);
            }
        } catch (Exception e) {
            Log.e(TAG, "request user api error.", e);
            responseModel.setCode(-1);
            responseModel.setMessage(e.getMessage());
        }
        return responseModel;
    }

    public static ResponseModel<String> saveResultData(Long chipId, String json) {
        ResponseModel<String> model = new ResponseModel<>();
        try {
            Map<String, Object> result = signRequestByPost(SAVE_RESULT_DATA + chipId, json);
            model.setCode((int) result.get("code"));
            if (model.getCode() == -1) {
                model.setMessage((String) result.get("message"));
            } else {
                model.setData("success");
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage(), e);
            model.setCode(-1);
            model.setMessage(e.getMessage());
        }
        return model;
    }

    public static ResponseModel<String> updateGMRData(Long chipId, String json) {
        ResponseModel<String> model = new ResponseModel<>();
        try {
            Map<String, Object> result = signRequestByPost(UPDATE_GMR_DATA + chipId, json);
            model.setCode((int) result.get("code"));
            if (model.getCode() == -1) {
                model.setMessage((String) result.get("message"));
            } else {
                model.setData("success");
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage(), e);
            model.setCode(-1);
            model.setMessage(e.getMessage());
        }
        return model;
    }

    public static ResponseModel<String> saveGMRData(Long chipId, String json) {
        ResponseModel<String> model = new ResponseModel<>();
        try {
            Map<String, Object> result = signRequestByPost(SAVE_GMR_DATA + chipId, json);
            model.setCode((int) result.get("code"));
            if (model.getCode() == -1) {
                model.setMessage((String) result.get("message"));
            } else {
                model.setData("success");
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage(), e);
            model.setCode(-1);
            model.setMessage(e.getMessage());
        }
        return model;
    }

    public static ResponseModel<AssayData> getAssays(Long typeId) {
        ResponseModel<AssayData> responseModel = new ResponseModel<>();
        try {
            Map<String, Object> result = signRequestByGet(GET_ASSAYS_2 + typeId, null);
            responseModel.setCode((int) result.get("code"));
            if (responseModel.getCode() == -1) {
                responseModel.setMessage((String) result.get("message"));
            } else {
                String jsonString = new Gson().toJson(result.get("data"));
                //System.out.println("##########getAssays: " + jsonString);
                AssayData assayData = new Gson().fromJson(jsonString,
                        new TypeToken<AssayData>() {
                        }.getType());
                responseModel.setData(assayData);
            }
        } catch (Exception e) {
            Log.e(TAG, "request user api error.", e);
            responseModel.setCode(-1);
            responseModel.setMessage(e.getMessage());
        }
        return responseModel;
    }

    public static ResponseModel<User> Login(String userName, String password) {
        Map<String, Object> params = new HashMap<String, Object>();
        ResponseModel<User> responseModel = new ResponseModel<User>();
        params.put("zeptoCustomerID", userName);
        params.put("password", password);
        try {
            Map<String, Object> result;
            result = signRequestByPost(LOGIN_API, params);
            responseModel.setCode((int) result.get("code"));
            if (responseModel.getCode() == -1) {
                responseModel.setMessage((String) result.get("message"));
            } else {
                User user = new Gson().fromJson(new Gson().toJson(result),
                        new TypeToken<User>() {
                        }.getType());
                responseModel.setData(user);
            }
        } catch (Exception e) {
            Log.e(TAG, "request user api error.", e);
            responseModel.setCode(-1);
            responseModel.setMessage(e.getMessage());
        }
        return responseModel;
    }

    public static ResponseModel<List<ProcessedResult>> getProcessedResultsByLocation(
            String zeptoCustomerID, int pageSize, int pageIndex, int locationId) {
        System.out.println("getProcessedResults   pageIndex:" + pageIndex);
        ResponseModel<List<ProcessedResult>> responseModel = new ResponseModel<List<ProcessedResult>>();
        try {
            Map<String, Object> result;
            Map<String, String> params = new HashMap<String, String>();
            params.put("pageSize", String.valueOf(pageSize < 0 ? 20 : pageSize));
            params.put("page", String.valueOf((pageIndex) < 0 ? 0 : pageIndex));
            result = signRequestByGet(GET_PROCESSED_RESULTS_BY_LOCATION + locationId, params);
            responseModel.setCode((int) result.get("code"));
            if (responseModel.getCode() == -1) {
                responseModel.setMessage((String) result.get("message"));
            } else if (result.get("data") != null) {
                if (result.get("data") instanceof HashMap) {
                    if (((HashMap) result.get("data")).size() == 0 ||
                            !((HashMap) result.get("data")).containsKey("testProcessedResults")) {
                        responseModel.setData(new ArrayList<ProcessedResult>());
                    } else {
                        List<ProcessedResult> list = new Gson()
                                .fromJson(new Gson().toJson(((HashMap) result
                                                .get("data"))
                                                .get("testProcessedResults")),
                                        new TypeToken<List<ProcessedResult>>() {
                                        }.getType());
                        responseModel.setData(list);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseModel.setCode(-1);
            responseModel.setMessage(e.getMessage());
        }
        return responseModel;
    }


    public static ResponseModel<List<Location>> getLocations() {
        ResponseModel<List<Location>> responseModel = new ResponseModel<List<Location>>();
        try {
            Map<String, Object> result;
            Map<String, String> params = new HashMap<String, String>();
            result = signRequestByGet(GET_LOCATIONS, params);
            responseModel.setCode((int) result.get("code"));
            if (responseModel.getCode() == -1) {
                responseModel.setMessage((String) result.get("message"));
            } else if (result.get("data") != null) {
                List<Location> list = new Gson().fromJson(
                        new Gson().toJson(result.get("data")),
                        new TypeToken<List<Location>>() {
                        }.getType());
                responseModel.setData(list);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseModel.setCode(-1);
            responseModel.setMessage(e.getMessage());
        }
        return responseModel;
    }

    public static ResponseModel<List<ProcessedResult>> getProcessedResultsByTerm(
            String term, String zeptoCustomerID, int pageSize, int pageIndex) {
        System.out.println("getProcessedResultsByTerm:" + term + ",pageIndex:" + pageIndex);
        ResponseModel<List<ProcessedResult>> responseModel = new ResponseModel<List<ProcessedResult>>();
        try {
            Map<String, Object> result;
            Map<String, String> params = new HashMap<String, String>();
            params.put("pageSize", String.valueOf(pageSize < 0 ? 20 : pageSize));
            params.put("page", String.valueOf((pageIndex) < 0 ? 0 : pageIndex));
            params.put("term", term);
            result = signRequestByGet(QUERY_PROCESSED_RESULTS_BY_TERM + zeptoCustomerID, params);
            responseModel.setCode((int) result.get("code"));
            if (responseModel.getCode() == -1) {
                responseModel.setMessage((String) result.get("message"));
            } else if (result.get("data") != null) {
                if (result.get("data") instanceof HashMap) {
                    if (((HashMap) result.get("data")).size() == 0 ||
                            !((HashMap) result.get("data")).containsKey("testProcessedResults")) {
                        responseModel.setData(new ArrayList<ProcessedResult>());
                    } else {
                        List<ProcessedResult> list = new Gson()
                                .fromJson(new Gson().toJson(((HashMap) result
                                                .get("data"))
                                                .get("testProcessedResults")),
                                        new TypeToken<List<ProcessedResult>>() {
                                        }.getType());
                        responseModel.setData(list);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            responseModel.setCode(-1);
            responseModel.setMessage(e.getMessage());
        }
        return responseModel;
    }

    public static ResponseModel<List<SampleType>> getSampleTypes() {
        ResponseModel<List<SampleType>> responseModel = new ResponseModel<List<SampleType>>();
        try {
            Map<String, Object> result;
            result = signRequestByGet(GET_SAMPLE_TYPES, null);
            if (result != null) {
                responseModel.setCode((int) result.get("code"));
                if (responseModel.getCode() == -1) {
                    responseModel.setMessage((String) result.get("message"));
                } else {
                    List<SampleType> list = new Gson().fromJson(
                            new Gson().toJson(result.get("data")),
                            new TypeToken<List<SampleType>>() {
                            }.getType());
                    responseModel.setData(list);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
            responseModel.setCode(-1);
            responseModel.setMessage(e.getMessage());
        }
        return responseModel;

    }

    public static ResponseModel<Long> createTest(String name, String desc, String zeptoCustomerId,
                                                 String sampleName, Long chipId) {
        ResponseModel<Long> responseModel = new ResponseModel<>();
        try {

            Map<String, Object> requestMap = new HashMap<String, Object>();
            requestMap.put("name", name);
            requestMap.put("description", desc);
            requestMap.put("inventoryChipId", chipId);
            requestMap.put("sampleName", sampleName);
            requestMap.put("zeptoCustomerID", zeptoCustomerId);
            requestMap.put("deviceId", ModelContainer.i.getDevice().getName());
            Map<String, Object> result;
            result = signRequestByPost(CREATE_TEST, requestMap);
            responseModel.setCode((int) result.get("code"));
            if (responseModel.getCode() == -1) {
                responseModel.setMessage((String) result.get("message"));
            } else {
                responseModel.setData(Long.valueOf(result.get("testId").toString()));
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage(), e);
            responseModel.setCode(-1);
            responseModel.setMessage(e.getMessage());
        }
        return responseModel;
    }

    public static ResponseModel<User> createUser(User user) {
        ResponseModel<User> responseModel = new ResponseModel<User>();
        try {

            Map<String, Object> requestMap = new HashMap<String, Object>();
            requestMap.put("password", user.getPassword());
            requestMap.put("email", user.getEmail());
            requestMap.put("firstName", user.getFirstName());
            requestMap.put("lastName", user.getLastName());
            requestMap.put("age", 0);
            requestMap.put("gender", user.getGender());
            requestMap.put("userRoleId", 1);
            requestMap.put("userClientId", 0);
            requestMap.put("name", user.getName());

            Map<String, Object> result;
            result = signRequestByPost(SIGN_IN, requestMap);
            responseModel.setCode((int) result.get("code"));
            if (responseModel.getCode() == -1) {
                responseModel.setMessage((String) result.get("message"));
            } else {
                User rsUser = new Gson().fromJson(
                        new Gson().toJson(result),
                        new TypeToken<User>() {
                        }.getType());
                responseModel.setData(rsUser);
            }
        } catch (Exception e) {
            responseModel.setCode(-1);
            responseModel.setMessage(e.getMessage());
        }
        return responseModel;
    }

    private static Map<String, Object> signRequestByGet(String url, Map<String, String> params)
            throws JSONException, ParseException, IOException {
        Map<String, Object> rsMap = null;
        String reqUrl = url + "?" + api_key;
        if (params != null) {
            List<BasicNameValuePair> paramList = new ArrayList<BasicNameValuePair>(
                    params.size());
            for (String key : params.keySet()) {
                paramList.add(new BasicNameValuePair(key, params.get(key)));
            }
            reqUrl = reqUrl + "&" + URLEncodedUtils.format(paramList, "UTF-8");
        }
        Log.d(TAG, "Get:" + reqUrl);
        HttpGet getMethod = new HttpGet(reqUrl);
        // getMethod.setHeader("Accept", "application/json");
        // getMethod.setHeader("Content-Type", "application/json");
        try {
            HttpClient client = HttpClientHelper.getHttpClient();
            HttpResponse response = client.execute(getMethod);
            rsMap = new HashMap<String, Object>();
            if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
                HttpEntity entity = response.getEntity();
                Object json = null;
                String s = EntityUtils.toString(entity);
                //			Log.d(TAG, "response result:" + s);
                try {
                    json = new Gson().fromJson(s, HashMap.class);
                } catch (JsonSyntaxException e) {
                    json = new Gson().fromJson(s, ArrayList.class);
                }
                //			Log.d("BioChecker", "json:" + json.toString());
                String message = null;
                if (json instanceof Map) {
                    try {
                        message = (String) ((Map<String, Object>) json).get("message");
                    } catch (Exception e) {
                    }
                }

                if (message == null) {
                    rsMap.put("code", 1);
                    rsMap.put("data", json);
                } else {
                    rsMap.put("code", -1);
                    rsMap.put("message", message);
                }
            } else {
                rsMap.put("code", -1);
                rsMap.put("message", response.getStatusLine().getReasonPhrase());
                Log.e(TAG, response.getStatusLine().getStatusCode() + " : "
                        + response.getStatusLine().getReasonPhrase());
            }
        } catch (Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }
        return rsMap;
    }

    private static Map<String, Object> signRequestByPost(String url,
                                                         Object params) throws IOException,
            ParseException, JSONException {
        Map<String, Object> rsMap = null;
        HttpPost postMethod = new HttpPost(url + "?" + api_key);
        postMethod.setHeader("Accept", "application/json");
        postMethod.setHeader("Content-Type", "application/json");
        String s;
        if (params instanceof Map) {
            Map<String, Object> map = (Map<String, Object>) params;
            s = new JSONObject(map).toString();
        } else if (params instanceof List) {
            List list = (List) params;
            s = new JSONArray(list).toString();
        } else {
            s = params.toString();
        }

        Log.d("BioChecker", url + "\t" + s);
        StringEntity se = new StringEntity(s);
        postMethod.setEntity(se);

        HttpClient client = HttpClientHelper.getHttpClient();
        HttpResponse response = client.execute(postMethod);

        if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
            HttpEntity entity = response.getEntity();
            JSONObject json = new JSONObject(EntityUtils.toString(entity));
            Log.d("BioChecker", json.toString());
            rsMap = (Map<String, Object>) json2MapORList(json);
            if (rsMap.get("message") == null) {
                rsMap.put("code", 1);
            } else {
                rsMap.put("code", -1);
            }
        } else {
            rsMap = new HashMap<>();
            rsMap.put("code", -1);
            rsMap.put("message", response.getStatusLine().getReasonPhrase());
            Log.e(TAG, response.getStatusLine().getStatusCode() + ":"
                    + response.getStatusLine().getReasonPhrase());
        }
        return rsMap;
    }

    private static Object json2MapORList(Object obj) throws JSONException {
        if (obj instanceof JSONObject) {
            Map<String, Object> map = new HashMap<String, Object>();
            JSONObject json = (JSONObject) obj;
            Iterator<String> iter = json.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                Object value = json.get(key);
                value = json2MapORList(value);
                map.put(key, value);
            }
            return map;
        } else if (obj instanceof JSONArray) {
            JSONArray jsonArray = (JSONArray) obj;
            List<Object> list = new ArrayList<Object>();
            for (int i = 0; i < jsonArray.length(); i++) {
                list.add(json2MapORList(jsonArray.get(i)));
            }
            return list;
        } else {
            return obj;
        }
    }

}
