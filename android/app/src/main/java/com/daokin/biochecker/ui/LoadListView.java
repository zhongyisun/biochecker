package com.daokin.biochecker.ui;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * @author Paul
 */
public class LoadListView extends ListView implements OnScrollListener {
    private View footer;//footer
    private TextView load_text;
    private ProgressBar load_pb;
    private ILoadListener loadListener;
    private IRefreshListener refreshListener;
    private String query;

    View header;// header
    int headerHeight;// header height
    int firstVisibleItem;// first visible item
    int scrollState;// listview scrolling
    boolean isRemark;//
    int startY;

    int state;
    final int NONE = 0;
    final int PULL = 1;
    final int RELEASE = 2;
    final int REFLASHING = 3;
    final int SPEAC = 30;

    private boolean isLoading;
    private int currentPage;

    int lastItem;
    int totalItem;

    public void setRefreshListener(IRefreshListener refreshListener) {
        this.refreshListener = refreshListener;
    }

    public void setLoadListener(ILoadListener loadListener) {
        this.loadListener = loadListener;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * Refresh interface
     */
    public interface IRefreshListener {
        public void onRefresh(String query);
    }

    /**
     * Load interface
     */
    public interface ILoadListener {
        public void onLoad(String query);
    }

    public void onRefresh() {
        if (refreshListener != null) {
            refreshListener.onRefresh(query);
        }
    }

    public void onLoad() {
        if (loadListener != null) {
            loadListener.onLoad(query);
        }
    }

    /**
     * load complete
     *
     * @param isLoading
     */
    public void loadComplete() {
        isLoading = false;
        load_pb.setVisibility(View.GONE);
        load_text.setVisibility(View.GONE);
    }

    public LoadListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public LoadListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public LoadListView(Context context) {
        super(context);
        initView(context);
    }

    /**
     * add view to bottom listView
     *
     * @param
     */
    private void initView(Context context) {
        //footer
        footer = inflate(context, R.layout.footer_layout, null);
        load_text = (TextView) (footer.findViewById(R.id.load_text));
        load_pb = (ProgressBar) footer.findViewById(R.id.load_pb);
        //header
        LayoutInflater inflater = LayoutInflater.from(context);
        header = inflater.inflate(R.layout.header_layout, null);
        measureView(header);
        headerHeight = header.getMeasuredHeight();
        topPadding(-headerHeight);
        this.addHeaderView(header);
        this.addFooterView(footer);
        this.setOnScrollListener(this);
    }

    /**
     * @param view
     */
    private void measureView(View view) {
        ViewGroup.LayoutParams p = view.getLayoutParams();
        if (p == null) {
            p = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        int width = ViewGroup.getChildMeasureSpec(0, 0, p.width);
        int height;
        int tempHeight = p.height;
        if (tempHeight > 0) {
            height = MeasureSpec.makeMeasureSpec(tempHeight,
                    MeasureSpec.EXACTLY);
        } else {
            height = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        }
        view.measure(width, height);
    }

    /**
     * @param topPadding
     */
    private void topPadding(int topPadding) {
        header.setPadding(header.getPaddingLeft(), topPadding,
                header.getPaddingRight(), header.getPaddingBottom());
        header.invalidate();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        this.firstVisibleItem = firstVisibleItem;
        this.lastItem = (firstVisibleItem + visibleItemCount) > totalItemCount ? totalItemCount : (firstVisibleItem + visibleItemCount);
        this.totalItem = totalItemCount;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.scrollState = scrollState;
        ifNeedLoad(view, scrollState);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (firstVisibleItem == 0) {
                    isRemark = true;
                    startY = (int) ev.getY();
                }
                break;
            case MotionEvent.ACTION_MOVE:
                onMove(ev);
                break;
            case MotionEvent.ACTION_UP:
                if (state == RELEASE) {
                    state = REFLASHING;
                    reflashHeaderViewByState();
                    onRefresh();
                } else if (state == PULL) {
                    state = NONE;
                    isRemark = false;
                    reflashHeaderViewByState();
                }
                isRemark = false;
                break;
        }
        return super.onTouchEvent(ev);
    }

    /**
     * @param ev
     */
    private void onMove(MotionEvent ev) {
        if (!isRemark) {
            return;
        }
        int tempY = (int) ev.getY();
        int space = tempY - startY;
        int topPadding = space - headerHeight;
        switch (state) {
            case NONE:
                if (space > 0) {
                    state = PULL;
                    reflashHeaderViewByState();
                }
                break;
            case PULL:
                topPadding(topPadding);
                if (space > headerHeight + SPEAC
                        && scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                    state = RELEASE;
                    reflashHeaderViewByState();
                }
                break;
            case RELEASE:
                topPadding(topPadding);
                if (space < headerHeight + SPEAC) {
                    state = PULL;
                    reflashHeaderViewByState();
                } else if (space <= 0) {
                    state = NONE;
                    isRemark = false;
                    reflashHeaderViewByState();
                }
                break;
        }
    }

    /**
     */
    private void reflashHeaderViewByState() {
        TextView tip = (TextView) header.findViewById(R.id.tip);
        ImageView arrow = (ImageView) header.findViewById(R.id.arrow);
        ProgressBar progress = (ProgressBar) header.findViewById(R.id.progress);
        RotateAnimation anim = new RotateAnimation(0, 180,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(500);
        anim.setFillAfter(true);
        RotateAnimation anim1 = new RotateAnimation(180, 0,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        anim1.setDuration(500);
        anim1.setFillAfter(true);
        switch (state) {
            case NONE:
                arrow.clearAnimation();
                topPadding(-headerHeight);
                break;

            case PULL:
                arrow.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                tip.setText("Pull To Reflash");
                arrow.clearAnimation();
                arrow.setAnimation(anim1);
                break;
            case RELEASE:
                arrow.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                tip.setText("Release To Reflash");
                arrow.clearAnimation();
                arrow.setAnimation(anim);
                break;
            case REFLASHING:
                topPadding(50);
                arrow.setVisibility(View.GONE);
                progress.setVisibility(View.VISIBLE);
                tip.setText("Reflashing...");
                arrow.clearAnimation();
                break;
        }
    }

    /**
     */
    public void reflashComplete() {
        state = NONE;
        isRemark = false;
        reflashHeaderViewByState();
        TextView lastupdatetime = (TextView) header
                .findViewById(R.id.lastupdate_time);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        String time = format.format(date);
        lastupdatetime.setText(time);
    }

    private void ifNeedLoad(AbsListView view, int scrollState) {
        try {
            if (scrollState == OnScrollListener.SCROLL_STATE_IDLE
                    && !isLoading
                    && view.getLastVisiblePosition() == view.getPositionForView(footer)) {
                load_pb.setVisibility(View.VISIBLE);
                load_text.setVisibility(View.VISIBLE);
                onLoad();
                isLoading = true;
            }
        } catch (Exception e) {
        }
    }

}
