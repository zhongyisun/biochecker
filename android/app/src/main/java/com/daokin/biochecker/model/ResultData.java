package com.daokin.biochecker.model;

import java.util.List;
import java.util.Map;

/**
 * Created by SamXCode on 2016/1/8.
 */
public class ResultData {
    private Map<String, SensorResult> sensor;
    private List<AnalyteResult> assay;

    public Map<String, SensorResult> getSensor() {
        return sensor;
    }

    public void setSensor(Map<String, SensorResult> sensor) {
        this.sensor = sensor;
    }

    public List<AnalyteResult> getAssay() {
        return assay;
    }

    public void setAssay(List<AnalyteResult> assay) {
        this.assay = assay;
    }

    public class SensorResult {
        private List<Float> result;

        public List<Float> getResult() {
            return result;
        }

        public void setResult(List<Float> result) {
            this.result = result;
        }
    }

    public class AnalyteResult {
        private int analyte;
        private String analyteName;
        private List<Float> result;

        public int getAnalyte() {
            return analyte;
        }

        public void setAnalyte(int analyte) {
            this.analyte = analyte;
        }

        public String getAnalyteName() { return analyteName; }

        public void setAnalyteName(String analyteName) {this.analyteName = analyteName; }

        public List<Float> getResult() {
            return result;
        }

        public void setResult(List<Float> result) {
            this.result = result;
        }
    }

}
