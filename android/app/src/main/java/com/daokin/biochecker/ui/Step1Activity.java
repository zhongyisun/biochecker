package com.daokin.biochecker.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.daokin.biochecker.model.AssayData;
import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ProcessedResult.TestChip.AssayPrinting;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.model.SharedApplicationData;
import com.daokin.biochecker.util.DBHelper;
import com.daokin.biochecker.util.RequestUtils;
import com.daokin.biochecker.util.Tools;
import com.daokin.biochecker.zxing.activity.CaptureActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Step1Activity extends Activity {
    private static final String TAG = "BioChecker";
    private Button qrscan_btn = null;
    private TextView tv_home = null;
    private TextView tv_cancel = null;
    private TextView tv_msg = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step1_layout);
        Log.d(TAG, "Step1Activity start");
        qrscan_btn = (Button) findViewById(R.id.qrscan_btn);
        tv_home = (TextView) findViewById(R.id.tv_home);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        tv_msg = (TextView) findViewById(R.id.tv_msg);

        qrscan_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent openCameraIntent = new Intent(Step1Activity.this,
                        CaptureActivity.class);
                startActivityForResult(openCameraIntent, 0);
            }
        });
        tv_home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Step1Activity.this.finish();
            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Step1Activity.this.finish();
            }
        });
        tv_home.setVisibility(View.INVISIBLE);
        tv_cancel.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Long chipId = data.getLongExtra("chipId", 0L);
            Long chipTypeId = data.getLongExtra("typeId", 0L);
            String QCFailedSensor = data.getStringExtra("QCFailedSensor");
            ModelContainer.i.setChipId(chipId);
            ModelContainer.i.setChipAssayTypeId(chipTypeId);
            qrscan_btn.setText("Loading chip message...");
            qrscan_btn.setClickable(false);
            new GetAssayDataTask().execute(chipId, chipTypeId, QCFailedSensor);
        }
    }

    class GetAssayDataTask extends AsyncTask<Object, Void, Integer> {
        private Long chipId;
        private String msg;
        private AssayData aData;

        @Override
        protected Integer doInBackground(Object... params) {
            chipId = (Long) params[0];
            Long typeId = (Long) params[1];
            String QCFailedSensor = (String) params[2];
            if (SharedApplicationData.isOffline()) {
                DBHelper dbHelper = new DBHelper(getApplicationContext());
                aData = dbHelper.getAssayData(chipId);
            } else {
                ResponseModel<AssayData> model = RequestUtils.getAssays(typeId);
                if (model.getCode() != -1) {
                    aData = model.getData();
                    ModelContainer.i.setAssayData(aData);
                } else {
                    msg = model.getMessage();
                    return -1;
                }
            }
            if (aData != null) {
                try {
                    Map<String, Byte> sensorMap = aData.getLableMux1Map();
                    List<AssayPrinting> aps = aData.getAssayPrintings(sensorMap);
                    ModelContainer.i.setAssayPrintings(aps);
                    List<Byte> qcFailedSensorList = getQCFailedSensors(QCFailedSensor, sensorMap);
                    ModelContainer.i.setQcFailedSensors(qcFailedSensorList);
                    ModelContainer.i.setMux1LabelMap(aData.getMux1LabelMap());
                    ModelContainer.i.setLabelMux1Map(aData.getLableMux1Map());
                    ModelContainer.i.setLabelMux2Map(aData.getLabelMux2Map());
                    ModelContainer.i.setReferenceSensorList(aData.getReferenceSensorList());
                    ModelContainer.i.setMaxDuration(aData.getMaxDuration());
                    return 1;
                } catch (Exception e) {
                    msg = e.getMessage();
                    Log.d(TAG, null, e);
                    return -1;
                }
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (result == -1) {
                Toast.makeText(Step1Activity.this, msg, Toast.LENGTH_LONG).show();
                qrscan_btn.setText("Scan qr code");
            } else if (result == 0) {
                if (SharedApplicationData.isOffline()) {
                    Toast.makeText(Step1Activity.this, "The chip is no print data offline!",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(Step1Activity.this, msg, Toast.LENGTH_LONG).show();
                }
                qrscan_btn.setText("Scan qr code");
                tv_msg.setText("The chip with id [" + chipId
                        + "] has no print data!");
            } else {
                ModelContainer.i.setAssayData(aData);
                tv_msg.setText("Chip id:" + chipId + "\nAssay Names: " +
                        ModelContainer.i.getAssayNames());
                qrscan_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Step1Activity.this,
                                Step2Activity.class);
                        startActivity(intent);
                        Step1Activity.this.finish();
                    }
                });
                qrscan_btn.setText("continue");
            }
            qrscan_btn.setClickable(true);
        }

        private List<Byte> getQCFailedSensors(String QCFailedSensor,
                                              Map<String, Byte> sensorMap) {
            String[] helpArray = {"A", "B", "C", "D", "E", "F"};
            List<Byte> list = new ArrayList<>();
            if (QCFailedSensor != null && Tools.validateQCFailedSensorMark(QCFailedSensor)) {
                int len = QCFailedSensor.length();
                for (int i = 0; i < len; i += 2) {
                    byte b = Byte.valueOf(QCFailedSensor.substring(i, i + 2), 16);
                    for (int j = 1; j <= 8; j++) {
                        if ((b & 1) == 1) {
                            list.add(sensorMap.get(helpArray[i / 2] + j));
                        }
                        b >>= 1;
                    }
                }
            }
            return list;
        }
    }
}
