package com.daokin.biochecker.ui;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Factory;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.daokin.biochecker.model.Location;
import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.model.SharedApplicationData;
import com.daokin.biochecker.util.RequestUtils;
import com.daokin.biochecker.view.LocationListDialog;
import com.daokin.biochecker.zxing.activity.CaptureActivity;

public class HomeActivity extends Activity {
    private static final String TAG = "BioChecker";
    private TextView tv_username = null;
    private ListView listView = null;
    //	 private static String BT_NAME = "DREW";
    private static String BT_NAME = ModelContainer.i.getBTName();
    //    private static String BT_NAME = "";
    private Button start_test_btn = null;
    private BluetoothAdapter bluetoothAdapter;
    private List<BluetoothDevice> devices = null;
    private List<Map<String, String>> dataMap = new ArrayList<>();
    private ListViewAdapter adapter = null;
    // Sample process status 0: processing; 1: not start or finished
    private int searchTime = 12;

    private NfcAdapter mNfcAdapter;
    private PendingIntent mPendingIntent;
    private IntentFilter[] mIntentFilters;
    private String[][] mNFCTechLists;

    private boolean NFCTagFound = false;
    private ActionBar mActionBar;
    private static List<Location> locations;
    private LocationListDialog locationsDialog;
    private Dialog loadDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_layout);
        mActionBar = getActionBar();
        mActionBar.setTitle("Create New Test");
        mActionBar.setDisplayHomeAsUpEnabled(false);

        listView = (ListView) findViewById(R.id.list_view);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(HomeActivity.this,
                        Step1Activity.class);
                ModelContainer.i.setDevice(devices.get(position));
                startActivity(intent);
            }
        });
        adapter = new com.daokin.biochecker.ui.ListViewAdapter(this, dataMap);
        listView.setAdapter(adapter);
        start_test_btn = (Button) findViewById(R.id.start_test_btn);
        start_test_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                start_test_btn.setClickable(false);
                searchBluetooth();
            }
        });
try {
    tv_username = (TextView) findViewById(R.id.tv_username);
    tv_username.setText("Hi, " + ModelContainer.i.getUser().getFirstName()
            + " " + ModelContainer.i.getUser().getLastName());
} catch (Exception ex) {
    //Log.d("##########", ex.getMessage());
}
        // deviceName.setText("Searching RNBT device...");
        //Log.d(TAG, "Searching paired RNBT device...");
        // search Bluetooth devices
        devices = new ArrayList<>();
        // Sample_Process_Status = 1;
        // start_test_btn.setText("Searching RNBT devices...");
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        start_test_btn.setClickable(false);
        searchBluetooth();

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (mNfcAdapter != null) {
            // mTextView.setText("Read an NFC tag");

            mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

            if (mNfcAdapter != null) {
                // mTextView.setText("Read an NFC tag");

                // create an intent with tag data and deliver to this activity
                mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(
                        this, getClass())
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

                // set an intent filter for all MIME data
                IntentFilter ndefIntent = new IntentFilter(
                        NfcAdapter.ACTION_NDEF_DISCOVERED);
                try {
                    ndefIntent.addDataType("*/*");
                    mIntentFilters = new IntentFilter[]{ndefIntent};
                } catch (Exception e) {
                    Log.e("TagDispatch", e.toString());
                }

                mNFCTechLists = new String[][]{new String[]{NfcF.class
                        .getName()}};

            } else {
                // mTextView.setText("This phone is not NFC enabled.");
            }

        } else {
            // mTextView.setText("This phone is not NFC enabled.");
        }

    }

    @Override
    public void onNewIntent(Intent intent) {
        if (intent.getBooleanExtra("exit", false)) {
            finish();
            return;
        }
        super.onNewIntent(intent);
        setIntent(intent);
        String action = intent.getAction();
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

        String s = action + "\n\n" + tag.toString();

        // parse through all NDEF messages and their records and pick text type
        // only
        Parcelable[] data = intent
                .getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

        if (data != null) {
            try {
                for (int i = 0; i < data.length; i++) {
                    NdefRecord[] recs = ((NdefMessage) data[i]).getRecords();
                    for (int j = 0; j < recs.length; j++) {
                        if (recs[j].getTnf() == NdefRecord.TNF_WELL_KNOWN
                                && Arrays.equals(recs[j].getType(),
                                NdefRecord.RTD_TEXT)) {

                            byte[] payload = recs[j].getPayload();
                            String textEncoding = ((payload[0] & 0200) == 0) ? "UTF-8"
                                    : "UTF-16";
                            int langCodeLen = payload[0] & 0077;

                            s += ("\n\nNdefMessage["
                                    + i
                                    + "], NdefRecord["
                                    + j
                                    + "]:\n\""
                                    + new String(payload, langCodeLen + 1,
                                    payload.length - langCodeLen - 1,
                                    textEncoding) + "\"");

                            String deviceName = new String(payload,
                                    langCodeLen + 1, payload.length
                                    - langCodeLen - 1, textEncoding);

                            for (BluetoothDevice d : devices) {
                                if (d.getName().equals(deviceName)) {
                                    if (d.getBondState() == BluetoothDevice.BOND_NONE) {
                                        try {
                                            NFCTagFound = true;
                                            Method createBondMethod = d.getClass().getMethod(
                                                    "createBond");
                                            createBondMethod.invoke(d);
                                            if (d.getBondState() == BluetoothDevice.BOND_BONDED) {
                                                Intent intent1 = new Intent(
                                                        HomeActivity.this,
                                                        Step1Activity.class);
                                                ModelContainer.i.setDevice(d);
                                                startActivity(intent1);
                                            }
                                        } catch (Exception e) {
                                            Log.e("TagDispatch", e.toString());
                                        }
                                    } else if (d.getBondState() == BluetoothDevice.BOND_BONDED) {
                                        Intent intent1 = new Intent(
                                                HomeActivity.this, Step1Activity.class);
                                        ModelContainer.i.setDevice(d);
                                        startActivity(intent1);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("TagDispatch", e.toString());
            }

        }

        // mTextView.setText(s);
        //Log.d(TAG, s);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getIntent().getBooleanExtra("exit", false)) {
            finish();
            return;
        }
        if (mNfcAdapter != null)
            mNfcAdapter.enableForegroundDispatch(this, mPendingIntent,
                    mIntentFilters, mNFCTechLists);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mNfcAdapter != null)
            mNfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(receiver);
        } catch (IllegalArgumentException e) {
            if (!e.getMessage().contains("Receiver not registered")) {
                throw e;
            }
        } catch (Exception e) {
            throw e;
        }
        if (loadDialog != null) {
            loadDialog.dismiss();
            loadDialog = null;
        }
        if (locationsDialog != null) {
            locationsDialog.dismiss();
            locationsDialog = null;
        }
        super.onDestroy();
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Log.d(TAG, "receiver onReceive");
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //Log.d(TAG, "receiver ACTION_FOUND");
                BluetoothDevice eDevice = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (eDevice != null) {
                    try {
//                        Log.d(TAG, "Found device. Name: " + eDevice.getName()
//                                + ", address: " + eDevice.getAddress() + ", BT_NAME:" + BT_NAME
//                                + ", match: " + eDevice.getName().startsWith(BT_NAME));
                        if (eDevice.getName() != null
                                && eDevice.getName().startsWith(BT_NAME)) {
                            devices.add(eDevice);
                            if (eDevice.getBondState() == BluetoothDevice.BOND_BONDED) {
                                Map<String, String> devicesMap = new HashMap<>();
                                devicesMap.put("deviceName", eDevice.getName());
                                dataMap.add(devicesMap);
                                adapter.notifyDataSetChanged();
                            }
                            if (NFCTagFound) {
                                NFCTagFound = false;
                            }
                        }
                    }
                    catch (Exception ex) {
                        //System.out.println(ex.getMessage());
                    }
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
                    .equals(action)) {
                //Log.d(TAG, "receiver ACTION_DISCOVERY_FINISHED");
                if (devices == null || devices.isEmpty()) {
                    Toast.makeText(HomeActivity.this,
                            "Can not find any paired RNBT device.",
                            Toast.LENGTH_SHORT).show();
                    start_test_btn.setText("Search again");
                    start_test_btn.setClickable(true);
                } else {
                    start_test_btn.setText("Search again");
                    start_test_btn.setClickable(true);
                }
            }
        }
    };

    private void searchBluetooth() {
        devices = new ArrayList<>();
        dataMap.clear();
        adapter.notifyDataSetChanged();
        Message message = timeHandler.obtainMessage(1); // Message
        timeHandler.sendMessageDelayed(message, 1000);

        if (!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        }

        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }

        bluetoothAdapter.startDiscovery();
        //Log.d(TAG, "bluetooth Adapter start Discovery");
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(receiver, filter);

        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(receiver, filter);
    }

    // time handler
    final Handler timeHandler = new Handler() {

        public void handleMessage(Message msg) { // handle message
            switch (msg.what) {
                case 1:
                    searchTime--;
                    start_test_btn.setText("Searching paired RNBT devices...("
                            + searchTime + "s)");

                    if (searchTime > 0) {
                        Message message = timeHandler.obtainMessage(1);
                        timeHandler.sendMessageDelayed(message, 1000); // send
                        // message
                    } else {
                        searchTime = 12;
                    }
            }

            super.handleMessage(msg);
        }
    };

    private long mExitTime;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                Toast.makeText(this, "Click again to exit!", Toast.LENGTH_SHORT)
                        .show();
                mExitTime = System.currentTimeMillis();
            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        if (SharedApplicationData.isOffline()) {
            menu.findItem(R.id.options_download_chip_data).setVisible(false);
        }
        if (android.os.Build.VERSION.SDK_INT < 3.0) {
            setMenuBackground();
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options_new_test:
                return true;
            case R.id.options_download_chip_data:
                Intent intent = new Intent(HomeActivity.this, CaptureActivity.class);
                intent.putExtra("last_activity", "HomeActivity");
                startActivity(intent);
                return true;
            case R.id.options_view_chip_data:
                startActivity(new Intent(HomeActivity.this, ChipListActivity.class));
                return true;
            case R.id.options_recent_tests:
                startActivity(new Intent(HomeActivity.this, TestListActivity.class));
                return true;
            case R.id.options_test_by_location_tests:
                getLocations();
                return true;
            case R.id.options_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void getLocations() {
        showLoadingDialog();
        if (locations != null) {
            hideLoadingDialog();
            locationsDialog = new LocationListDialog(HomeActivity.this,
                    HomeActivity.this, locations);
            locationsDialog.show();
            return;
        }
        AsyncTask<String, Void, ResponseModel<List<Location>>> loadDataTask =
                new AsyncTask<String, Void, ResponseModel<List<Location>>>() {

                    @Override
                    protected ResponseModel<List<Location>> doInBackground(
                            String... params) {
                        return RequestUtils.getLocations();
                    }

                    @Override
                    protected void onPostExecute(ResponseModel<List<Location>> result) {
                        hideLoadingDialog();
                        if (result == null || result.getCode() == -1) {
                            Toast.makeText(HomeActivity.this, result.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            if (result.getData() == null
                                    || result.getData().size() == 0) {
                                Toast.makeText(HomeActivity.this, "Not Location Data",
                                        Toast.LENGTH_LONG).show();
                            } else {
                                locations = result.getData();
                                if (locationsDialog != null) {
                                    locationsDialog.dismiss();
                                }
                                locationsDialog = new LocationListDialog(
                                        HomeActivity.this, HomeActivity.this, locations);
                                locationsDialog.show();
                            }
                        }
                    }
                };
        loadDataTask.execute(new String[]{});
    }

    // for sdk.version < 3.2
    protected void setMenuBackground() {

        //Log.d(TAG, "Enterting setMenuBackGround");
        getLayoutInflater().setFactory(new Factory() {

            @Override
            public View onCreateView(String name, Context context,
                                     AttributeSet attrs) {

                if (name.equalsIgnoreCase("com.android.internal.view.menu.IconMenuItemView")) {

                    try { // Ask our inflater to create the view
                        LayoutInflater f = getLayoutInflater();
                        final View view = f.createView(name, null, attrs);
                        new Handler().post(new Runnable() {
                            public void run() {
                                new Color();
                                // view.setBackgroundResource(
                                // R.drawable.menu_backg);
                                view.setBackgroundColor(Color.BLACK);
                            }
                        });
                        return view;
                    } catch (InflateException e) {
                    } catch (ClassNotFoundException e) {
                    }
                }
                return null;
            }
        });
    }

    public void showLoadingDialog() {
        if (loadDialog == null) {
            loadDialog = new LoadingProgress(this, R.style.MyDialog);
            loadDialog.show();
        } else {
            loadDialog.show();
        }
    }

    public void hideLoadingDialog() {
        if (loadDialog != null) {
            loadDialog.dismiss();
        }
    }


}
