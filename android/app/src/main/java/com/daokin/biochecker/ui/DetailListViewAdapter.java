package com.daokin.biochecker.ui;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import com.daokin.biochecker.util.Tools;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author Adam
 * 
 */
@SuppressLint("ViewHolder")
public class DetailListViewAdapter extends BaseAdapter {

	private List<Map<String, String>> list;
	private Context context;
	private static Handler handler = new Handler();

	public DetailListViewAdapter(Context context, List<Map<String, String>> list) {
		this.list = list;
		this.context = context;
	}

	public int getCount() {
		return list.size();
	}

	public Object getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		MyDetailListView tag;
		View v = LayoutInflater.from(context).inflate(
				R.layout.test_detail_item_layout, null);
		if (null == convertView) {			
			tag = new MyDetailListView();
			tag.sampleImageView = (ImageView) v
					.findViewById(R.id.sample_imageView);
			tag.minImageView = (ImageView) v.findViewById(R.id.min_imageView);
			tag.maxImageView = (ImageView) v.findViewById(R.id.max_imageView);
			tag.assayNameView = (TextView) v.findViewById(R.id.assay_name);
			tag.testLevelView = (TextView) v.findViewById(R.id.test_levels);

			v.setTag(tag);
			convertView = v;
		} else {
			tag = (MyDetailListView) convertView.getTag();
		}
		String sample_image_tag = list.get(position).get("sampleValue");
		String min_image_tag = list.get(position).get("minValue");
		String max_image_tag = list.get(position).get("maxValue");
		String assayName = list.get(position).get("assayName");
		String testLevel = list.get(position).get("sampleValue");
		tag.assayNameView.setText(assayName);
		tag.testLevelView.setText(testLevel);
		
		tag.sampleImageView.setTag(sample_image_tag);
		tag.minImageView.setTag(min_image_tag);
		tag.maxImageView.setTag(max_image_tag);
		
		double sampleValue = Double.parseDouble(sample_image_tag);
		double minValue = Double.parseDouble(min_image_tag);
		double maxValue = Double.parseDouble(max_image_tag);
		double[] percents = referPercentValue(sampleValue, minValue, maxValue);
		Paint p = new Paint();
		p.setColor(Color.RED);
		Bitmap bitmap = BitmapFactory.decodeResource(v.getResources(), R.drawable.abc_list_pressed_holo_dark);
		Bitmap bitmap1 = bitmap.copy(bitmap.getConfig(), true);
		Bitmap bitmap2 = bitmap.copy(bitmap.getConfig(), true);
		Bitmap bitmap3 = bitmap.copy(bitmap.getConfig(), true);
		
		Canvas canvas1 = new Canvas(bitmap1);
		canvas1.drawRect(0, 0, (int)(bitmap1.getWidth() * percents[0]), (int)bitmap1.getHeight(), p);
		tag.sampleImageView.setImageBitmap(bitmap1);
		
		Canvas canvas2 = new Canvas(bitmap2);
		p.setColor(Color.GREEN);
		canvas2.drawRect(0, 0, (int)(bitmap2.getWidth() * percents[1]), (int)bitmap2.getHeight(), p);
		tag.minImageView.setImageBitmap(bitmap2);
		
		Canvas canvas3 = new Canvas(bitmap3);
		p.setColor(Color.BLUE);
		canvas3.drawRect(0, 0, (int)(bitmap3.getWidth() * percents[2]), (int)bitmap3.getHeight(), p);
		tag.maxImageView.setImageBitmap(bitmap3);
		
		return convertView;
	}
	
	private double[] referPercentValue(double sampleValue, double minValue, double maxValue){
		double referValue = Tools.getReferValue(sampleValue, minValue, maxValue);
		double samplePercentValue = 0.5;
		double minPercentValue = 0.5;
		double maxPercentValue = 0.5;
		if (referValue> 0) {
			samplePercentValue = (double) sampleValue / referValue;
			minPercentValue = (double) minValue / referValue;
			maxPercentValue = (double) maxValue / referValue;
		}
		double[] resultValues = {samplePercentValue, minPercentValue, maxPercentValue};
 		return resultValues;
	}
	
	
}

class DownloadImagesTask extends AsyncTask<ImageView, Void, Bitmap> {
	ImageView imageView = null;
	String imageUrl;
	Handler handler;

	public DownloadImagesTask(Handler handler) {
		this.handler = handler;
	}

	@Override
	protected Bitmap doInBackground(ImageView... imageViews) {
		imageView = imageViews[0];
		Bitmap bit = imageView.getDrawingCache();
		if (bit != null) {
			// imageView.setImageBitmap(bit);
			return bit;
		} else {
			imageUrl = (String) imageViews[0].getTag();
			return download_Image(imageUrl);
		}
	}

	@Override
	protected void onPostExecute(final Bitmap result) {
		if (imageUrl.equals(imageView.getTag())) {
			// imageView.setImageBitmap(result);
			handler.post(new Runnable() {
				public void run() {
					imageView.setImageBitmap(result);
				}
			});

		}
	}

	private Bitmap download_Image(String url) {
		// ---------------------------------------------------
		Bitmap bm = null;
		try {
			URL aURL = new URL(url);
			URLConnection conn = aURL.openConnection();
			conn.connect();
			InputStream is = conn.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(is);
			bm = BitmapFactory.decodeStream(bis);
			bis.close();
			is.close();
		} catch (IOException e) {
			Log.e("Hub", "Error getting the image from server : "
					+ e.getMessage().toString());
		}
		return bm;
		// ---------------------------------------------------
	}

}

class MyDetailListView {
	ImageView sampleImageView;
	ImageView minImageView;
	ImageView maxImageView;
	TextView assayNameView;
	TextView testLevelView;
}
