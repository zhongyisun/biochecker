package com.daokin.biochecker.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daokin.biochecker.view.TextProgressBar;

import java.util.Map;

public class TestDetailActivity extends Activity {

    private ActionBar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_detail_layout);
        Intent intent = this.getIntent();
        mActionBar = getActionBar();
        mActionBar.setTitle(intent.getStringExtra("testName"));
        mActionBar.setSubtitle(intent.getStringExtra("testDate"));
        LinearLayout resultLayout = (LinearLayout) findViewById(R.id.test_result);
        //Map<String, Step3Activity.ResultDisplay> map = (Map<String, Step3Activity.ResultDisplay>) intent.getSerializableExtra("result");
        Map<String, String> map = (Map<String, String>)intent.getSerializableExtra("result");
        //for (Map.Entry<String, Step3Activity.ResultDisplay> entry : map.entrySet()) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.test_detail_result_layout, resultLayout, false);
            TextView tv = (TextView) view.findViewById(R.id.ota_tv);
            tv.setText(entry.getKey());
            TextProgressBar tpb = (TextProgressBar) view.findViewById(R.id.ota_progressbar);
            String value = entry.getValue();
            tpb.setText(value);
            resultLayout.addView(view);
        }

//        Fragment tabAContent = new TabAFragment(this, intent.getStringExtra("testProcessedResults"));
//        Fragment tabBContent = new TabBFragment();
//
//        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
//        mActionBar.addTab(mActionBar.newTab().setText("DRINKING WATER")
//                .setTabListener(new DetailTabListener(tabAContent, this)));
//        mActionBar.addTab(mActionBar.newTab().setText("RIVERS & LAKES")
//                .setTabListener(new DetailTabListener(tabBContent, this)));

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu_bar, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.options_new_test:
                startActivity(new Intent(TestDetailActivity.this,
                        HomeActivity.class));
                finish();
                return true;
            case R.id.options_recent_tests:
                startActivity(new Intent(TestDetailActivity.this,
                        TestListActivity.class));
                finish();
                return true;
            case R.id.options_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


//    class TabBFragment extends Fragment {
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            return inflater.inflate(R.layout.test_detail_tab_2, container, false);
//        }
//
//    }

//    class TabAFragment extends Fragment {
//        private Activity mActivity;
//        private DetailListViewAdapter adapter = null;
//        private List<Map<String, String>> dataMap = null;
//        private ListView listView = null;
//        private String testProcessedResultJson = null;
//
//        public TabAFragment(Activity activity, String testProcessedResultJson) {
//            this.mActivity = activity;
//            this.testProcessedResultJson = testProcessedResultJson;
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View view = inflater.inflate(R.layout.test_detail_tab_1, container, false);
//            listView = (ListView) view.findViewById(R.id.detail_listView);
//            getData(null);
//            return view;
//        }
//
//
//        private void showData() {
//            adapter = new DetailListViewAdapter(mActivity, dataMap);
//            listView.setAdapter(adapter);
//        }
//
//        private void getData(String status) {
//            if (dataMap == null) {
//                dataMap = new ArrayList<Map<String, String>>();
//            } else {
//                dataMap.clear();
//            }
//            try {
//                JSONObject jsonObject = new JSONObject(testProcessedResultJson);
//                Iterator<String> keys = jsonObject.keys();
//                while (keys.hasNext()) {
//                    String assayName = keys.next();
//                    JSONObject assayObj = jsonObject.getJSONObject(assayName);
//                    String assayAvgValue = assayObj.getString("avg");
//                    HashMap<String, String> item = new HashMap<String, String>();
//                    //item.put("assayName", assayName + ": " + assayAvgValue);
//                    item.put("assayName", assayName);
//                    item.put("minValue", assayAvgValue);
//                    item.put("maxValue", assayAvgValue);
//                    item.put("sampleValue", assayAvgValue);
//                    dataMap.add(item);
//                }
//                showData();
//                // System.out.println(keys.next());
//            } catch (JSONException e) {
//                // TODO Auto-generated catch block
//                Log.e(e.getMessage(), "TestDetailActivity");
//            }
//        }
//    }
//
//    class DetailTabListener implements ActionBar.TabListener {
//        private Fragment tabContentFragment;
//        private Activity activity;
//
//        public DetailTabListener(Fragment tabContentFragment, Activity activity) {
//            this.tabContentFragment = tabContentFragment;
//            this.activity = activity;
//        }
//
//        @Override
//        public void onTabSelected(Tab tab, FragmentTransaction ft) {
//            ft.add(R.id.fragment_place, tabContentFragment);
//        }
//
//        @Override
//        public void onTabUnselected(Tab tab, FragmentTransaction ft) {
//            ft.remove(tabContentFragment);
//        }
//
//        @Override
//        public void onTabReselected(Tab tab, FragmentTransaction ft) {
//        }
//    }
}
