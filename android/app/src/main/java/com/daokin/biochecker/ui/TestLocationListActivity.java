package com.daokin.biochecker.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ProcessedResult;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.util.RequestUtils;

public class TestLocationListActivity extends Activity implements
		LoadListView.ILoadListener, LoadListView.IRefreshListener {
	private SimpleAdapter adapter = null;
	private List<HashMap<String, Object>> data = null;
	private LoadListView listView = null;
	private TextView tv_showuser = null;
	private final int DEFAULT_PAGE_SIZE = 20;
	private Dialog loadDialog;
	private ActionBar mActionBar;
	private int locationId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_list_layout);
		mActionBar = getActionBar();
		mActionBar.setTitle("Tests By Location");
		mActionBar.setSubtitle("Location: " + getIntent().getStringExtra("locationName"));
		mActionBar.setDisplayHomeAsUpEnabled(false);
		listView = (LoadListView) this.findViewById(R.id.test_listView);
		locationId = getIntent().getIntExtra("locationId", -1);
		if (locationId == -1) {
			Toast.makeText(this, "Invalid Location Id", Toast.LENGTH_LONG).show();
			return;
		}
		getData(null, DEFAULT_PAGE_SIZE, 0, locationId);
		tv_showuser = (TextView) findViewById(R.id.tv_showuser);
		tv_showuser.setText("Hi, " + ModelContainer.i.getUser().getFirstName()
				+ " " + ModelContainer.i.getUser().getLastName());

		listView.setOnItemClickListener(new OnItemClickListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				HashMap<String, String> itemMap = (HashMap<String, String>) listView
						.getItemAtPosition(position);
				String testName = itemMap.get("testName");
				String testDate = itemMap.get("testDate");
				String testProcessedResults = itemMap.get("testProcessedResults");
				Intent intent = new Intent(TestLocationListActivity.this, TestDetailDemoActivity.class);
				intent.putExtra("testName", testName);
				intent.putExtra("testDate", testDate);
				intent.putExtra("testProcessedResults", testProcessedResults);
				intent.putExtra("afb", 25);
				intent.putExtra("ota", 110);
				intent.putExtra("result",0);
				startActivity(intent);
			}

		});
		showLoadingDialog();
	}

	public void showLoadingDialog() {
		if (loadDialog == null) {
			loadDialog = new LoadingProgress(this, R.style.MyDialog);
			loadDialog.show();
		} else {
			loadDialog.show();
		}
	}

	public void hideLoaddingDialog() {
		if (loadDialog != null) {
			loadDialog.dismiss();
		}
	}

	public void showData() {
		if (adapter == null) {
			adapter = new SimpleAdapter(this, data, R.layout.test_item_layout,
					new String[] { "testName", "testDate" }, new int[] {
							R.id.test_name, R.id.test_date });
			listView.setAdapter(adapter);
			listView.setRefreshListener(this);
			listView.setLoadListener(this);
		}
		adapter.notifyDataSetChanged();
	}

	public void getData(String status, final int pageSize, final int page, final int locationId) {
		if (data == null) {
			data = new ArrayList<HashMap<String, Object>>();
		}
		AsyncTask<String, Void, ResponseModel<List<ProcessedResult>>> loadDataTask = new AsyncTask<String, Void, ResponseModel<List<ProcessedResult>>>() {
			private String status;
			private int pageSize;
			private int page;
			private int locationId;

			@Override
			protected ResponseModel<List<ProcessedResult>> doInBackground(
					String... params) {
				this.status = params[0];
				this.pageSize = Integer.parseInt(params[1]);
				this.page = Integer.parseInt(params[2]);
				this.locationId = Integer.parseInt(params[3]);
					return RequestUtils.getProcessedResultsByLocation(ModelContainer.i
							.getUser().getZeptoCustomerID(), this.pageSize, this.page, this.locationId);
			}

			@Override
			protected void onPostExecute(
					ResponseModel<List<ProcessedResult>> result) {
				hideLoaddingDialog();
				if (result == null || result.getCode() == -1) {
					Toast.makeText(TestLocationListActivity.this, result.getMessage(),
							Toast.LENGTH_LONG).show();
				} else {
					if ("refresh".equals(status)) {
						// Clear old data
						data.clear();
					}
					List<ProcessedResult> list = result.getData();
					for (int i = 0; i < list.size(); i++) {
						ProcessedResult processedResult = list.get(i);
						HashMap<String, Object> item = new HashMap<String, Object>();
						item.put("testName", processedResult.getTest()
								.getName());
						item.put("testDate",
								processedResult.getSimpleProcessedTime());
						item.put("testProcessedResults",
								processedResult.getProcessedResult());
						data.add(item);
					}
					showData();
					if ("refresh".equals(status)) {
						listView.reflashComplete();
						listView.setCurrentPage(0);
					} else if ("load".equals(status)) {
						listView.setCurrentPage(page);
						listView.loadComplete();
					}
					if (list.size() == 0) {
						if (page == 0) {
							listView.loadComplete();
							Toast.makeText(TestLocationListActivity.this, "Not Found Data", Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(TestLocationListActivity.this, "No More Data", Toast.LENGTH_LONG).show();
						}
					}
				}
			}
		};
		loadDataTask.execute(new String[] { status, String.valueOf(pageSize),
				String.valueOf(page), String.valueOf(locationId) });
	}

	@Override
	public void onRefresh(String query) {
		getData("refresh", DEFAULT_PAGE_SIZE, 0, locationId);
	}

	@Override
	public void onLoad(String query) {
		getData("load", DEFAULT_PAGE_SIZE, listView.getCurrentPage() + 1, locationId);
	}

	private long mExitTime;

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if ((System.currentTimeMillis() - mExitTime) > 2000) {
				Toast.makeText(this, "Click again to exit!", Toast.LENGTH_SHORT)
						.show();
				mExitTime = System.currentTimeMillis();
			} else {
				finish();
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public boolean onCreateOptionsMenu(final Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.list_options_menu_bar, menu);
		MenuItem item = menu.findItem(R.id.options_test_by_location_tests);
		item.setVisible(false);
		menu.findItem(R.id.menu_search).setVisible(false);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.options_new_test:
			startActivity(new Intent(TestLocationListActivity.this,
					HomeActivity.class));
			TestLocationListActivity.this.finish();
			return true;
		case R.id.options_recent_tests:
			Intent intent = new Intent(TestLocationListActivity.this,
					TestListActivity.class);
			startActivity(intent);
			TestLocationListActivity.this.finish();
			return true;
		case R.id.options_test_by_location_tests:
			return true;
		case R.id.options_exit:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (loadDialog != null && loadDialog.isShowing()) {
			loadDialog.dismiss();
		}
	}
}
