package com.daokin.biochecker.view;

import com.daokin.biochecker.ui.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ProgressBar;

public class TextProgressBar extends ProgressBar {
	private String text;
	private Paint mPaint;

	public TextProgressBar(Context context) {
		super(context);
		initText();
	}
	
	public void setText(String text) {
		this.text = text;
	}

	public TextProgressBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initText();
		TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.daokin_progress);
		if (text == null || text.trim().length() == 0) {
			text = array.getString(R.styleable.daokin_progress_progress_text);
		}
		array.recycle();
	}

	public TextProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initText();
		TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.daokin_progress);
		if (text == null || text.trim().length() == 0) {
			text = array.getString(R.styleable.daokin_progress_progress_text);
		}
		array.recycle();
	}

	@Override
	public void setProgress(int progress) {
		if (progress > 0 && progress <= 100) {
			setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_green_color));
		} else if (progress > 100){
			setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_red_color));
		}
		super.setProgress(progress);
	}

	@Override
	protected synchronized void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		Rect rect = new Rect();
		if (text != null) {
			this.mPaint.getTextBounds(this.text, 0, this.text.length(), rect);
			int x = (getWidth() / 2) - rect.centerX();
			int y = (getHeight() / 2) - rect.centerY();
			canvas.drawText(this.text, x, y, this.mPaint);
		}
	}

	private void initText() {
		this.mPaint = new Paint();
		this.mPaint.setAntiAlias(true);
		this.mPaint.setColor(Color.BLACK);
		this.mPaint.setTextSize(36);
	}
}