package com.daokin.biochecker.protocol;

import com.daokin.biochecker.protocol.PacketDefinitions.MessageSize;
import com.daokin.biochecker.protocol.PacketDefinitions.MessageType;
import com.daokin.biochecker.util.ConvertUtil;

/**
 *
 */
public final class ReportMessage extends GenericMessage {
    private enum ReportFlags {
        analog_clip(0x1),
        digital_clip(0x2),
        impedance_oor(0x4),
        sensitivity_oor(0x8),
        fsum_oor(0x10),
        calibrated(0x20);

        private int value;

        ReportFlags(int val) {
            this.value = val;
        }

        public int getValue() {
            return this.value;
        }
    }

    private int timestamp;
    private int cycleNum;
    private byte sensorId;
    private float center_tone_impedance_magnitude;
    private float center_tone_impedance_angle;
    private float mr_magnitude;
    private float mr_angle;
    //private float kR;
    //private float kX;
    private int flags;
    private boolean analog_clip;
    private boolean digital_clip;

    public ReportMessage(GenericMessage msg) {
        this.seqNum = msg.seqNum;
        this.payload = msg.payload.clone();
    }

    public double getTimestamp() {
        return this.read_double(1);
    }

    public void setTimestamp(long timestamp) {
        this.write_double(1, timestamp);
    }

    public short getCyclenum() {
        return this.read_unit16(9);
    }

    public void setCycleNum(short cycleNum) {
        Byte[] intBytes = ConvertUtil.intToByte(cycleNum);
        this.payload[9] = intBytes[0];
        this.payload[10] = intBytes[1];
    }

    public byte getSensorId() {
        return payload[11];
    }

    public void setSensorId(byte sensorId) {
        this.payload[11] = sensorId;
    }

//    public float getFd_volts_uncompensated() {
//        return this.read_float(16);
//    }
//
//    public void setFd_volts_uncompensated(float fd_volts_uncompensated) {
//        this.write_float(16, fd_volts_uncompensated);
//    }
//
//    public float getFc_volts_uncompensated() {
//        return this.read_float(20);
//    }
//
//    public void setFc_volts_uncompensated(float fc_volts_uncompensated) {
//        this.write_float(20, fc_volts_uncompensated);
//    }
//
//    public float getFs_volts_uncompensated() {
//        return this.read_float(24);
//    }
//
//    public void setFs_volts_uncompensated(float fs_volts_uncompensated) {
//        this.write_float(24, fs_volts_uncompensated);
//    }
//
//    public float getFd_volts() {
//        return this.read_float(40);
//    }
//
//    public void setFd_volts(float fd_volts) {
//        this.write_float(40, fd_volts);
//    }
//
//    public float getFd_rads() {
//        return this.read_float(44);
//    }
//
//    public void setFd_rads(float fd_rads) {
//        this.write_float(44, fd_rads);
//    }
//
//    public float getFc_volts() {
//        return this.read_float(48);
//    }
//
//    public void setFc_volts(float fc_volts) {
//        this.write_float(48, fc_volts);
//    }
//
//    public float getFc_rads() {
//        return this.read_float(52);
//    }
//
//    public void setFc_rads(float fc_rads) {
//        this.write_float(52, fc_rads);
//    }
//
//    public float getFs_volts() {
//        return this.read_float(56);
//    }
//
//    public void setFs_volts(float fs_volts) {
//        this.write_float(56, fs_volts);
//    }
//
//    public float getFs_rads() {
//        return this.read_float(60);
//    }
//
//    public void setFs_rads(float fs_rads) {
//        this.write_float(60, fs_rads);
//    }
//
//    public void setCodec_temp(float codec_temp) {
//        this.write_float(64, codec_temp);
//    }
//
//    public float getCodec_temp() {
//        return this.read_float(64);
//    }
//
//    public void setCoil_amp_temp(float coil_amp_temp) {
//        this.write_float(68, coil_amp_temp);
//    }
//
//    public float getCoil_amp_temp() {
//        return this.read_float(68);
//    }
//
//    public float getFd_R() {
//        return this.read_float(72);
//    }
//
//    public void setFd_R(float fd_R) {
//        this.write_float(72, fd_R);
//    }
//
//    public float getFd_X() {
//        return this.read_float(76);
//    }
//
//    public void setFd_X(float fd_X) {
//        this.write_float(76, fd_X);
//    }
//
//    public float getFc_R() {
//        return this.read_float(80);
//    }
//
//    public void setFc_R(float fc_R) {
//        this.write_float(80, fc_R);
//    }
//
//    public float getFc_X() {
//        return this.read_float(84);
//    }
//
//    public void setFc_X(float fc_X) {
//        this.write_float(84, fc_X);
//    }
//
//    public float getFs_R() {
//        return this.read_float(88);
//    }
//
//    public void setFs_R(float fs_R) {
//        this.write_float(88, fs_R);
//    }
//
//    public float getFs_X() {
//        return this.read_float(72);
//    }
//
//    public void setFs_X(float fs_X) {
//        this.write_float(72, fs_X);
//    }

    public float getMRMagnitude() {
        return this.read_float(96);
    }

    public void setMRMagnitude(float mr_magnitude) {
        this.write_float(96, mr_magnitude);
    }

    public float getMRAngle() {
        return this.read_float(100);
    }

    public void setMRAngle(float mr_angle) {
        this.write_float(100, mr_angle);
    }

    public float getCenterToneImpedanceMagnitude() {
        return this.read_float(72);
    }

    public void setCenterToneImpedanceMagnitude(float value) {
        this.write_float(72, value);
    }

    public float getCenterToneImpedanceAngle() {
        return this.read_float(76);
    }

    public void setCenterToneImpedanceAngle(float value) {
        this.write_float(76, value);
    }

    public int getFlags() {
        return this.read_unit32(112);
    }

    public void setFlags(int flags) {
        this.write_unit32(112, flags);
    }

    public boolean isAnalog_clip() {
        return (this.flags & ReportFlags.analog_clip.value) == ReportFlags.analog_clip.value;
    }

    public void setAnalog_clip(boolean analog_clip) {
        this.analog_clip = analog_clip;
    }

    public boolean isDigital_clip() {
        return (this.flags & ReportFlags.digital_clip.value) == ReportFlags.digital_clip.value;
    }

    public void setDigital_clip(boolean digital_clip) {
        this.digital_clip = digital_clip;
    }

//    public ReportMessage(Byte[] vTag, long timestamp, short cycleNum, byte sensorId,
//                         float fd_volts_uncompensated, float fc_volts_uncompensated,
//                         float fs_volts_uncompensated, float fd_volts, float fd_rads,
//                         float fc_volts, float fc_rads, float fs_volts, float fs_rads,
//                         float codec_temp, float coil_amp_temp, float fd_R, float fd_X, float fc_R,
//                         float fc_X, float fs_R, float fs_X, float mr_magnitude,
//                         float mr_angle, float kR, float kX, int flags) {
//        payload = new Byte[MessageSize.ReportBytes.getValue()];
//        for (int i = 0; i < payload.length; i++) {
//            payload[i] = 0;
//        }
//        payload[0] = MessageType.Report.getValue();
//        this.vTag = vTag;
//        setTimestamp(timestamp);
//        setCycleNum(cycleNum);
//        setSensorId(sensorId);
////        setFd_volts_uncompensated(fd_volts_uncompensated);
////        setFc_volts_uncompensated(fc_volts_uncompensated);
////        setFs_volts_uncompensated(fs_volts_uncompensated);
////        setFd_volts(fd_volts);
////        setFd_rads(fd_rads);
////        setFc_volts(fc_volts);
////        setFc_rads(fc_rads);
////        setFs_volts(fs_volts);
////        setFs_rads(fs_rads);
////        setCodec_temp(codec_temp);
////        setCoil_amp_temp(coil_amp_temp);
////        setFd_R(fd_R);
////        setFd_X(fd_X);
////        setFc_R(fc_R);
////        setFc_X(fc_X);
////        setFs_R(fs_R);
//        setMRMagnitude(mr_magnitude);
//        setMRAngle(mr_angle);
////        setkR(kR);
////        setkX(kX);
//        setFlags(flags);
//        setMessageType(PacketDefinitions.MessageType.Report);
//        this.seqNum = generate_seqNum();
//    }

}
