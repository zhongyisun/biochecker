package com.daokin.biochecker.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ResponseModel;
import com.daokin.biochecker.model.SharedApplicationData;
import com.daokin.biochecker.model.User;
import com.daokin.biochecker.util.DBHelper;
import com.daokin.biochecker.util.RequestUtils;
import com.daokin.biochecker.util.StringUtils;
import com.daokin.biochecker.util.Tools;

public class LoginActivity extends Activity {
    private static final String TAG = "BioChecker";
    private TextView tv_signin = null;
    private EditText text_username = null;
    private EditText text_password = null;
    private TextView tv_forgetpassword = null;
    private Button login_btn = null;
    private Button offline_btn = null;
    private CheckBox cb_remenber = null;
    private SharedPreferences sp = null;
    private DBHelper dbHelper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        Log.d(TAG, "LoginActivity start");
        tv_signin = (TextView) findViewById(R.id.tv_signin);
        tv_forgetpassword = (TextView) findViewById(R.id.tv_forgetpassword);
        cb_remenber = (CheckBox) findViewById(R.id.cb_remenber);
        dbHelper =  new DBHelper(getApplicationContext());
        tv_signin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,
                        SignInActivity.class));
            }
        });

        tv_forgetpassword.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,
                        ForgetPasswordActivity.class));
            }
        });

        login_btn = (Button) findViewById(R.id.login_btn);
        login_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String userName = text_username.getText().toString();
                String password = text_password.getText().toString();
                if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(password)) {
                    Toast.makeText(LoginActivity.this,
                            "username and password can not be null!",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                new LoginTask().execute(userName, password);
            }
        });

        offline_btn = (Button) findViewById(R.id.offline_btn);
        offline_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = text_username.getText().toString();
                String password = text_password.getText().toString();
                if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(password)) {
                    Toast.makeText(LoginActivity.this,
                            "username and password can not be null!",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                offline_btn.setClickable(false);
                offline_btn.setText("Logining...");
                User user = dbHelper.queryUser(userName, Tools.string2MD5(password));
                if (user != null) {
                    Toast.makeText(LoginActivity.this, "Login Success",
                            Toast.LENGTH_LONG).show();
                    ModelContainer.i.setUser(user);
                    Editor editor = sp.edit();
                    if (cb_remenber.isChecked()) {
                        editor.putBoolean("REMENBER", true);
                        editor.putString("USERNAME", userName);
                        editor.putString("PASSWORD", password);
                    } else {
                        editor.putBoolean("REMENBER", false);
                    }
                    editor.commit();
                    SharedApplicationData.setLoginStatus(SharedApplicationData.OFFLINE);
                    Intent intent = new Intent(LoginActivity.this,
                            HomeActivity.class);
                    startActivity(intent);
                    LoginActivity.this.finish();
                } else {
                    Toast.makeText(LoginActivity.this,
                            "Login error: User not exist in the local device!",
                            Toast.LENGTH_LONG).show();
                }
                offline_btn.setClickable(true);
            }
        });

        text_username = (EditText) findViewById(R.id.text_username);
        text_password = (EditText) findViewById(R.id.text_password);

        sp = this.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if (sp.getBoolean("REMENBER", false)) {
            text_username.setText(sp.getString("USERNAME", ""));
            text_password.setText(sp.getString("PASSWORD", ""));
        }
    }

    private class LoginTask extends
            AsyncTask<String, Void, ResponseModel<User>> {
        private String userName;
        private String password;

        @Override
        protected ResponseModel<User> doInBackground(String... params) {
            ResponseModel<User> user = null;
            try {
                this.userName = params[0];
                this.password = params[1];
                user = RequestUtils.Login(params[0], params[1]);
            }
            catch (Exception ex) {
                Toast.makeText(LoginActivity.this, "Login error: " + ex.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
            return user;
        }

        @Override
        protected void onPreExecute() {
            login_btn.setClickable(false);
            login_btn.setText("Logining...");
        }

        @Override
        protected void onPostExecute(ResponseModel<User> result) {
            try {
                login_btn.setClickable(true);
                login_btn.setText("Login");
                if (result == null || result.getCode() == -1) {
                    Toast.makeText(LoginActivity.this,
                            "Login error: " + result.getMessage(),
                            Toast.LENGTH_LONG).show();
                    text_username.setFocusable(true);
                } else {
                    Toast.makeText(LoginActivity.this, "Login Success",
                            Toast.LENGTH_LONG).show();
                    ModelContainer.i.setUser(result.getData());
                    Editor editor = sp.edit();
                    if (cb_remenber.isChecked()) {
                        editor.putBoolean("REMENBER", true);
                        editor.putString("USERNAME", this.userName);
                        editor.putString("PASSWORD", this.password);
                    } else {
                        editor.putBoolean("REMENBER", false);
                    }
                    editor.commit();
                    SharedApplicationData.setLoginStatus(SharedApplicationData.LOGIN);
                    String md5pwd = Tools.string2MD5(password);
                    if (dbHelper.queryUser(userName, md5pwd) == null) {
                        User user = result.getData();
                        if (user != null) {
                            user.setPassword(md5pwd);
                            dbHelper.addUser(result.getData());
                        }
                    }
                    Intent intent = new Intent(LoginActivity.this,
                            HomeActivity.class);
                    startActivity(intent);
                    LoginActivity.this.finish();
                }
            }
            catch (Exception ex) {
                Toast.makeText(LoginActivity.this, "Login error: " + ex.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private long mExitTime;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                Toast.makeText(this, "Click again to exit!", Toast.LENGTH_SHORT).show();
                mExitTime = System.currentTimeMillis();
            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
