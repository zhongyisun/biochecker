package com.daokin.biochecker.model;

import java.util.List;

import com.daokin.biochecker.util.DateUtil;

/**
 * @author XXY
 */
public class ProcessedResult {

    private int id;
    private Test test;
    private TestDevice testDevice;
    private TestChip testChip;
    private TestSample testSample;
    private List<Labels> labels;
    private String processedResult;
    private String lastUpdatedTimestamp;
    private String processedTimestamp;
    private SharedLevel sharedLevel;
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Labels> getLabels() {
        return labels;
    }

    public void setLabels(List<Labels> labels) {
        this.labels = labels;
    }

    public String getProcessedResult() {
        return processedResult;
    }

    public void setProcessedResult(String processedResult) {
        this.processedResult = processedResult;
    }

    public String getLastUpdatedTimestamp() {
        return lastUpdatedTimestamp;
    }

    public void setLastUpdatedTimestamp(String lastUpdatedTimestamp) {
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
    }

    public String getProcessedTimestamp() {
        return processedTimestamp;
    }

    public void setProcessedTimestamp(String processedTimestamp) {
        this.processedTimestamp = processedTimestamp;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public TestDevice getTestDevice() {
        return testDevice;
    }

    public void setTestDevice(TestDevice testDevice) {
        this.testDevice = testDevice;
    }

    public TestChip getTestChip() {
        return testChip;
    }

    public void setTestChip(TestChip testChip) {
        this.testChip = testChip;
    }

    public TestSample getTestSample() {
        return testSample;
    }

    public void setTestSample(TestSample testSample) {
        this.testSample = testSample;
    }

    public SharedLevel getSharedLevel() {
        return sharedLevel;
    }

    public void setSharedLevel(SharedLevel sharedLevel) {
        this.sharedLevel = sharedLevel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSimpleProcessedTime() {
        return DateUtil.formatDate(DateUtil.getDataFromSpecialTimestamp(this.processedTimestamp));
    }

    public static class Test {

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        private int id;
        private String name;
    }

    public static class TestDevice {

        private int id;
        private String name;
        private String description;
        private String version;
        private String deviceId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }
    }

    public static class TestChip {
        @Override
        public String toString() {
            return "ChipId:" + chipId;
        }

        private int id;
        private String chipId;
        private String name;
        private String target;
        private String description;
        private List<AssayPrinting> assayPrintings;

        public List<AssayPrinting> getAssayPrintings() {
            return assayPrintings;
        }

        public void setAssayPrintings(List<AssayPrinting> assayPrintings) {
            this.assayPrintings = assayPrintings;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getChipId() {
            return chipId;
        }

        public void setChipId(String chipId) {
            this.chipId = chipId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTarget() {
            return target;
        }

        public void setTarget(String target) {
            this.target = target;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public static class AssayPrinting {

            private int id;
            private String name;
            private String printing;
            private Assay assay;
            private double max_value;
            private double min_value;


            public double getMax_value() {
                return max_value;
            }

            public void setMax_value(double max_value) {
                this.max_value = max_value;
            }

            public double getMin_value() {
                return min_value;
            }

            public void setMin_value(double min_value) {
                this.min_value = min_value;
            }

            public Assay getAssay() {
                return assay;
            }

            public void setAssay(Assay assay) {
                this.assay = assay;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrinting() {
                return printing;
            }

            public void setPrinting(String printing) {
                this.printing = printing;
            }

            public static class Assay {

                private int id;
                private String name;
                private String description;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }
            }
        }
    }

    public static class TestSample {

        private int id;
        private String clientSampleId;
        private SampleType sampleType;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getClientSampleId() {
            return clientSampleId;
        }

        public void setClientSampleId(String clientSampleId) {
            this.clientSampleId = clientSampleId;
        }

        public SampleType getSamType() {
            return sampleType;
        }

        public void setSamType(SampleType sampleType) {
            this.sampleType = sampleType;
        }

        public static class SampleType {

            private int id;
            private String name;
            private String description;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }
        }

    }

    public static class Labels {

        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class SharedLevel {

        private int id;
        private String name;
        private String description;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
