package com.daokin.biochecker.model;

public enum ChipDataTypeEnum {
	CONDITION(1),
	PRINT(2),
	CHARACTER(3),
	FLUORESCENT(4),
	GMR(5),
	PRINTALIGNED(6),
	TOXIN(7),
	WATER(8),
	OVARIANCANCER(9);
	
	private Integer value;
	public Integer getValue() {
		return this.value;
	}
	private ChipDataTypeEnum(Integer value) {
		this.value = value;
	}
}
