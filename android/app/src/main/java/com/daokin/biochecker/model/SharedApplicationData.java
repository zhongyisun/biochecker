package com.daokin.biochecker.model;

/**
 * Created by SamXCode on 2015/11/24.
 */
public class SharedApplicationData {

    public final static byte OFFLINE = 0;
    public final static byte LOGIN = 1;
    private static byte loginStatus = 0;

    public static void setLoginStatus(byte status) {
        loginStatus = status;
    }

    public static boolean isOffline() {
        return loginStatus == OFFLINE;
    }
}
