package com.daokin.biochecker.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.preference.DialogPreference;
import android.provider.Settings;
import android.util.Log;
import android.widget.TextView;

import com.daokin.biochecker.ui.HomeActivity;
import com.daokin.biochecker.ui.R;

import java.security.MessageDigest;
import java.util.List;

public class Tools {
    private static final String TAG = "BioChecker.Tools";
    public static boolean isNetworkConnected(Context context) {
        boolean flag = false;
        ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr == null) {
            flag = false;
        } else {
            NetworkInfo[] infos = connMgr.getAllNetworkInfo();
            if (infos != null) {
                for (NetworkInfo netInfo : infos) {
                    if (netInfo.getState() == State.CONNECTED) {
                        flag = true;
                        break;
                    }
                }
            }
        }
        return flag;
    }

    public static void checkNetwork(final Activity context) {
        if (!Tools.isNetworkConnected(context)) {
            TextView msg = new TextView(context);
            msg.setText("No avaliable network, please set");
            new AlertDialog.Builder(context).setIcon(R.drawable.not)
                    .setTitle("No avaliable network").setView(msg)
                    .setNegativeButton("setting", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            context.startActivity(new Intent(
                                    Settings.ACTION_WIRELESS_SETTINGS));
                            context.finish();
                        }
                    }).setPositiveButton("cancel", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    context.finish();
                }
            }).show();
        }
    }

    public static double getReferValue(double value1, double value2, double value3) {
        double maxValue = getMaxValue(value1, value2, value3);
        double referValue = 0.0d;
        String valueStr = Double.toString(maxValue);
        int dotIndex = valueStr.indexOf(".");
        String part1 = valueStr.substring(0, dotIndex);
        String part2 = valueStr.substring(dotIndex + 1, valueStr.length());
        if (dotIndex != -1) {
            for (int i = 0; i < part1.length(); i++) {
                if (part1.charAt(i) != '0') {
                    referValue = Double.parseDouble(buildSuffix(i) + (Integer.parseInt(part1.charAt(i) + "") + 1));
                    break;
                }
            }
            if (referValue != 0) {
                for (int i = 0; i < part2.length(); i++) {
                    if (part2.charAt(i) != '0') {
                        referValue = Double.parseDouble("0." + buildSuffix(i) + (Integer.parseInt(part2.charAt(i) + "") + 1));
                        break;
                    }
                }
            }
        }
        return referValue;
    }

    private static String buildSuffix(int num) {
        String result = "";
        for (int i = 0; i < num; i++) {
            result += "0";
        }
        return result;
    }

    public static double getMaxValue(double value1, double value2, double value3) {
        if (value1 >= value2) {
            if (value1 >= value3) {
                return value1;
            } else {
                return value3;
            }
        } else {
            if (value2 >= value3) {
                return value2;
            } else {
                return value3;
            }
        }
    }

    /*md5*/
    public static String string2MD5(String inStr) {
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
            return "";
        }
        char[] charArray = inStr.toCharArray();
        byte[] byteArray = new byte[charArray.length];

        for (int i = 0; i < charArray.length; i++)
            byteArray[i] = (byte) charArray[i];
        byte[] md5Bytes = md5.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++) {
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16)
                hexValue.append("0");
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }

    public static void showExitTips(final Activity activity) {
        AlertDialog dialog = new AlertDialog.Builder(activity).
                setMessage("Are you sure to exit?").
                setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(activity, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("exit", true);
                        activity.startActivity(intent);
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                }).create();
        dialog.show();
    }

    //Just to verify the qc failed sensor parts, must be even numbers
    public static boolean validateQCFailedSensorMark(String QCFailedSensor) {
        if (QCFailedSensor == null) {
            return true;
        }
        boolean validQCFailedMark = true;
        if (QCFailedSensor != null && QCFailedSensor.length() % 2 != 0) {
            validQCFailedMark = false;
        } else {
            try {
                for (int i = 0; i < QCFailedSensor.length(); i += 2) {
                    Byte.valueOf(QCFailedSensor.substring(i, i + 2), 16);
                }
            } catch (Exception ex) {
                Log.w(TAG, "unexpected QCFailed sensor mark:" + ex.getMessage());
                validQCFailedMark = false;
            }
        }
        return validQCFailedMark;
    }
}
