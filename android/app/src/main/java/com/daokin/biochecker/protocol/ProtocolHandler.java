package com.daokin.biochecker.protocol;

import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.daokin.biochecker.model.AssayData;
import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.protocol.PacketDefinitions.HeaderFlag;
import com.daokin.biochecker.protocol.PacketDefinitions.MessageType;
import com.daokin.biochecker.protocol.PacketDefinitions.SpecialByte;
import com.daokin.biochecker.util.CRC;
import com.daokin.biochecker.util.ConvertUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Timer;
import java.util.TreeMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author XXY
 */
public class ProtocolHandler {

    private enum CONNECTION_STATE { COMMS_CLOSED, SYN_PENDING, COMMS_OPEN };

    private static final String TAG = "BioChecker";

    private final static int MAX_RETRY = 10;
    private final static int RETRY_INTERVAL = 1000;
    private final static int ACK_THROTTLE_MILLISECONDS = 50;
    private BlockingQueue<GenericMessage> writeMessageQueue;
    private BlockingQueue<ReportMessage> reportMessageQueue;
    private ReportProcessingThread reportProcessingThread;
    private MessageSendingThread messageSendingThread;
    private SerialDataReadingThread serialDataReadingThread;
    private BluetoothSocket btSocket;
    private InputStream is;
    private OutputStream os;
    private Handler uiHandler;
    private Timer timer;
    private StartCommand startCommand = null;
    private DataOperation dataOperation;

    // for tracking checksum error
    private long total_CRC_errors = 0;
    private long total_packets = 0;
    private long total_bytes = 0;

    private Byte[] txVTag;
    private short startConfirmSeqNum = 0;
    private short stopConfirmSeqNum = 0;

    boolean screening = false; // true for screening reference sensors, false for running sample

    public Byte[] screenStartCommandPayload = null;
    public Byte[] sampleStartCommandPayload = null;

    public ProtocolHandler(BluetoothSocket btSocket, Handler uiHandler, boolean screening)
            throws IOException {
        this.btSocket = btSocket;
        this.uiHandler = uiHandler;
        this.screening = screening;
        is = btSocket.getInputStream();
        os = btSocket.getOutputStream();
        reportMessageQueue = new LinkedBlockingQueue<>();
        writeMessageQueue = new LinkedBlockingQueue<>();
        dataOperation = new DataOperation();

        reportProcessingThread = new ReportProcessingThread();
        messageSendingThread = new MessageSendingThread();
        serialDataReadingThread = new SerialDataReadingThread();
    }

    private Byte[] generateVTag() {
        Byte[] vTag = new Byte[4];
        Random random = new Random();
        int index = 0;
        String str = "**********Generated vTag: ";
        for (byte b : ByteBuffer.allocate(4).putInt(random.nextInt()).array()) {
            vTag[index++] = Byte.valueOf(b);
            str += String.format("0x%02X ", b);
        }
        Log.d(TAG, str);
        return vTag;
    }

    public void start(Context context) throws IOException {
        txVTag = generateVTag();
        startReportProcessingThread();
        startMessageSendingThread();
        startSerialDataReadingThread();
        int i = 0;
        while (!serialDataReadingThread.isStarted() && i++ < MAX_RETRY) {
            try {
                Thread.sleep(RETRY_INTERVAL);
            } catch (InterruptedException ex) {
                Log.d(TAG, ex.getMessage(), ex);
            }
        }
        if (serialDataReadingThread.isStarted()) {
            if (screening) {
                sendScreenStartCommand(context);
            }
            else {
                sendSampleStartCommand(context);
            }
            try {
                Thread.sleep(1000);
            }
            catch (Exception ex) {
                Log.e(TAG, ex.getMessage(), ex);
            }
        } else {
            throw new IOException("Bluetooth can not access");
        }
    }

    public void stopHandler() {
        try {
            if (timer != null) {
                timer.cancel();
            }
            if (messageSendingThread != null) {
                messageSendingThread.stopSender();
            }
            if (serialDataReadingThread != null) {
                serialDataReadingThread.stopReader();
            }
            if (reportProcessingThread != null) {
                reportProcessingThread.stopProcessing();
            }
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
            if (btSocket != null) {
                if (btSocket.isConnected()) {
                    btSocket.close();
                }
                btSocket = null;
            }
            if (dataOperation != null) {
                dataOperation = null;
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage(), ex);
        }
    }

    public DataOperation getDataOperation() {
        return dataOperation;
    }

    public StartCommand getStartCommand() {
        return startCommand;
    }

    public void sendStopCommand() {
        StopCommand cmd = new StopCommand(txVTag);
        stopConfirmSeqNum = cmd.seqNum;
        writeMessageQueue.offer(cmd);
        Log.d(TAG, "sendStopCommand. Sequence number: " + cmd.seqNum);
    }

    public void sendScreenStartCommand(Context context) {
        startCommand = generateScreenStartCommand(context);
        startConfirmSeqNum = startCommand.seqNum;
        screenStartCommandPayload = startCommand.payload.clone();
        writeMessageQueue.offer(startCommand);
        Log.d(TAG, "sendScreenStartCommand: " + startCommand.seqNum);
    }

    public void sendSampleStartCommand(Context context) {
        startCommand = generateSampleStartCommand(context);
        startConfirmSeqNum = startCommand.seqNum;
        sampleStartCommandPayload = startCommand.payload.clone();;
        writeMessageQueue.offer(startCommand);
        Log.d(TAG, "sendSampleStartCommand: " + startCommand.seqNum);
    }

    private void startReportProcessingThread() {
        if (!reportProcessingThread.isStarted()) {
            reportProcessingThread.start();
        }
    }

    private void startMessageSendingThread() {
        if (!messageSendingThread.isStarted()) {
            messageSendingThread.start();
        }
    }

    private void startSerialDataReadingThread() {
        if (!serialDataReadingThread.isStarted()) {
            serialDataReadingThread.start();
        }
    }

    private class SerialDataReadingThread extends Thread {

        private boolean isStarted = false;

        public boolean isStarted() {
            return isStarted;
        }

        public void stopReader() {isStarted = false; }

        @Override
        public void run() {
            isStarted = true;
            boolean startReplyReceived = false;
            String str = "";
            ByteArrayOutputStream inputData = new ByteArrayOutputStream(); // input data storage buffer
            boolean startFound= false;
            byte currByte = 0x00;
            byte[] byteBuffer = new byte[1024];

            // variables for RX connection state
            CONNECTION_STATE rxConnectionState = CONNECTION_STATE.COMMS_CLOSED;
            short lastRxSeqNum = 0;
            Byte[] rxVTag = new Byte[4];

            while (isStarted) {
                try {
                    int inByte = -1;
                    try {
                        inByte = is.read(byteBuffer);
                        if (inByte < 0) {
                            Log.d(TAG, "Socket reading error");
                            break;
                        }
//                            str = "<<<<<<<<<< " + inByte + " bytes read: ";
//                            for (int i = 0; i < inByte; i++) {
//                                str += String.format("0x%02X ", byteBuffer[i]);
//                            }
//                            Log.d(TAG, str);
                    }
                    catch (IOException ex) {
                        //Log.e(TAG, "Exception: " + ex.getMessage(), ex);
                        break;
                    }
                    for (int i = 0; i < inByte; i++) {
                        currByte = (byte) byteBuffer[i];
                        if (!startFound) {
                            if (currByte == SpecialByte.SOF.getValue()) {
                                startFound = true;
                            }
                            continue;
                        }

                        if (startFound) {
                            // if another start byte found, start the package over
                            if (currByte == SpecialByte.SOF.getValue()) {
                                Log.d(TAG, "Another start byte found, throw all bytes before it, if any");
                                inputData.reset();
                                continue;
                            }
                            // look for end byte
                            if (currByte == SpecialByte.EOF.getValue()) {
                                //Log.d(TAG, "End byte found");
                                startFound = false;
                            } else {
                                // no end byte, save it as a part of payload
                                inputData.write(currByte);
                                continue;
                            }
                        }

                        byte[] load = inputData.toByteArray();
//                            str = "##########load: ";
//                            for (byte b : load) {
//                                str += String.format("0x%02X ", b);
//                            }
//                            Log.d(TAG, str);

                                // reset inputData buffer for next packet
                                inputData.reset();

                        // load may have ESC bytes
                        boolean isEsc = false;
                        List<Byte> payLoadBuffer = new ArrayList<Byte>();
                        // Remove Esc bytes from payload and restore the escaped byte
                        for (byte b : load) {
                            if (isEsc == false) {
                                //If the current byte is esc, then set is esc and went through
                                if (b == SpecialByte.ESC.getValue()) {
                                    isEsc = true;
                                } else {
                                    payLoadBuffer.add(b);
                                }
                            } else {
                                byte xor = SpecialByte.ESC_XOR.getValue();
                                byte XOR = (byte) (b ^ xor);
                                payLoadBuffer.add(XOR);
                                isEsc = false;
                            }
                        }

                        // check packet length
                        if (payLoadBuffer.size() < 16) {
                            Log.d(TAG, "Insufficient data!!!Payload size: " + payLoadBuffer.size());
                            continue;
                        }
                        Byte[] receivedVTag = payLoadBuffer.subList(0, 4).toArray(new Byte[4]);
                        Byte[] receivedCRCBytes = payLoadBuffer.subList(payLoadBuffer.size() - 4,
                                payLoadBuffer.size()).toArray(new Byte[4]);
                        long receivedCRC = ConvertUtil.byte2Long(receivedCRCBytes, 0);
                        // remove CRC bytes
                        for (int k = 0; k < 4; k++) {
                            payLoadBuffer.remove(payLoadBuffer.size() - 1);
                        }
                        short[] result = new short[payLoadBuffer.size()];
                        for (int k = 0; k < payLoadBuffer.size(); k++) {
                            result[k] = (short) (0x000000FF & ((int) payLoadBuffer.get(k)));
                        }
                        long calculatedCRC = CRC.compute_crc(result);
                        Byte[] receivedSeqNumBytes = payLoadBuffer.subList(4, 6).toArray(new Byte[2]);
                        short receivedSeqNum = (short) ConvertUtil.byte2short(receivedSeqNumBytes, 0);
                        Byte[] headerFlagArray = payLoadBuffer.subList(6, 7).toArray(new Byte[1]);
                        byte headerFlag = headerFlagArray[0];
                        // zs v2 start from byte index 11
                        int startIndex = 11; // seqNum  + reserved
                        payLoadBuffer = payLoadBuffer.subList(startIndex, payLoadBuffer.size());
                        Byte[] payload = payLoadBuffer.toArray(new Byte[payLoadBuffer.size()]);

                        total_packets++;
                        total_bytes += payLoadBuffer.size();

                        if (calculatedCRC != receivedCRC) {
                            // for debugging purpose only, display in progress window
//                              total_CRC_errors++;
//                            Message msg = new Message();
//                            msg.obj = "Checksum error:"
//                                    + "\r\n\r\nSequence number: " + receivedSeqNum
//                                    + "\r\nPayload bytes: " + payLoadBuffer.size()
//                                    + "\r\n\r\ntotal_CRC_errors: " + total_CRC_errors
//                                    + "\r\ntotal_packets: " + total_packets
//                                    + "\r\ntotal_bytes: " + total_bytes;
//                            uiHandler.sendMessage(msg);
                            continue;
                        }

                        if ((headerFlag & (byte)HeaderFlag.Ack.getValue()) > 0) {
                            if (!Arrays.equals(receivedVTag, txVTag)) {
                                continue; // not the VTag we're looking for; discard
                            }
                            Log.d(TAG, "Ack received");
                            if (receivedSeqNum == startConfirmSeqNum) {
                                Log.d(TAG, "Start command ack received, Sequence number: " + (long)receivedSeqNum);
                                startCommand = null;
                                Message msg = new Message();
                                msg.obj = "Start succeeded";
                                uiHandler.sendMessage(msg);
                                continue; // not a message; we're done with this loop iteration.
                            } else if (receivedSeqNum == stopConfirmSeqNum) {
                                Log.d(TAG, "Stop command ack received, Sequence number: " + (long)receivedSeqNum);
                                messageSendingThread.stopSender();
                                // Stop acknowledged; I guess we're done here!
                                break;
                            }
                        }

                        if ((headerFlag & (byte)HeaderFlag.Syn.getValue()) > 0) {
                            if (   rxConnectionState != CONNECTION_STATE.COMMS_CLOSED
                                && Arrays.equals(receivedVTag, rxVTag)) {
                                short nextRxSeqNum = (short)(lastRxSeqNum + 1);
                                if (receivedSeqNum != nextRxSeqNum) {
                                    rxConnectionState = CONNECTION_STATE.SYN_PENDING;
                                    writeMessageQueue.put(new Acknowledgement(rxVTag, lastRxSeqNum));
                                    continue; // suppress duplicate to application layer
                                } else {
                                    rxConnectionState = CONNECTION_STATE.COMMS_OPEN;
                                    lastRxSeqNum = receivedSeqNum;
                                    writeMessageQueue.put(new Acknowledgement(rxVTag, lastRxSeqNum));
                                }
                            } else {
                                rxConnectionState = CONNECTION_STATE.COMMS_OPEN;
                                rxVTag = receivedVTag.clone();
                                lastRxSeqNum = receivedSeqNum;
                                writeMessageQueue.put(new Acknowledgement(rxVTag, lastRxSeqNum));
                            }
                        } else if (   rxConnectionState == CONNECTION_STATE.SYN_PENDING
                                   && Arrays.equals(receivedVTag, rxVTag)) {
                            short nextRxSeqNum = (short)(lastRxSeqNum + 1);
                            if (receivedSeqNum != nextRxSeqNum) {
                                writeMessageQueue.put(new Acknowledgement(rxVTag, lastRxSeqNum));
                                continue; // suppress duplicate to application layer
                            } else {
                                rxConnectionState = CONNECTION_STATE.COMMS_OPEN;
                                lastRxSeqNum = receivedSeqNum;
                                writeMessageQueue.put(new Acknowledgement(rxVTag, lastRxSeqNum));
                            }
                        } else if (rxConnectionState == CONNECTION_STATE.COMMS_CLOSED) {
                            continue; // rx connection closed; discard message
                        } else if (!Arrays.equals(receivedVTag, rxVTag)) {
                            continue; // verification tag doesn't match; discard
                        } else {
                            short nextRxSeqNum = (short)(lastRxSeqNum + 1);
                            if (receivedSeqNum != nextRxSeqNum) {
                                writeMessageQueue.put(new Acknowledgement(rxVTag, lastRxSeqNum));
                                continue; // suppress duplicate to application layer
                            } else {
                                lastRxSeqNum = receivedSeqNum;
                                writeMessageQueue.put(new Acknowledgement(rxVTag, lastRxSeqNum));
                            }
                        }

                        GenericMessage genericMessage = new GenericMessage(receivedSeqNum, payload);
                        MessageType messageType = genericMessage.getMessageType();
                        switch (messageType) {
                            case StartReply:
                                Log.d(TAG, "StartReply");
                                startReplyReceived = true;
                                break;
                            case Report:
                                if (startReplyReceived) {
                                    ReportMessage report = new ReportMessage(genericMessage);
                                    reportMessageQueue.put(report);
                                }
                                break;
                            case Status:
                                StatusMessage statusMessage;
                                statusMessage = new StatusMessage(genericMessage);
                                Log.d(TAG, "Status: " + statusMessage.getStatusCode());
                                if (statusMessage.getStatusCode()
                                        == PacketDefinitions.StatusCode.NO_CHIP) {
                                    Message msg1 = new Message();
                                    msg1.obj = "NoChip";
                                    uiHandler.sendMessage(msg1);
                                } else {
                                    Message msg1 = new Message();
                                    msg1.obj = "ChipSuccess";
                                    uiHandler.sendMessage(msg1);
                                }
                                break;
                            case HighWater:
                                HighWaterMessage highWaterMessage = new HighWaterMessage(genericMessage);
                                Log.d(TAG, "HighWater");
                                float SR, average, highWater;
                                SR = highWaterMessage.getMicroCycle() / highWaterMessage.getPeriod();
                                average = highWaterMessage.getAverage() / highWaterMessage.getPeriod();
                                highWater = highWaterMessage.getHighWater() / highWaterMessage.getPeriod();
                                Log.d(TAG, String.format("Sample rate: %.3fHz", SR));
                                Log.d(TAG, String.format("Real-time CPU consumption: average = %.3f, max = %.3f", average, highWater));
                                break;
                            case Debug:
                                DebugMessage debugMessage = new DebugMessage(genericMessage);
                                Log.d(TAG, "Debug: " + debugMessage.getText());
                                break;
                            default:
                                Log.d(TAG, "Unknown message type: " + messageType);
                                break;
                        }
                    }
                } catch (Exception ex) {
                    Log.e(TAG, ex.getMessage(), ex);
                    sendErrorMsg(2);
                    break;
                }
            }
            isStarted = false;
            Log.d(TAG, "Exit SerialDataReadingThread");
        }
    }

    private class MessageSendingThread extends Thread {

        private volatile boolean messageSendingThreadStarted = false;

        public void stopSender() { // stopHandler sender and clear data
            messageSendingThreadStarted = false;
            writeMessageQueue.clear();
        }

        public boolean isStarted() {
            return messageSendingThreadStarted;
        }

        @Override
        public void start() {
            writeMessageQueue.clear();
            messageSendingThreadStarted = true;
            super.start();
        }
        long lastMillis = 0;

        @Override
        public void run() {
            try {
                while (messageSendingThreadStarted) {
                    if (!writeMessageQueue.isEmpty()) {
                        GenericMessage message = writeMessageQueue.take();
                        long currentMillis = System.currentTimeMillis();
                        if ((currentMillis - lastMillis) >= ACK_THROTTLE_MILLISECONDS) {
                            lastMillis = currentMillis;
                        }
                        else {
                            Thread.sleep(ACK_THROTTLE_MILLISECONDS - (currentMillis - lastMillis));
                            lastMillis = System.currentTimeMillis();
                        }
                        writePacket(message);
                        if (message.getMessageType() == MessageType.Start) {
                            Thread.sleep(1000);
                        }
                    }
                }
            }
            catch (Exception ex) {
                Log.e(TAG, ex.getMessage(), ex);
                sendErrorMsg(3);
            }
            Log.d(TAG, "Exit MessageSendingThread");
        }
    }

    private class ReportProcessingThread extends Thread {

        private boolean reportProcessingThreadStart = false;

        public void stopProcessing() {
            reportProcessingThreadStart = false;
            writeMessageQueue.clear();
        }

        public boolean isStarted() {
            return reportProcessingThreadStart;
        }

        @Override
        public void start() {
            reportMessageQueue.clear();
            reportProcessingThreadStart = true;
            super.start();
        }

        @Override
        public void run() {
            try {
                int lastElapsedTime = -1;
                while (reportProcessingThreadStart) {
                    ReportMessage reportMessage = reportMessageQueue.take();
                    Object[] data = new Object[8];
                    data[0] = reportMessage.getSensorId();
                    data[1] = reportMessage.getMRMagnitude();
                    data[2] = reportMessage.getMRAngle();
                    data[3] = reportMessage.getTimestamp();
                    data[4] = reportMessage.getCyclenum();
                    data[5] = reportMessage.getFlags();
                    data[6] = reportMessage.getCenterToneImpedanceMagnitude();
                    data[7] = reportMessage.getCenterToneImpedanceAngle();
                    dataOperation.operate(data);
                    System.out.println("Report message: " + String.format("0x%02X", data[0])
                            + ", " + String.format("%.3f", data[3]) + ", " + data[4] + ", "
                            + String.format("%.3f", data[1]) + ", " + String.format("%.3f", data[6])
                            + ", " + data[5] + ", " + reportMessage.seqNum);
                    int elapsedTime = (int)Math.round(reportMessage.getTimestamp());
                    if (elapsedTime != lastElapsedTime) {
                        lastElapsedTime = elapsedTime;
                        Message msg1 = new Message();
                        msg1.obj = "Elapsed time: " + elapsedTime + " seconds";
                        uiHandler.sendMessage(msg1);
                    }
                }
            }
            catch (Exception ex) {
                Log.e(TAG, ex.getMessage(), ex);
            }
        }
    }

    private void sendErrorMsg(int errorCode) {
        Message msg = new Message();
        msg.obj = "Error:" + errorCode;
        Log.e(TAG, msg.obj.toString());
        uiHandler.sendMessage(msg);
        stopHandler();
    }

    private long addByte(List<Byte> bufferToWrite, byte b, long crc) {
        byte data = b;
        if (data == PacketDefinitions.SpecialByte.EOF.getValue()
                || data == PacketDefinitions.SpecialByte.ESC.getValue()
                || data == PacketDefinitions.SpecialByte.SOF.getValue()) {
            bufferToWrite.add(PacketDefinitions.SpecialByte.ESC.getValue());
            data ^= PacketDefinitions.SpecialByte.ESC_XOR.getValue();
            bufferToWrite.add(data);
        } else {
            bufferToWrite.add(data);
        }
        short unsignedByte = (short) (0x000000FF & ((int) b));
        return CRC.compute_crc_next(unsignedByte, crc);
    }

    private byte[] buildWriteBytes2(GenericMessage packetToWrite) {
        long crc = CRC.CRC_INIT_VAL;
        List<Byte> bufferToWrite = new ArrayList();
        bufferToWrite.add(PacketDefinitions.SpecialByte.SOF.getValue());
        Byte[] vTagBytes = packetToWrite.vTag;
        for (int i = 0; i < 4; ++i) {
            crc = addByte(bufferToWrite, vTagBytes[i], crc);
        }
        Byte[] seqNumBytes = ConvertUtil.shortToByte(packetToWrite.seqNum);
        for (int i = 0; i < 2; ++i) {
            crc = addByte(bufferToWrite, seqNumBytes[i], crc);
        }
        crc = addByte(bufferToWrite, packetToWrite.getHeaderFlagVal(), crc);
        // Set vmajor
        crc = addByte(bufferToWrite, (byte) 0x02, crc);
        // Set reserved
        crc = addByte(bufferToWrite, (byte) 0x00, crc);
        crc = addByte(bufferToWrite, (byte) 0x00, crc);
        if (packetToWrite.getHeaderFlag() == HeaderFlag.Ack) {

        } else if (packetToWrite.getHeaderFlag() == HeaderFlag.Syn) {
            crc = addByte(bufferToWrite, (byte) 0x00, crc);
        }
        if (packetToWrite.payload != null) {
            for (byte b : packetToWrite.payload) {
                crc = addByte(bufferToWrite, b, crc);
            }
        }

        // Set computed crc
        Byte[] crcBytes = ConvertUtil.intToByte(crc);
        for (int i = 0; i < 4; ++i) {
            crc = addByte(bufferToWrite, crcBytes[i], crc);
        }
        bufferToWrite.add(PacketDefinitions.SpecialByte.EOF.getValue());
        Byte[] finalPacketToWrite = bufferToWrite.toArray(new Byte[bufferToWrite.size()]);
        byte[] finalBytesToWrite = new byte[finalPacketToWrite.length];
        for (int i = 0; i < finalPacketToWrite.length; i++) {
            finalBytesToWrite[i] = finalPacketToWrite[i];
        }
        return finalBytesToWrite;
    }

    private synchronized void writePacket(GenericMessage packetToWrite) {
        byte[] finalBytesToWrite = buildWriteBytes2(packetToWrite);
//        String str = ">>>>>>>>>> " + finalBytesToWrite.length + " bytes to write: ";
//        for (int i = 0; i < finalBytesToWrite.length; i++) {
//            str += String.format("0x%02X ", finalBytesToWrite[i]);
//        }
//        Log.d(TAG, str);
        try {
            if (os != null) {
                os.write(finalBytesToWrite);
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage(), ex);
        }
    }

    private StartCommand generateScreenStartCommand(Context context) {
        Log.d(TAG, "Enter generateScreenStartCommand");
        InputStream in;
        try {
            int mux2SensorAnalyte = 0;
            List<String> mux2SensorsInUse = new ArrayList<String>();
            in = context.getAssets().open("config.properties");
            Properties py = new Properties();
            py.load(in);
            AssayData assayData =  ModelContainer.i.getAssayData();
            List<AssayData.Assay> assays = assayData.getAssays();
            for (AssayData.Assay assay : assays) {
                int analyte = assay.getAnalyte();
                String name = assay.getName();
                Log.d(TAG, "Name: " + name + ", Analyte: " + analyte);
                if (name.equals("BSA")) {
                    mux2SensorAnalyte = analyte;
                    Log.d(TAG, "Mux2 sensor analyte: " + mux2SensorAnalyte);
                }
            }
            Map<String, AssayData.Sensor> sensors = assayData.getSensors();
            for (Map.Entry<String, AssayData.Sensor> entry : sensors.entrySet()) {
                String label = entry.getKey();
                AssayData.Sensor sensor = entry.getValue();
                if (sensor.getAnalyte() == mux2SensorAnalyte && sensor.getType() == AssayData.NEGATIVE_TYPE) {
                    Log.d(TAG, "Add mux2 sensor: " + label);
                    mux2SensorsInUse.add(label);
                }
            }
            List<Byte> qcFailedSensors = ModelContainer.i.getQcFailedSensors();
            List<String> labelInUseList = new ArrayList<String>();
            List<Byte> muxInUseList = new ArrayList<Byte>();
            List<AssayData.Sensor> referenceSensorList = ModelContainer.i.getReferenceSensorList();
            for (AssayData.Sensor sensor : referenceSensorList) {
                byte b = Byte.parseByte(sensor.mux1.replace("0x", ""), 16);
                if (!qcFailedSensors.contains(b)) {
                    labelInUseList.add(sensor.label);
                    muxInUseList.add(b);
                }
            }
            dataOperation.setLabelInUse(labelInUseList);
            dataOperation.setMuxInUse(muxInUseList);
            String [] labelsInUse = labelInUseList.toArray(new String[labelInUseList.size()]);
            Log.d(TAG, "Labels in use:" + Arrays.toString(labelsInUse));
            Byte[] muxesInUse = muxInUseList.toArray(new Byte[muxInUseList.size()]);
            String str = "Mux addresses in use: ";
            for (int i = 0; i < muxInUseList.size(); i++) {
                str += String.format("0x%02X ", muxesInUse[i]);
            }
            Log.d(TAG, str);
            AssayData aData = ModelContainer.i.getAssayData();
            AssayData.Measurement measurement = aData.getMeasurement();
            float bridge_amp = measurement.getBridge_amp() / 1000;
            float bridge_hz = measurement.getBridge_hz();
            float coil_amp = measurement.getCoil_amp();
            float coil_hz = measurement.getCoil_hz();
            float coil_dc = measurement.getCoil_dc();
            float meas_period = measurement.getMeas_period();
            byte digital_gain = measurement.getDigital_gain();
            return new StartCommand(txVTag, muxesInUse, bridge_amp, coil_amp, bridge_hz,
                    coil_dc, coil_hz, meas_period, digital_gain, (byte)0xFF, new Byte[0]);
        } catch (IOException e) {
            Log.d(TAG, "There are errors in the properties", e);
        }
        finally {
            Log.d(TAG, "Leave generateScreenStartCommand");
        }
        return null;
    }

    private StartCommand generateSampleStartCommand(Context context) {
        Log.d(TAG, "Enter generateSampleStartCommand");
        try {
            String electricalReference = dataOperation.getElectricalReferenceSensor();
            byte mux2 = (byte)0xFF;
            Map<String, Byte> sensorMux2Map = ModelContainer.i.getLabelMux2Map();
            if (sensorMux2Map.containsKey(electricalReference)) {
                mux2 = sensorMux2Map.get(electricalReference);
            }
            Log.d(TAG, "Electrical reference sensor: " + electricalReference + ", mux2 address: " + String.format("0x%02X", mux2));
            List<Byte> qcFailedSensors = ModelContainer.i.getQcFailedSensors();
            List<String> labelInUseList = new ArrayList<String>();
            List<Byte> muxInUseList = new ArrayList<Byte>();
            Map<String, Byte> sensorMux1Map = ModelContainer.i.getLabelMux1Map();
            for (Map.Entry<String, Byte> entry : sensorMux1Map.entrySet()) {
                String label = entry.getKey();
                byte b = entry.getValue();
                if (!electricalReference.equals(label) && !qcFailedSensors.contains(b)) {
                    Log.d(TAG, "Add to sensor list: " + label + String.format(", %02X ", b));
                    labelInUseList.add(label);
                    muxInUseList.add(b);
                } else {
                    if (qcFailedSensors.contains(b)) {
                        Log.d(TAG, "Remove QC failed sensor. Sensor: " + label);
                        dataOperation.removeQCFailedSensor(b);
                    }
                }
            }
            dataOperation.setLabelInUse(labelInUseList);
            dataOperation.setMuxInUse(muxInUseList);
            Map<Byte, String> muxLabelMap = new TreeMap<Byte, String>();
            Map<String, Byte> labelMuxMap = new TreeMap<String, Byte>();
            for (int i = 0; i < labelInUseList.size(); i++) {
                String label = labelInUseList.get(i);
                Byte mux = muxInUseList.get(i);
                muxLabelMap.put(mux, label);
                labelMuxMap.put(label, mux);
            }
            dataOperation.setLabelMuxMap(labelMuxMap);
            dataOperation.setMuxLabelMap(muxLabelMap);
            String [] labelsInUse = labelInUseList.toArray(new String[labelInUseList.size()]);
            Log.d(TAG, "Labels in use:" + Arrays.toString(labelsInUse));
            Byte[] muxesInUse = muxInUseList.toArray(new Byte[muxInUseList.size()]);
            String str = "Mux addresses in use: ";
            for (int i = 0; i < muxInUseList.size(); i++) {
                str += String.format("0x%02X ", muxesInUse[i]);
            }
            Log.d(TAG, str);
            AssayData aData = ModelContainer.i.getAssayData();
            List<AssayData.Control> controls = aData.getValidControls();
            List<Byte> controlByteList = new ArrayList<Byte>();
            float nmps = 0;
            byte[] floatBytes;
            floatBytes = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(controls.size()).array();
            copyBytes(controlByteList, floatBytes);
            if (controls.size() > 0) {
//                float nmps = 0;
                for (AssayData.Control control : controls) {
                    nmps += control.getDuration();
                    floatBytes = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(control.getFrequency()).array();
                    copyBytes(controlByteList, floatBytes);
                    floatBytes = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(control.getDuration()).array();
                    copyBytes(controlByteList, floatBytes);
                    floatBytes = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) control.getAddress()).array();
                    copyBytes(controlByteList, floatBytes);
                }
            }
            Byte[] controlBytes = controlByteList.toArray(new Byte[controlByteList.size()]);
            str = "Control bytes: " + controlBytes.length + ", ";
            for (byte b : controlBytes) {
                str += String.format("0x%02X ", b);
            }
            Log.d(TAG, str);
            AssayData.Measurement measurement = aData.getMeasurement();
            float bridge_amp = measurement.getBridge_amp() / 1000; //in seconds
            float bridge_hz = measurement.getBridge_hz();
            float coil_amp = measurement.getCoil_amp();
            float coil_hz = measurement.getCoil_hz();
            float coil_dc = measurement.getCoil_dc();
            float meas_period = measurement.getMeas_period();
            byte digital_gain = measurement.getDigital_gain();
            // End of zs
            return new StartCommand(txVTag, muxesInUse, bridge_amp, coil_amp, bridge_hz,
                    coil_dc, coil_hz, meas_period, digital_gain, mux2, controlBytes);
        } catch (Exception e) {
            Log.d(TAG, "There are errors in the properties", e);
        }
        finally {
            Log.d(TAG, "Leave generateSampleStartCommand");
        }
        return null;
    }

    private void copyBytes(List<Byte> controlByteList, byte[] bytes) {
        for (byte b : bytes) {
            controlByteList.add(b);
        }
    }

    private void copyBytesToList(byte[] bytes, List<Byte> byteList, int startIndex) {
        for (int i = 0; i < bytes.length; i++) {
            byteList.set(startIndex++, bytes[i]);
        }
    }
}
