package com.daokin.biochecker.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class ForgetPasswordActivity extends Activity {
	private Button back_Button = null;
	private Button send_btn = null;
	
	@Override 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forget_password_layout);
		back_Button = (Button) findViewById(R.id.cancel_btn);
		back_Button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ForgetPasswordActivity.this.finish();
			}
		});
		send_btn = (Button) findViewById(R.id.send_btn);
		send_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(ForgetPasswordActivity.this, "Modify password url has send to your email", Toast.LENGTH_LONG).show();
			}
		});
	}
}
