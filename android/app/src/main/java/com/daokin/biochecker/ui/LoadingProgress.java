package com.daokin.biochecker.ui;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

public class LoadingProgress extends Dialog {
    
    
    public LoadingProgress(Context context, int theme){
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_dialog_layout);
    }
}