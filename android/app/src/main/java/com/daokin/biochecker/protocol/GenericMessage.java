package com.daokin.biochecker.protocol;

import com.daokin.biochecker.protocol.PacketDefinitions.MessageType;
import com.daokin.biochecker.protocol.PacketDefinitions.SpecialByte;
import com.daokin.biochecker.protocol.PacketDefinitions.HeaderFlag;
import com.daokin.biochecker.util.ConvertUtil;

import java.util.Arrays;

import static com.daokin.biochecker.util.ConvertUtil.floatToBytes1;

public class GenericMessage {

    public boolean reconnectOneShot = false;//only allow one reconnect attempt per message
    public int tx_count = 0;
    private static short seqNumGen = 0;

    protected static short generate_seqNum() {
        if (seqNumGen > Short.MAX_VALUE) {
            seqNumGen = 0;
        }
        seqNumGen += 1;
        return seqNumGen;
    }

    public Byte[] vTag;
    public short seqNum;
    public byte headerFlag = 0x00;
    public byte messageType = MessageType.Unknown.getValue();; // msg code
    public byte vMajor = 0x02;

    public Byte[] payload;

    public MessageType getMessageType() {
        return MessageType.getMessageType((byte) (payload[0] & ~SpecialByte.ACK_FLAG.getValue()));
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType.getValue();
        payload[0] = messageType.getValue();
    }

    public byte getHeaderFlagVal() {
        return headerFlag;
    }

    public HeaderFlag getHeaderFlag() {
        return HeaderFlag.getHeaderFlag(headerFlag);
    }

    public void setHeaderFlag(byte headerFlag) {
        this.headerFlag = headerFlag;
    }

    public Byte[] getVTag() {
        return vTag;
    }

    public void setVTag(Byte[] vTag) {
        this.vTag = vTag;
    }

    public short getSeqnum() {
        return seqNum;
    }

    public void setSeqnum(short seqNum) {
        this.seqNum = seqNum;
    }

    /**
     * Check if is acknoledge
     *
     * @return
     */
    public boolean isAck() {
        return (this.payload[0] & SpecialByte.ACK_FLAG.getValue()) != 0;
    }

    public void setIsAck(boolean inputIsAck) {
        if (inputIsAck) {
            this.payload[0] = (byte) (this.getMessageType().getValue() | SpecialByte.ACK_FLAG.getValue());
        } else {
            this.payload[0] = (byte) (this.getMessageType().getValue() & ~SpecialByte.ACK_FLAG.getValue());
        }
    }

    protected void write_float(int pos, float val) {
        Byte[] intData = floatToBytes1(val);
        System.arraycopy(intData, 0, payload, pos, 4);
    }

    protected float read_float(int pos) {
        return ConvertUtil.byte2float(this.payload, pos);
    }

    protected void write_unit16(int pos, int val) {
        Byte[] intData = ConvertUtil.intToByte(val);
        System.arraycopy(intData, 0, payload, pos, 2);
    }

    protected void write_unit32(int pos, int val) {
        Byte[] intData = ConvertUtil.intToByte(val);
        System.arraycopy(intData, 0, payload, pos, 4);
    }

    protected double read_double(int pos)  {
        return ConvertUtil.bytesToDouble(payload, pos);
    }

    public void write_double(int pos, double val) {
        Byte[] bytes = ConvertUtil.doubleToBytes(val);
        System.out.println(Arrays.toString(bytes));
        try {
            System.arraycopy(bytes, 0, payload, pos, 8);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    protected short read_unit16(int pos) {
        return (short) ConvertUtil.byte2short(payload, pos);
    }

    protected int read_unit32(int pos) {
        return ConvertUtil.byte2Int(payload, pos);
    }

    public GenericMessage(short seqNum, Byte[] payload) {
        this.seqNum = seqNum;
        this.payload = payload.clone();
    }

    public GenericMessage(Byte[] vTag, short seqNum, byte headerFlag, byte messageType) {
        this.vTag = vTag.clone();
        this.seqNum = seqNum;
        this.vMajor = 0x02;
        this.headerFlag = headerFlag;
        this.messageType = messageType;
    }

    public GenericMessage() {
    }

}
