package com.daokin.biochecker.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by SamXCode on 2016/1/7.
 */
public class GMRData {
    private String measurement_period;
    private String field_dc_offset;
    private String field_frequency;
    private String field_amplitude;
    private String wheatstone_frequency;
    private String wheatstone_amplitude;

    private List<String> data_tags = Arrays.asList("Time Stamp", "Cycle Number", "mr_magnitude", "mr_angle", "Flags");
    private SortedMap<String, SensorData> data = new TreeMap<String, SensorData>();

    public SensorData getData(String key) {
        return data.get(key);
    }
    public void setData(String key, SensorData data) {
        this.data.put(key, data);
    }

    public List<String> getData_tags() {
        return data_tags;
    }

    public String getMeasurement_period() {
        return measurement_period;
    }

    public void setMeasurement_period(String measurement_period) {
        this.measurement_period = measurement_period;
    }

    public String getField_dc_offset() {
        return field_dc_offset;
    }

    public void setField_dc_offset(String field_dc_offset) {
        this.field_dc_offset = field_dc_offset;
    }

    public String getField_frequency() {
        return field_frequency;
    }

    public void setField_frequency(String field_frequency) {
        this.field_frequency = field_frequency;
    }

    public String getField_amplitude() {
        return field_amplitude;
    }

    public void setField_amplitude(String field_amplitude) {
        this.field_amplitude = field_amplitude;
    }

    public String getWheatstone_frequency() {
        return wheatstone_frequency;
    }

    public void setWheatstone_frequency(String wheatstone_frequency) {
        this.wheatstone_frequency = wheatstone_frequency;
    }

    public String getWheatstone_amplitude() {
        return wheatstone_amplitude;
    }

    public void setWheatstone_amplitude(String wheatstone_amplitude) {
        this.wheatstone_amplitude = wheatstone_amplitude;
    }

    public Map<String, SensorData> getData() {
        return data;
    }




}
