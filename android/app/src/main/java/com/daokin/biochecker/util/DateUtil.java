package com.daokin.biochecker.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
	private static SimpleDateFormat sdf = new SimpleDateFormat("EEEE MM/dd/yy - K:mma", Locale.US);
	private static SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
	private static SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

	public static String formatDate() {
		return sdf3.format(Calendar.getInstance().getTime());
	}
	
	public static String formatDate(Date date) {
		if (date == null) return null;
		return sdf.format(date);
	}
	
	public static Date getDataFromSpecialTimestamp(String str) {
		try {
			return sdf2.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}
