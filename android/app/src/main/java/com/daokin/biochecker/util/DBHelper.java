package com.daokin.biochecker.util;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.daokin.biochecker.model.AssayData;
import com.daokin.biochecker.model.ChipDataTypeEnum;
import com.daokin.biochecker.model.InventoryChip;
import com.daokin.biochecker.model.InventoryChipData;
import com.daokin.biochecker.model.ModelContainer;
import com.daokin.biochecker.model.ProcessedResult.TestSample.SampleType;
import com.daokin.biochecker.model.ResultData;
import com.daokin.biochecker.model.Test;
import com.daokin.biochecker.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SamXCode on 2015/11/23.
 */
public class DBHelper extends SQLiteOpenHelper {

    private final static int DATABASE_VERSION = 2;
    private final static String DATABASE_NAME = "zepto.db";
    private final static String SAMPLE_TYPE_TABLE_NAME = "sample_type";
    private final static String USER_TABLE_NAME = "user";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.createUserTable(db);
        this.createAssayTable(db);
        this.createSampleTypeTable(db);
        this.createChipDataTable(db);
        this.createChipTable(db);
        this.createTestTable(db);
    }

    private void createUserTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + USER_TABLE_NAME + " (id INTEGER PRIMARY KEY," +
                " zepto_customer_id varchar(255) DEFAULT NULL," +
                " name varchar(255) DEFAULT NULL," +
                " password varchar(255) DEFAULT NULL," +
                " first_name varchar(255) DEFAULT NULL," +
                " last_name varchar(255) DEFAULT NULL)";
        db.execSQL(sql);
    }

    private void createSampleTypeTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + SAMPLE_TYPE_TABLE_NAME +
                " (id INTEGER PRIMARY KEY NOT NULL," +
                " name varchar(255) DEFAULT NULL," +
                " description varchar(255) DEFAULT NULL)";
        db.execSQL(sql);
    }

    private void createAssayTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE assay (" +
                "  id INTEGER NOT NULL PRIMARY KEY," +
                "  name varchar(255) DEFAULT NULL," +
                "  description varchar(255) DEFAULT NULL)";
        db.execSQL(sql);
    }

    private void createChipDataTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE inventory_chip_data (" +
                " chip_id INTEGER DEFAULT NULL," +
                " data_type_id INTEGER DEFAULT NULL," +
                " data text," +
                " last_updated TEXT DEFAULT NULL," +
                " PRIMARY KEY(chip_id,data_type_id))";
        db.execSQL(sql);
    }

    private void createChipTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE inventory_chip (" +
                " id INTEGER NOT NULL PRIMARY KEY," +
                " assay_type_id Integer NOT NULL," +
                " sop_title varchar(255) DEFAULT NULL," +
                " sop_version varchar(255) DEFAULT NULL," +
                " chip_size_type_id INTEGER DEFAULT NULL," +
                " QCFailed varchar(255) DEFAULT NULL," +
                " assay_printing text)";
        db.execSQL(sql);
    }

    private void createTestTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE test (" +
                " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                " oid INTEGER DEFAULT NULL," +
                " name varchar(225) DEFAULT NULL," +
                " description varchar(225) DEFAULT NULL," +
                " sample_name varchar(225) DEFAULT NULL," +
                " create_time TEXT DEFAULT NULL," +
                " zepto_customer_id varchar(255) DEFAULT NULL," +
                " chip_id INTEGER DEFAULT NULL," +
                " state Integer DEFAULT NULL)";
        db.execSQL(sql);
    }


    //upgrade sqls
    private String CREATE_TEMP_TEST = "alter table test rename to _temp_test";
    private String V2_INSERT_TEST_DATA = "insert into test select * from _temp_test";
    private String DROP_TEMP_TEST = "drop table _temp_test";

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (newVersion) {
            case 2:
                db.execSQL(CREATE_TEMP_TEST);
                createTestTable(db);
                db.execSQL(V2_INSERT_TEST_DATA);
                db.execSQL(DROP_TEMP_TEST);
        }
    }

    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", user.getId());
        values.put("name", user.getName());
        values.put("zepto_customer_id", user.getZeptoCustomerID());
        values.put("password", user.getPassword());
        values.put("first_name", user.getFirstName());
        values.put("last_name", user.getLastName());
        db.insert(USER_TABLE_NAME, null, values);
    }

    public User queryUser(String username, String password) {
        String sql = "SELECT * FROM user WHERE zepto_customer_id=? AND password=?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[]{username, password});
        User user = null;
        if (cursor != null && cursor.moveToFirst()) {
            user = new User();
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String zepto_customer_id = cursor.getString(cursor.getColumnIndex("zepto_customer_id"));
            String firstName = cursor.getString(cursor.getColumnIndex("first_name"));
            String lastName = cursor.getString(cursor.getColumnIndex("last_name"));
            user.setId(id);
            user.setName(name);
            user.setZeptoCustomerID(zepto_customer_id);
            user.setFirstName(firstName);
            user.setLastName(lastName);
        }
        return user;
    }

    public List<SampleType> querySamples() {
        String sql = "SELECT * FROM sample_type";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        List<SampleType> types = new ArrayList<SampleType>();
        if (null != cursor && cursor.moveToFirst()) {
            types = new ArrayList<>(cursor.getCount());
            while (cursor.moveToNext()) {
                SampleType type = new SampleType();
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String description = cursor.getString(cursor.getColumnIndex("description"));
                type.setId(id);
                type.setName(name);
                type.setDescription(description);
                types.add(type);
            }
        }
        return types;
    }

    public void addSampleTypes(List<SampleType> types) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            deleteSampleTypes();
            for (SampleType type : types) {
                ContentValues values = new ContentValues();
                values.put("id", type.getId());
                values.put("name", type.getName());
                values.put("description", type.getDescription());
                db.insert("sample_type", null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void deleteSampleTypes() {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "DELETE FROM sample_type";
        db.execSQL(sql);
    }

    public Long getLastTestId() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT MAX(id) FROM test";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor != null && cursor.moveToFirst()) {
            return cursor.getLong(cursor.getColumnIndex("MAX(id)")) + 1;
        }
        return 1L;
    }

    public boolean isChipDataExisted(Long chipId, int typeId) {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT count(*) FROM inventory_chip_data WHERE chip_id=" + chipId
                + " and data_type_id=" + typeId;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor != null && cursor.moveToFirst()) {
            int count = cursor.getInt(cursor.getColumnIndex("count(*)"));
            return count >= 1 ? true : false;
        }
        return true;
    }

    public void updateChipData(InventoryChipData chipData) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("chip_id", chipData.getChipId());
        values.put("data", chipData.getData());
        values.put("data_type_id", chipData.getDataTypeId());
        values.put("last_updated", chipData.getLastUpdated());
        db.update("inventory_chip_data", values, "chip_id=? AND data_type_id=?",
                new String[]{String.valueOf(chipData.getChipId()),
                        String.valueOf(chipData.getDataTypeId())});
    }

    public void addChipData(InventoryChipData chipData) {
        if (isChipDataExisted(chipData.getChipId(), chipData.getDataTypeId())) {
            updateChipData(chipData);
            return;
        }
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("chip_id", chipData.getChipId());
        values.put("data", chipData.getData());
        values.put("data_type_id", chipData.getDataTypeId());
        values.put("last_updated", chipData.getLastUpdated());
        db.insert("inventory_chip_data", null, values);
    }

    public InventoryChipData getChipData(Long chipId, Integer dataTypeId) {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM inventory_chip_data WHERE chip_id=" + chipId
                + " and data_type_id=" + dataTypeId;
        Cursor cursor = db.rawQuery(sql, null);
        InventoryChipData inventoryChipData = null;
        if (cursor != null && cursor.moveToFirst()) {
            inventoryChipData = new InventoryChipData();
            String data = cursor.getString(cursor.getColumnIndex("data"));
            inventoryChipData.setChipId(chipId);
            inventoryChipData.setDataTypeId(dataTypeId);
            inventoryChipData.setData(data);
            inventoryChipData.setLastUpdated(
                    cursor.getString(cursor.getColumnIndex("last_updated")));
        }
        return inventoryChipData;
    }

    public void deleteChipData(Long chipId) {
        String sql = "DELETE FROM inventory_chip_data WHERE chip_id=" + chipId;
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(sql);
    }

    public AssayData getAssayData(Long chipId) {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM inventory_chip WHERE id=" + chipId;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor != null && cursor.moveToFirst()) {
            String str = cursor.getString(cursor.getColumnIndex("assay_printing"));
            AssayData assayData = new Gson().fromJson(str, new TypeToken<AssayData>() {
            }.getType());
            return assayData;
        }
        return null;
    }

    public int getChipCount(Long id) {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT count(*) FROM inventory_chip WHERE id=" + id;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor != null && cursor.moveToFirst()) {
            return cursor.getInt(cursor.getColumnIndex("count(*)"));
        }
        return 0;
    }

    public void addInventoryChip(InventoryChip chip) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", chip.getId());
        values.put("assay_type_id", chip.getAssayTypeId());
        values.put("assay_printing", chip.getAssayPrinting());
        values.put("QCFailed", chip.getQCFailed());
        db.insert("inventory_chip", null, values);
    }

    public boolean deleteInventoryChip(Long chipId) {
        try {
            String sql = "DELETE FROM inventory_chip WHERE id=" + chipId;
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL(sql);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<InventoryChip> getChips() {
        String sql = "SELECT * FROM inventory_chip";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        List<InventoryChip> types = null;
        if (cursor != null) {
            types = new ArrayList<>(cursor.getCount());
            while (cursor.moveToNext()) {
                InventoryChip type = new InventoryChip();
                type.setAssayPrinting(cursor.getString(cursor.getColumnIndex("assay_printing")));
                type.setId(cursor.getLong(cursor.getColumnIndex("id")));
                type.setQCFailed(cursor.getString(cursor.getColumnIndex("QCFailed")));
                types.add(type);
            }
        }
        return types == null ? new ArrayList<InventoryChip>() : types;
    }

    public List<Test> getTests(String customerId) {
        String sql = "SELECT * FROM test WHERE zepto_customer_id='" + customerId + "'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        List<Test> tests = null;
        if (cursor != null) {
            tests = new ArrayList<>(cursor.getCount());
            while (cursor.moveToNext()) {
                Test test = new Test();
                test.setId(cursor.getLong(cursor.getColumnIndex("id")));
                test.setName(cursor.getString(cursor.getColumnIndex("name")));
                test.setSampleName(cursor.getString(cursor.getColumnIndex("sample_name")));
                test.setCreatedTime(cursor.getString(cursor.getColumnIndex("create_time")));
                test.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                test.setChipId(cursor.getLong(cursor.getColumnIndex("chip_id")));
                test.setOid(cursor.getLong(cursor.getColumnIndex("oid")));
                test.setState(cursor.getInt(cursor.getColumnIndex("state")));
                tests.add(test);
            }
        }
        return tests;
    }

    public boolean tableIsExist(String tabName) {
        boolean result = false;
        if (tabName == null) {
            return false;
        }
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = this.getReadableDatabase();
            String sql = "select count(*) as c from sqlite_master where type ='table' and name ='" + tabName.trim() + "'";
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                int count = cursor.getInt(0);
                if (count > 0) {
                    result = true;
                }
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
        return result;
    }


    public void addTest(Test test) {
        SQLiteDatabase db = getWritableDatabase();
        if (!tableIsExist("test")) {
            createTestTable(db);
        }
        ContentValues values = new ContentValues();
        values.put("create_time", test.getCreatedTime());
        values.put("description", test.getDescription());
        values.put("name", test.getName());
        values.put("sample_name", test.getSampleName());
        values.put("chip_id", test.getChipId());
        values.put("oid", test.getOid());
        values.put("state", test.getState());
        values.put("zepto_customer_id", ModelContainer.i.getUser().getZeptoCustomerID());
        long testId = db.insert("test", null, values);
        test.setId(testId);
    }

    public void updateTest(Test test) {
        SQLiteDatabase db = getWritableDatabase();
        if (!tableIsExist("test")) {
            createTestTable(db);
            return;
        }
        ContentValues values = new ContentValues();
        values.put("create_time", test.getCreatedTime());
        values.put("description", test.getDescription());
        values.put("name", test.getName());
        values.put("sample_name", test.getSampleName());
        values.put("chip_id", test.getChipId());
        values.put("oid", test.getOid());
        values.put("state", test.getState());
        values.put("zepto_customer_id", ModelContainer.i.getUser().getZeptoCustomerID());
        db.update("test", values, "id=?", new String[]{String.valueOf(test.getId())});
    }

    public void deleteTest(Long testId) {
        String sql = "DELETE FROM test WHERE id=" + testId;
        SQLiteDatabase db = getWritableDatabase();
        if (!tableIsExist("test")) {
            createTestTable(db);
            return;
        }
        db.execSQL(sql);
    }

    public ResultData getTestResult(Long testId, Integer chipDataTypeId) {
        String sql = "SELECT * FROM inventory_chip_data as a, test as t WHERE a.chip_id=t.chip_id "
                //There may be some old local tests data, to make it compatible
                + " and (a.data_type_id=" + ChipDataTypeEnum.TOXIN.getValue() + " or a.data_type_id= " + chipDataTypeId + ") and t.id=" + testId;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        ResultData resultData = null;
        if (cursor != null && cursor.moveToFirst()) {
            String data = cursor.getString(cursor.getColumnIndex("data"));
            resultData = new Gson().fromJson(data, new TypeToken<ResultData>() {}.getType());
        }
        return resultData;
    }
}
