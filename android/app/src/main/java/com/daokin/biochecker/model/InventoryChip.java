package com.daokin.biochecker.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Created by SamXCode on 2015/12/23.
 */
public class InventoryChip {
    private Long id;
    private Long assayTypeId;
    private String sopTitle;
    private String sopVersion;
    private long chipSizeTypeId;
    private String assayPrinting;
    private String QCFailed;

    public Long getAssayTypeId() {
        return assayTypeId;
    }

    public void setAssayTypeId(Long assayTypeId) {
        this.assayTypeId = assayTypeId;
    }

    public String getQCFailed() {
        return QCFailed;
    }

    public void setQCFailed(String QCFailed) {
        this.QCFailed = QCFailed;
    }

    public String getAssayPrinting() {
        return assayPrinting;
    }

    public void setAssayPrinting(String assayPrinting) {
        this.assayPrinting = assayPrinting;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSopTitle() {
        return sopTitle;
    }

    public void setSopTitle(String sopTitle) {
        this.sopTitle = sopTitle;
    }

    public String getSopVersion() {
        return sopVersion;
    }

    public void setSopVersion(String sopVersion) {
        this.sopVersion = sopVersion;
    }

    public long getChipSizeTypeId() {
        return chipSizeTypeId;
    }

    public void setChipSizeTypeId(long chipSizeTypeId) {
        this.chipSizeTypeId = chipSizeTypeId;
    }

    public AssayData getAssayData() {
        return new Gson().fromJson(assayPrinting, new TypeToken<AssayData>() {
        }.getType());
    }

    @Override
    public String toString() {
        return "Chip Id: " + id;
    }
}
