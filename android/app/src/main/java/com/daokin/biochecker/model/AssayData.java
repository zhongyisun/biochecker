package com.daokin.biochecker.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.daokin.biochecker.model.ProcessedResult.TestChip.AssayPrinting;

/**
 * Created by SamXCode on 2015/12/23.
 */
public class AssayData {

    public final static int ACTIVE_TYPE = 0;
    public final static int NEGATIVE_TYPE = 1;
    public final static int POSITIVE_TYPE = 2;
    private List<Assay> assays;
    private Map<String, Sensor> sensors;
    private List<Control> controls;
    private int nmps_initial = 5;
    private int nmps_final = 5;
    private Measurement measurement;
    private List<Sensor> sensor_list;

    public List<Sensor> getSensorList() {
        return sensor_list;
    }

    public void setSensorList(List<Sensor> sensor_list) {
        this.sensor_list = sensor_list;
    }

    public Map<String, Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(Map<String, Sensor> sensors) {
        this.sensors = sensors;
    }

    public List<Assay> getAssays() {
        return assays;
    }

    public void setAssays(List<Assay> assays) {
        this.assays = assays;
    }

    public List<Control> getControls() {
        return controls;
    }

    public List<Control> getValidControls() {
        List<Control> ret = new ArrayList<Control>();
        for (Control c : controls) {
            if (c.duration > 0) {
                ret.add(c);
            }
        }
        return ret;
    }

    public void setControls(List<Control> controls) {
        this.controls = controls;
    }

    public int getNmps_initial() {
        return nmps_initial;
    }

    public void setNmps_final(int nmps_final) {
        this.nmps_final = nmps_final;
    }

    public Measurement getMeasurement() {
        return this.measurement;
    }

    public void setMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }

    public class Assay {
        private int analyte;
        private String name;
        private String unit;
        private int duration;

        public int getAnalyte() {
            return analyte;
        }

        public void setAnalyte(int analyte) {
            this.analyte = analyte;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public int getDuration() {
            return duration;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }
    }

    public class Sensor {
        public String label;
        public String mux1;
        public String mux2;
        public int analyte;
        public int type;

        //private int analyte;
        //private int type;

        public int getAnalyte() {
            return analyte;
        }

        public void setAnalyte(int analyte) {
            this.analyte = analyte;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }

    public class Measurement {
        private float bridge_amp = 300.0f;
        private float coil_amp = 24.0f;
        private float bridge_hz = 1000.0f;
        private float coil_dc = 0.0f;
        private float coil_hz = 50.0f;
        private float meas_period = 1.0f;
        private byte digital_gain = 0x01;

        public float getBridge_amp() {
            return bridge_amp;
        }

        public void setBridge_amp(float bridge_amp) {
            this.bridge_amp = bridge_amp;
        }

        public float getBridge_hz() {
            return bridge_hz;
        }

        public void setBridge_hz(float bridge_hz) {
            this.bridge_hz = bridge_hz;
        }

        public float getCoil_amp() {
            return coil_amp;
        }

        public void setCoil_amp(float coil_amp) {
            this.coil_amp = coil_amp;
        }

        public float getCoil_hz() {
            return coil_hz;
        }

        public void setCoil_hz(float coil_hz) {
            this.coil_hz = coil_hz;
        }

        public float getCoil_dc() {
            return coil_dc;
        }

        public void setCoil_dc(float coil_dc) {
            this.coil_dc = coil_dc;
        }

        public float getMeas_period() {
            return meas_period;
        }

        public void setMeas_period(float meas_period) {
            this.meas_period = meas_period;
        }

        public byte getDigital_gain() {
            return digital_gain;
        }

        public void setDigital_gain(byte digital_gain) {
            this.digital_gain = digital_gain;
        }

    }

    public class Control {
        //private int designator;
        private int address;
        //private float opentime;
        private float frequency;
        private float duration;

        public int getAddress() {
            return address;
        }

        public void setAddress(int address) {
            this.address = address;
        }

        public float getFrequency() {
            return frequency;
        }

        public void setFrequency(float frequency) {
            this.frequency = frequency;
        }

        public float getDuration() {
            return duration;
        }

        public void setDuration(float p_duration) {
            this.duration = duration;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Assay assay : assays) {
            sb.append(assay.getAnalyte() + " ");
        }
        return "ChipAssayTypeAnalytes: " + sb.toString();
    }

    public Map<Integer, List<String>> getAnalyteSensorMap() {
        Map<Integer, List<String>> map = new HashMap<>();
        for (Assay assay : assays) {
            map.put(assay.getAnalyte(), new ArrayList<String>());
        }
        for (Map.Entry<String, Sensor> entry : sensors.entrySet()) {
            String label = entry.getKey();
            map.get(entry.getValue().getAnalyte()).add(label);
        }
        return map;
    }

    public List<AssayPrinting> getAssayPrintings(Map<String, Byte> sensorMap) {
        List<AssayPrinting> aps = new ArrayList<>();
        Map<Integer, String> printMap = new HashMap<>();
        for (AssayData.Assay assay : assays) {
            printMap.put(assay.getAnalyte(), "");
        }
        for (Map.Entry<String, Sensor> entry : sensors.entrySet()) {
            AssayData.Sensor sensor = entry.getValue();
            int a = sensor.getAnalyte();
            Byte b = sensorMap.get(entry.getKey());
            if (b != null) {
                printMap.put(a, printMap.get(a) + ",s" + b);
            }
        }
        for (AssayData.Assay assay : assays) {
            AssayPrinting assayPrinting = new AssayPrinting();
            AssayPrinting.Assay ay = new AssayPrinting.Assay();
            assayPrinting.setAssay(ay);
            ay.setName(assay.getName());
            String printStr = printMap.get(assay.getAnalyte());
            if (printStr.length() >= 1) {
                assayPrinting.setPrinting(printStr.substring(1));
            } else {
                assayPrinting.setPrinting("");
            }
            aps.add(assayPrinting);
        }
        return aps;
    }

    public int getMaxDuration() {
        int max = 0;
        for (Assay assay : assays) {
            int duration = assay.getDuration();
            max = duration > max ? duration : max;
        }
        return max;
    }

    public String getMux2Label() {
        String label = "";
        for (Map.Entry<String, Sensor> entry : sensors.entrySet()) {
            Sensor sensor = entry.getValue();
            if (sensor.getType() == POSITIVE_TYPE) {
                label = entry.getKey();
                break;
            }
        }
        return label;
    }

    public Map<Byte, String> getMux1LabelMap() {
        Map<Byte, String> mux1LabelMap = new HashMap<Byte, String>();
        for (Sensor sensor : sensor_list) {
            String label = sensor.label;
            byte b = Byte.parseByte(sensor.mux1.replaceFirst("0x", ""), 16);
            mux1LabelMap.put(b, label);
        }
        return mux1LabelMap;
    }

    public Map<String, Byte> getLableMux1Map() {
        Map<String, Byte>sensorMap = new HashMap<String, Byte>();
        for (Sensor sensor : sensor_list) {
            String label = sensor.label;
            byte b = Byte.parseByte(sensor.mux1.replaceFirst("0x", ""), 16);
            sensorMap.put(label, b);
        }
        return sensorMap;
    }

    public Map<String, Byte> getLabelMux2Map() {
        Map<String, Byte> labelMux2Map = new HashMap<String, Byte>();
        for (Sensor sensor : sensor_list) {
            if (sensor.mux2 != null) {
                String label = sensor.label;
                byte b = Byte.parseByte(sensor.mux2.replaceFirst("0x", ""), 16);
                labelMux2Map.put(label, b);
            }
        }
        return labelMux2Map;
    }

    public List<Sensor> getReferenceSensorList() {
        List<Sensor> referenceSensorList = new ArrayList<Sensor>();
        for (Sensor sensor : sensor_list) {
            if (sensor.analyte == 0 && sensor.type == 1) {
                referenceSensorList.add(sensor) ;
            }
        }
        return referenceSensorList;
    }

}
