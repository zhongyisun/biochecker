package com.daokin.biochecker.protocol;

import com.daokin.biochecker.protocol.PacketDefinitions.MessageSize;

/**
 *
 */
public class StopCommand extends GenericMessage{
  public StopCommand(Byte[] vtag) {
	    this.vTag = vtag;
	    this.payload = new Byte[MessageSize.StopBytes.getValue()];
	    this.seqNum = generate_seqNum();
	    this.setHeaderFlag((byte) PacketDefinitions.HeaderFlag.Syn.getValue());
	    this.setMessageType(PacketDefinitions.MessageType.Stop);
	  }
}
